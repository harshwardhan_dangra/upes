$(".CarouselforSlick").slick({ dots: !1, infinite: !1, arrows: !1, speed: 300, slidesToShow: 3, slidesToScroll: 3, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 3, slidesToScroll: 3, infinite: !0, dots: !1, arrows: !0 } }, { breakpoint: 767, settings: { dots: !1, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }, { breakpoint: 480, settings: { dots: !1, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }] });
/**Award carousel**/
$("#AwardCarousel").slick({ dots: !1, infinite: !0, arrows: !1, autoplay: !0, autoplaySpeed: 2e3, speed: 300, arrows: !1, slidesToShow: 3, slidesToScroll: 1, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 3, slidesToScroll: 3, infinite: !0, dots: !1, arrows: !0 } }, { breakpoint: 767, settings: { dots: !1, arrows: !1, slidesToShow: 1, slidesToScroll: 1 } }, { breakpoint: 480, settings: { dots: !1, arrows: !1, slidesToShow: 1, slidesToScroll: 1 } }] });
/**OneBlockcarousel**/
$(".OneBlockCarousel").slick({ dots: !1, infinite: !1, arrows: !0, speed: 300, slidesToShow: 1, slidesToScroll: 1, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 1, slidesToScroll: 1, infinite: !1, dots: !1, arrows: !0 } }, { breakpoint: 767, settings: { dots: !0, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }, { breakpoint: 480, settings: { dots: !0, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }] });
/**TwoBlockcarousel**/
$(".TwoBlockCarousel").slick({ dots: !1, infinite: !1, arrows: !0, speed: 300, slidesToShow: 2, slidesToScroll: 2, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 2, slidesToScroll: 2, infinite: !0, dots: !1, arrows: !0 } }, { breakpoint: 767, settings: { dots: !0, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }, { breakpoint: 480, settings: { dots: !0, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }] });
/**Companies List**/
$(".CarouselForCompanies").slick({ dots: !1, infinite: !0, arrows: !0, autoplay: !0, autoplaySpeed: 2e3, speed: 300, slidesToShow: 5, slidesToScroll: 1, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 3, slidesToScroll: 3, infinite: !0, dots: !1, arrows: !0 } }, { breakpoint: 767, settings: { dots: !0, arrows: !0, slidesToShow: 3, slidesToScroll: 3 } }, { breakpoint: 480, settings: { dots: !0, arrows: !0, slidesToShow: 3, slidesToScroll: 3 } }] });
$(".CarouselForCompanies").on("click", function (event, slick, current_slide_index, next_slide_index) {
})
/**Courses**/
$("#CoursesCarousel").slick({ dots: !1, infinite: !1, arrows: !0, speed: 300, slidesToShow: 5, slidesToScroll: 1, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 4, slidesToScroll: 1, infinite: !0, dots: !1, arrows: !0, infinite: !1 } }, { breakpoint: 767, settings: { dots: !0, arrows: !0, slidesToShow: 2, slidesToScroll: 2 } }, { breakpoint: 480, settings: { dots: !0, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }] });
/**Courses**/
$(".students-slide").slick({ dots: !1, infinite: !1, arrows: !0, speed: 300, slidesToShow: 3, slidesToScroll: 1, responsive: [{ breakpoint: 1024, settings: { slidesToShow: 2, slidesToScroll: 2, infinite: !0, dots: !1, arrows: !0 } }, { breakpoint: 600, settings: { dots: !0, arrows: !0, slidesToShow: 2, slidesToScroll: 2 } }, { breakpoint: 480, settings: { dots: !0, arrows: !0, slidesToShow: 1, slidesToScroll: 1 } }] });
$('.nav-pills a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
});

$('#navbarNav').click(function () {
    $('.CollapsePanel').toggleClass('show');
    $('.icon-bar').toggleClass('active');
    $('.MenuOverlay').toggle();
    $('body').toggleClass('hidden');
});
$('.carousel').carousel({
    interval: 6000
});
$(".ClickArrow").click(function () {
    $('html, body').animate({
        scrollTop: $("#PanelTwo").offset().top
    }, 1000);
});

$('#txtSearch').on("keypress", function (e) {
    if (e.keyCode == 13) {
        doRedirectSearch();
    }
});

function doRedirectSearch() {
    var searchTerm = $('#txtSearch').val();
    if (searchTerm != '')
        window.location = "/Search/" + searchTerm;
}


function doLoadSuggestions() {
    var ArrSearchData = [];
    var searchString = $('#txtSearch').val();
    if (searchString.length >= 3) {
        $.ajax({
            url: "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/suggestions/search",
            data: { q: searchString, customconfig: '7c26ee75-6ad6-4446-a84e-3db736bfce51' },
            type: "GET",
            crossDomain: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Ocp-Apim-Subscription-Key', '967c9ca99b9b43f58f297b4049bc4c87');
            },
            success: function (response) {
                if (response != "") {
                    var obj = jQuery.parseJSON(response);
                    $.each(obj.suggestionGroups[0].searchSuggestions, function (key, val) {
                        ArrSearchData.push({ label: val.displayText.toString() });
                    });
                    $("#txtSearch").autocomplete({
                        source: ArrSearchData,
                        scroll: true,
                        selectFirst: true,
                        multiple: false, multipleSeparator: ';',
                        autoFill: false,
                        minLength: 2
                    });
                    console.log(ArrSearchData);
                }
                else {
                    $("#txtSearch").catcomplete({
                        delay: 0,
                        source: '',
                    });
                }
            }
        });
    }
}

function doOpenHomePopup(videoLink) {
    $('#ifrmHomeBannerVideo').attr('src', videoLink);
    $('#topBannerVideo').modal('show');
}

function stopVideoHome() {
    $('#ifrmHomeBannerVideo').removeAttr('src');
    $('#topBannerVideo').modal('hide');
};

function doOpenVideoHomeFuture(videoLink) {
    $('iframe.homeBannerVideoFuture').attr('src', videoLink);
    $('#divFutureWatchVideo').modal('show');
}

function stopVideoHomeFuture() {
    $('iframe.homeBannerVideoFuture').removeAttr('src');
    $('#divFutureWatchVideo').modal('hide');
};

var stopVideo = function () {
    var iframe = document.querySelector('iframe.bottomVideo');
    var video = document.querySelector('video');
    if (iframe !== null) {
        var iframeSrc = iframe.src;
        iframe.src = iframeSrc;
    }
    if (video !== null) {
        video.pause();
    }
};

function doShowCourseEligibilityModal(divConID) {
    var modalContent = $('#' + divConID).html();
    $('#divCourseEligibilityModalContent').empty().html(modalContent);
    $('#divEligibilityModal').modal('show');
}

function openChat() {
    window.open("http://ameyo.delhi.upes.ac.in:8888/ameyochatjs/defaultChat/chatRegistration.html?campaignId=16&nodeflowId=5&contextId=16&contextType=constraint.context.type.campaign", "popupWindow", "width=600, height=720, scrollbars=yes");
}
$('.carousel').hover(function () {
    $("#HomeCarouselPanel").carousel('pause');
}, function () {
    $("#HomeCarouselPanel").carousel('cycle');
});

$('.modal').on('shown.bs.modal', function (e) {
    $('#HomeCarouselPanel').carousel('pause');
})

function doOpenVirtualTour(videoLink) {
    $('#iframeVirtualTour').attr('src', videoLink);
    $('#topBanner3DCampus').modal('show');
}
$('#helplineCrop').click(function () {
    $('.Number').toggleClass('opennumber');

});
$('main').click(function () {
    $('.Number').removeClass('opennumber');
});
function doOpenSocialImpactVideo(videoLink) {
    $('#ifrmSocialImpact').attr('src', videoLink);
    $('#divSocialImpactVideo').modal('show');
}
function stopVideoHomeSocialImpact() {
    $('#ifrmSocialImpact').removeAttr('src');
    $('#divSocialImpactVideo').modal('hide');
};

function doOpenProgramVideo(videoLink) {
    $('#ifrmProgramVideo').attr('src', videoLink);
    $('#divProgramDetailsVideo').modal('show');
}
function stopVideoProgramDetails() {
    $('#ifrmProgramVideo').removeAttr('src');
    $('#divProgramDetailsVideo').modal('hide');
};

function doOpenParentVideo(videoLink) {
    $('#ifrmParentVideo').attr('src', videoLink);
    $('#divParentVideo').modal('show');
}
function stopVideoParentDetails() {
    $('#ifrmParentVideo').removeAttr('src');
    $('#divParentVideo').modal('hide');
};

function goBack() {
    window.history.back();
}

//$('.AccordionStyle .collapse').on('hide.bs.collapse', function (e) {
//    var $card = $(this).closest('.card');
//    $('html,body').animate({
//        scrollTop: $card.offset().top-200
//    },100);
//}); 

$(document).ready(function () {
    $('.AccordionStyle .collapse').on('hide.bs.collapse', function (e) {
        if ($(this).height() > 400) {
            var $card = $(this).closest('.card');
            $('html,body').animate({
                scrollTop: $card.offset().top - 200
            }, 100);
        }
    });


});
$(document).ready(function () {
    $('.BannerStripContext h1').addClass('animateTitle');
});
$(document).ready(function () {
    var scrollTop = $(".scrollTop");
    $(window).scroll(function () {
        var topPos = $(this).scrollTop();
        if (topPos > 1500) {
            $(scrollTop).css("opacity", "1");

        } else {
            $(scrollTop).css("opacity", "0");
        }

    });

    $(scrollTop).click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;

    });
});
