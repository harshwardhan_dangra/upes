﻿function loginValidate() {
    var email = $('#txtEmailFL').val();
    var txtPassword = $('#txtPasswordFL').val();
    //alert("sss");
    //alert(txtPassword);
    $(".validation").remove();
    if (email == "") {
        $(".validation").remove();
        $("#txtEmailFL").focus();
        $("#txtEmailFL").after("<p class='validation' style='color:red;margin-bottom:0px;'>Please enter your Email ID</p>");
        return false;
    }
    if (!(isValidEmailAddress(email))) {
        $(".validation").remove();
        $('#txtEmailFL').focus();
        $('#txtEmailFL').after("<p class='validation' style='color:red;margin-bottom:0px;'>Please enter your valid Email ID</p>");
        return false;
    }
    if (txtPassword == "") {
        $(".validation").remove();
        $("#txtPasswordFL").focus();
        $("#txtPasswordFL").after("<p class='validation' style='color:red;margin-bottom:0px;'>Please enter your Password</p>");
        return false;
    }

    $.ajax({
        url: "/umbraco/Surface/Login/CheckLogin",
        type: "GET",
     
        data: {
            "email": email, "password": txtPassword
        },
        success: function (response) {

            if (response == "yes") {
                console.log('login success');
                window.location.href = "/faculty/GDPI";
            }

            else {
                //alert("2i:" + response);
                console.log('login failed');
                $("#txtPasswordFL").after("<p class='validation' style='color:red;margin-bottom:0px;'>Invalid Credentials</p>");
                return false;
            }
        },
        error: function (response) {
            //alert("2:" + response);
            console.log('login failed');

            $("#txtPasswordFL").after("<p class='validation' style='color:red;margin-bottom:0px;'>Invalid Credentials</p>");
            return false;
        }
    });
   
}

function CPValidate() {
    //var OldPassword = $('#txtOldPassword').val();
    var NewPassword = $('#txtNewPassword').val();
    var ConfirmNewPassword = $('#txtConNewPassword').val();

    if (NewPassword == "") {
        $(".validation").remove();
        $("#txtNewPassword").focus();
        $("#txtNewPassword").after("<p class='validation' style='color:red;margin-bottom:0px;'>Please enter the Password</p>");
        return false;
    }

    if (NewPassword.length > 8 || NewPassword.length < 6) {
        $(".validation").remove();
        $("#txtNewPassword").focus();
        $("#txtNewPassword").after("<p class='validation' style='color:red;margin-bottom:0px;'>Password must be  6-8 characters </p>");
        return false;
    }

    if (ConfirmNewPassword == "") {
        $(".validation").remove();
        $("#txtConNewPassword").focus();
        $("#txtConNewPassword").after("<p class='validation' style='color:red;margin-bottom:0px;'>Please Confirm the Password</p>");
        return false;
    }

    if (NewPassword != ConfirmNewPassword) {
        $(".validation").remove();
        $("#txtConNewPassword").focus();
        $("#txtConNewPassword").after("<p class='validation' style='color:red;margin-bottom:0px;'>Password not matched</p>");
        return false;
    }


    $.ajax({
        url: "/umbraco/Surface/Login/ChangePassword",
        type: "GET",

        data: {
            "newpassword": NewPassword
        },
        success: function (response) {

            if (response == "yes") {
                $(".validation").remove();
                $("#cpformconf").show();
                $("#cpform").hide();
                console.log("CP:"+response);
            }

            else {
                console.log("CP:" + response);
                $(".validation").remove();
                $("#txtConNewPassword").focus();
                $("#txtConNewPassword").after("<p class='validation' style='color:red;margin-bottom:0px;'>Invalid Credentials</p>");
                return false;
            }
        },
        error: function (response) {
            console.log("CP:" + response);
            $(".validation").remove();
            $("#txtConNewPassword").focus();
            $("#txtConNewPassword").after("<p class='validation' style='color:red;margin-bottom:0px;'>Invalid Credentials</p>");
            return false;
        }
    });

}

function FPValidate() {

  
    var FPEmail = $('#txtEmailID').val();
    if (FPEmail == "") {
        $(".validation").remove();
        $("#txtEmailID").focus();
        $("#txtEmailID").after("<p class='validation' style='color:red;margin-bottom:0px;'>Please enter your Email ID</p>");
        return false;
    }
    if (!(isValidEmailAddress(FPEmail))) {
        $(".validation").remove();
        $('#txtEmailID').focus();
        $('#txtEmailID').after("<p class='validation' style='color:red;margin-bottom:0px;'>Please enter your valid Email ID</p>");
        return false;
    }


    $.ajax({
        url: "/umbraco/Surface/Login/ForgotPassword",
        type: "GET",

        data: {
            "Email": FPEmail
        },
        success: function (response) {

            if (response == "yes") {
                $(".validation").remove();
                $("#FPformConf").show();
                $("#FPform").hide();
                console.log("FP:" + response);
            }

            else {
                console.log("FP:" + response);
                $(".validation").remove();
                $("#txtEmailID").focus();
                $("#txtEmailID").after("<p class='validation' style='color:red;margin-bottom:0px;'>No Records Found</p>");
                return false;
            }
        },
        error: function (response) {
            console.log("FP:" + response);
            $(".validation").remove();
            $("#txtEmailID").focus();
            $("#txtEmailID").after("<p class='validation' style='color:red;margin-bottom:0px;'>No Records Found</p>");
            return false;
        }
    });

}
function loginRest() {
    $('#txtEmailFL').val('');
    $('#txtPasswordFL').val('');
    $(".validation").remove();
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}