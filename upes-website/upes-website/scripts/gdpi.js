﻿var validmarks = 0;

function getEventList() {
    $.ajax({
        type: "GET",
        url: "/umbraco/Surface/GDPI/getEventList",
        data: null,
        success: function (response) {
            //console.log('getCourseList: ' + response);
            $('#ddlEvent').html(response);
        },
        error: function () {
        }
    });
}

function getGroupList() {
    var course = $('#ddlEvent').val();
    //console.log('course: ' + course);
    if (course != '0') {
        $.ajax({
            type: "GET",
            url: "/umbraco/Surface/GDPI/getGroupList",
            data: { "c_id": course },
            success: function (response) {
                //console.log('getEventsList: ' + response);
                $('#ddlGroup').html(response);
            },
            error: function () {
            }
        });
    }
}

function getSearchResult(rowcount) {
    $("#marknote").hide();

    var eventid = $('#ddlEvent').val();
    var groupid = $('#ddlGroup').val();
    var appno = $('#txtApplicantionNo').val();
    var process = $('input[name="rdProcess"]:checked').val();

    if (eventid == 0 && appno == '') {
        alert('Please select the event name or enter application no to proceed');
        $('#ddlEvent').focus();
        return false;
    }
    else if (eventid != 0) {
        if (eventid == 0 || eventid == '') {
            alert('Please select the event name');
            $('#ddlEvent').focus();
            return false;
        }

        if (groupid == 0 || groupid == '') {
            alert('Please select the group name');
            $('#ddlGroup').focus();
            return false;
        }
    }
    if (appno == '') {
        appno = 0;
    }
    else {
        if (!doNumber(appno)) {
            alert('Please enter valid application no.');
            $('#txtApplicantionNo').focus();
            return false;
        }
    }


    $.ajax({
        url: "/umbraco/surface/GDPI/getSearchResult",
        type: 'GET',
        data: "eventid=" + eventid + "&groupid=" + groupid + "&appno=" + appno + "&process=" + process + "&rowcount=" + rowcount,
        beforeSend: function () {
            $('#btnSearch').empty().html("Loading...");
            $('#divSearchResults').empty().html();
        },
        success: function (response) {
            //alert(response);
            if (response != 'Error') {
                var arResponse = response.split('||');
                $("#marknote").show();
                $('#divSearchResults').empty().html(response);
                $('#btnSearch').empty().html("Search");
            } else {
                $('#divSearchKeyword').empty().html('Error occured while getting your search result. Please try again.');
            }
        }
    });
}

function doNumber(strString) {
    var strValidChars = "0123456789.-";
    var strChar;
    var blnResult = true;

    if (strString.length == 0)
        return false;
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strValidChars.indexOf(strChar) == -1) {
            blnResult = false;
        }
    }

    return blnResult;
}

function doSubmit() {
    alert("Marks Updated Successfully");
}

function doReset() {
    $('#ddlEvent').val() = '0';
    $('#ddlGroup').val() = '0';
    $('input[name="rdProcess"]:checked').val() = 'GD';
}

function onEditMarks(id) {
    $('#lblResponse' + id).val('');
}

function UpdateMarks(studid, eventid, groupid, id, reqparameterid) {
    var parameterid = '';
    var maxmarks = '';
    var arParameterID = reqparameterid.split('|');
    console.log('arParameterID Length: ' + arParameterID.length);
    for (var i = 0; i < arParameterID.length; i++) {
        parameterid = arParameterID[i];
        var contrl = $('#txt' + id + parameterid);
        var txtMarks = $('#txt' + id + parameterid).val();
        maxmarks = $('#hfPercentageID' + id + parameterid).val();

        if (txtMarks == '') {
            alert('Please enter the marks');
            $('#txt' + id + parameterid).focus();
            //$('#FormButtonTwoSubmit').hide();
            contrl.addClass("er");
            return false;
        }
        else {
            contrl.removeClass("er");
            //$('#FormButtonTwoSubmit').hide();
        }

        if (!doNumber(txtMarks)) {
            alert('Please enter the marks numeric only');
            $('#txt' + id + parameterid).focus();
            //$('#FormButtonTwoSubmit').hide();
            validmarks += 1;
            return false;
        }
        else {
            //$('#FormButtonTwoSubmit').hide();
        }

        if (parseFloat(txtMarks) > parseFloat(maxmarks)) {
            alert('Please enter the marks less than ' + maxmarks);
            $('#txt' + id + parameterid).focus();
            //$('#FormButtonTwoSubmit').hide();
            validmarks += 1;
            return false;
        }
        else {
            //$('#FormButtonTwoSubmit').hide();
        }

        if (parseFloat(txtMarks) < 0) {
            alert('Please enter the marks more than 0');
            $('#txt' + id + parameterid).focus();
            //$('#FormButtonTwoSubmit').hide();
            validmarks += 1;
            return false;
        }
        else {
            //$('#FormButtonTwoSubmit').hide();
        }
	}
	
	for (var i = 0; i < arParameterID.length; i++) {
        parameterid = arParameterID[i];
        var contrl = $('#txt' + id + parameterid);
        var txtMarks = $('#txt' + id + parameterid).val();
        maxmarks = $('#hfPercentageID' + id + parameterid).val();
		
        $.ajax({
            type: "GET",
            url: "/umbraco/Surface/GDPI/UpdateMarks",
            data: { "Eventsid": eventid, "groupid": groupid, "studid": studid, "parameterid": parameterid, "maxmarks": parseInt(maxmarks), "marks": parseInt(txtMarks) },
            success: function (response) {
                console.log("Marks Updated: " + response);
                $('#lblResponse' + id).val(response);
                if (response == 'success') 
                    $('#lblResponse' + id).css('color','green');
                else
                    $('#lblResponse' + id).css('color', 'red');
                //var arRes = response.split('|');
                //if (arRes[0] == '1') {
                //    window.location.href = "/thankyou-event-registration?refno=" + arRes[1] + "&loc=" + arRes[2] + "&date=" + arRes[3] + "&course=" + arRes[4];
                //}
            },
            error: function () {
            }
        });
    }
}


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

//function getStudentRecords(studid,eventid,groupid) {
function getStudentRecords(studid, eventid, groupid) {
    var stjson = '{ "eventid": "' + eventid + '", "groupid": "' + groupid + '", "studid":"' + studid + ' "}';
    var obj = JSON.parse(stjson);

    $.ajax({
        url: "/umbraco/Surface/GDPI/getStudentRecords",
        type: 'GET',
        //data: { "eventid": eventid, "groupid": groupid, "studid": studid},
        data: obj,
        beforeSend: function () {
            //$('#divSearchKeyword').empty().html("Loading...");
        },
        success: function (response) {
            //alert(response);
            if (response != 'Error') {
                var arResponse = response.split('||');
                $('#divSearchResults').empty().html(response);
                //$('#divSearchKeyword').empty().html(arResponse[2]);
            } else {
                $('#divSearchKeyword').empty().html('Error occured while getting your search result. Please try again.');
            }
        }
    });
}

$(document).ready(function () {
    getEventList();
    //$('#FormButtonTwoSubmit').hide();
    //var refno = getUrlParameter('refno');
    //if (refno == '') {
    //    $('#divFormContainer').show();
    //    getEventList();
    //}
    //else {
    //    //$('#divFormContainer').hide();
    //    //var course = getUrlParameter('course');
    //    //var loc = getUrlParameter('loc');
    //    //var date = getUrlParameter('date');

    //    //var divInnerContent = $('#divInnerContent').html();
    //    //divInnerContent = divInnerContent.replace('@@refno@@', refno);
    //    //divInnerContent = divInnerContent.replace('@@loc@@', loc);
    //    //divInnerContent = divInnerContent.replace('@@date@@', date);
    //    //divInnerContent = divInnerContent.replace('@@course@@', course);

    //    //$('#divInnerContent').html(divInnerContent);
    //}
});

// Total number of rows visible at a time
var rowperpage = 5;
$(document).ready(function () {
    getData();

    $("#but_prev").click(function () {
        var rowid = Number($("#txt_rowid").val());
        var allcount = Number($("#txt_allcount").val());
        rowid -= rowperpage;
        if (rowid < 0) {
            rowid = 0;
        }
        $("#txt_rowid").val(rowid);
        getData();
    });

    $("#but_next").click(function () {
        var rowid = Number($("#txt_rowid").val());
        var allcount = Number($("#txt_allcount").val());
        rowid += rowperpage;
        if (rowid <= allcount) {
            $("#txt_rowid").val(rowid);
            getData();
        }

    });
});
/* requesting data */
function getData() {
    var rowid = $("#txt_rowid").val();
    var allcount = $("#txt_allcount").val();

    $.ajax({
        url: 'getEmployeeInfo.php',
        type: 'post',
        data: { rowid: rowid, rowperpage: rowperpage },
        dataType: 'json',
        success: function (response) {
            createTablerow(response);
        }
    });

}
/* Create Table */
function createTablerow(data) {

    var dataLen = data.length;

    $("#divSearchResults tr:not(:first)").remove();

    for (var i = 0; i < dataLen; i++) {
        if (i == 0) {
            var allcount = data[i]['allcount'];
            $("#txt_allcount").val(allcount);
        } else {
            var empid = data[i]['empid'];
            var empname = data[i]['empname'];
            var salary = data[i]['salary'];

            $("#divSearchResults").append("<tr id='tr_" + i + "'></tr>");
            $("#tr_" + i).append("<td align='center'>" + empid + "</td>");
            $("#tr_" + i).append("<td align='left'>" + empname + "</td>");
            $("#tr_" + i).append("<td align='center'>" + salary + "</td>");
        }
    }
}