﻿function getCourseList() {
    $.ajax({
        type: "GET",
        url: "/umbraco/Surface/AdmissionEventRegistration/getCourseList",
        data: null,
        success: function (response) {
            //console.log('getCourseList: ' + response);
            $('#ddlCourse').html(response);
        },
        error: function () {
        }
    });
}

function getEventList() {
    var course = $('#ddlCourse').val();
    //console.log('course: ' + course);
    if (course != '0') {
        $.ajax({
            type: "GET",
            url: "/umbraco/Surface/AdmissionEventRegistration/getEventsList",
            data: { "c_id": course },
            success: function (response) {
                //console.log('getEventsList: ' + response);
                $('#ddlEvent').html(response);
            },
            error: function () {
            }
        });
    }
}

function addRegistrationToEvent() {
    var ddlCourse = $('#ddlCourse').val();
    var txtFirstName = $('#txtFirstName').val();
    var txtMiddleName = $('#txtMiddleName').val();
    var txtLastName = $('#txtLastName').val();
    var txtDateofBirth = $('#txtDateofBirth').val();
    var txtEmail = $('#txtEmail').val();
    var txtMobile = $('#txtMobile').val();
    var txtCity = $('#txtCity').val();
    var ddlEvent = $('#ddlEvent').val();
    var txtApplicationNo = $('#txtApplicationNo').val();

    if (ddlCourse == '0') {
        alert('Please select the course');
        $('#ddlCourse').focus();
        return false;
    }
    if (txtFirstName == '') {
        alert('Please enter your first name');
        $('#txtFirstName').focus();
        return false;
    }
    if (txtFirstName.length < 3) {
        alert('Please enter valid first name');
        $('#txtFirstName').focus();
        return false;
    }
    if (txtDateofBirth == '') {
        alert('Please select your date of birth');
        $('#txtDateofBirth').focus();
        return false;
    }
    if (txtEmail == '') {
        alert('Please enter your email');
        $('#txtEmail').focus();
        return false;
    }
    if (!isValidEmailAddress(txtEmail)) {
        alert("Please enter valid email address");
        $('#txtEmail').focus();
        return false;
    }
    if (txtCity == '') {
        alert('Please enter your city');
        $('#txtCity').focus();
        return false;
    }
    if (ddlEvent == '0') {
        alert('Please select the event');
        $('#ddlEvent').focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "/umbraco/Surface/AdmissionEventRegistration/addRegistrationToEvent",
        data: { "Eventsid": ddlEvent, "fname": txtFirstName, "mname": txtMiddleName, "lname": txtLastName, "dob": txtDateofBirth, "city": txtCity, "email": txtEmail, "mobile": txtMobile, "appno": txtApplicationNo },
        success: function (response) {
            console.log("Event add: " + response);
            var arRes = response.split('|');
            if (arRes[0] == '1') {
                window.location.href = "/thankyou-event-registration?refno=" + arRes[1] + "&loc=" + arRes[2] + "&date=" + arRes[3] + "&course=" + arRes[4];
            }
        },
        error: function () {
        }
    });
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};


$(document).ready(function () {

    var refno = getUrlParameter('refno');
    if (refno == '') {
        $('#divFormContainer').show();
        getCourseList();
        var curDate = new Date();
        $("#txtDateofBirth").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date((curDate.getFullYear() - 16) + '-12-31'),
            minDate: new Date((curDate.getFullYear() - 90) + '-01-01')
        });
    }
    else {
        $('#divFormContainer').hide();
        var course = getUrlParameter('course');
        var loc = getUrlParameter('loc');
        var date = getUrlParameter('date');

        var divInnerContent = $('#divInnerContent').html();
        divInnerContent = divInnerContent.replace('@@refno@@', refno);
        divInnerContent = divInnerContent.replace('@@loc@@', loc);
        divInnerContent = divInnerContent.replace('@@date@@', date);
        divInnerContent = divInnerContent.replace('@@course@@', course);

        $('#divInnerContent').html(divInnerContent);
    }
});