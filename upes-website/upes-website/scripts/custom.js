
var swiper = new Swiper('.s3', {
  slidesPerView: 3,
  spaceBetween: 30,
   autoplay: {
      delay: 8000,
      disableOnInteraction: false,
   },
   navigation: {
    nextEl: '.swiper-button-next1',
    prevEl: '.swiper-button-prev1',
  },
   // Responsive breakpoints
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
    640: {
      slidesPerView: 1,
    },
    320: {
      slidesPerView: 1,
    }
  }
});


var swiper = new Swiper('.s1', {
  slidesPerView: 3,
  slidesPerColumn: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
   autoplay: {
      delay: 3500,
      disableOnInteraction: false,
   },
    // Responsive breakpoints
    breakpoints: {
      768: {
        slidesPerView: 2,
        slidesPerColumn: 2,
      },
      640: {
        slidesPerView: 1,
        slidesPerColumn: 1,
      },
      320: {
        slidesPerView: 1,
        slidesPerColumn: 1,
      }
    }
});

var Swiper = new Swiper('.s2', {
  slidesPerView: 3,
  spaceBetween: 30,
  autoHeight: true, //enable auto height
   autoplay: {
        delay: 8000,
        disableOnInteraction: false,
   },
   pagination: {
        el: '.swiper-pagination2',
      },
     
  // Responsive breakpoints
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
    640: {
      slidesPerView: 1,
    },
    320: {
      slidesPerView: 1,
    }
  }
});

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

// $('.panel-collapse').on('show.bs.collapse', function () {
//   $(this).siblings('.panel-heading').addClass('active');
// });

// $('.panel-collapse').on('hide.bs.collapse', function () {
//   $(this).siblings('.panel-heading').removeClass('active');
// });

$(window).scroll(function() {
  if ($(this).scrollTop() >= 150 ) {
      $('.scrolltop:hidden').stop(true, true).fadeIn();
  } else {
      $('.scrolltop').stop(true, true).fadeOut();
   }
});
$(function(){$(".scroll").click(function(){$("html,body").animate({scrollTop:$(".banner").offset().top},"1000");return false})})
