$('#health-medical-AccordionPanel-toggler').click(function () {
    $('#health-medical-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#health-medical-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#health-medical-AccordionPanel-toggler').text('Collapse all');

    }
});

$('#student-affairs-AccordionPanel-toggler').click(function () {
    $('#student-affairs-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#student-affairs-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#student-affairs-AccordionPanel-toggler').text('Collapse all');

    }
});

$('.ForExpand a').click(function () {
    $('.AccordionStyle .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('.ForExpand a').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('.ForExpand a').text('Collapse all');

    }
});

$('#INTERNATIONAL-ADMISSIONS-AccordionPanel-toggler').click(function () {
    $('#INTERNATIONAL-ADMISSIONS-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#INTERNATIONAL-ADMISSIONS-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#INTERNATIONAL-ADMISSIONS-AccordionPanel-toggler').text('Collapse all');

    }
});

$('#HELP-TOPICS-AccordionPanel-toggler').click(function () {
    $('#HELP-TOPICS-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#HELP-TOPICS-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#HELP-TOPICS-AccordionPanel-toggler').text('Collapse all');

    }
});

$('#working-upes-toggler').click(function () {
    $('#working-upes .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#working-upes-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#working-upes-toggler').text('Collapse all');

    }
});

$('#current-openings-AccordionPanel-toggler').click(function () {
    $('#current-openings-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#current-openings-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#current-openings-AccordionPanel-toggler').text('Collapse all');

    }
});

$('#Regulators-iqac-AccordionPanel-toggler').click(function () {
    $('#Regulators-iqac-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#Regulators-iqac-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#Regulators-iqac-AccordionPanel-toggler').text('Collapse all');

    }
});

$('#Regulators-rti-AccordionPanel-toggler').click(function () {
    $('#Regulators-rti-AccordionPanel .collapse').toggleClass('show');
    if ($(".CardHeaderPanel a").attr("aria-expanded") == "true") {
        $(".CardHeaderPanel a").attr("aria-expanded", "false");
        $('#Regulators-rti-AccordionPanel-toggler').text('Expand all');
    }
    else {
        $(".CardHeaderPanel a").attr("aria-expanded", "true");
        $('#Regulators-rti-AccordionPanel-toggler').text('Collapse all');

    }
});


