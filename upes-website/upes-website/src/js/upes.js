$('.CarouselforSlick').slick({
    dots: false,
    infinite: false,
    arrows: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll:3,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false,
                arrows: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }

    ]
});
/**OneBlockcarousel**/
$('.OneBlockCarousel').slick({
    dots: false,
    infinite: false,
    arrows: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll:1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false,
                arrows: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }

    ]
});
/**Companies List**/
$('.CarouselForCompanies').slick({
    dots: false,
    infinite: false,
    arrows: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false,
                arrows: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }

    ]
});
/**Courses**/
$('#CoursesCarousel').slick({
    dots: false,
    infinite: false,
    arrows: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false,
                arrows: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                dots: true,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }

    ]
});
$('.nav-pills a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
});

$('#navbarNav').click(function () {
    $('.CollapsePanel').toggleClass('show');
    $('.icon-bar').toggleClass('active');
    $('.MenuOverlay').toggle();
    $('body').toggleClass('hidden');
});
$('.carousel').carousel({
    interval: 6000
});
$(".ClickArrow").click(function () {
    $('html, body').animate({
        scrollTop: $("#PanelTwo").offset().top
    }, 1000);
});

