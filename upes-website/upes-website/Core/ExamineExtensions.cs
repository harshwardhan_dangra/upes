﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace upes_website.Core
{
    public static class ExamineExtensions
    {
        public static IEnumerable<IPublishedContent> ExamineAlias(string lookupAlias)
        {
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
            if (helper == null)
            {
                return null;
            }

            var searchCriteria = ExamineManager.Instance.DefaultSearchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            var query = searchCriteria.Field("nodeTypeAlias", lookupAlias);
            return ExamineManager.Instance.DefaultSearchProvider.Search(query.Compile()).Select((x) => helper.TypedContent(x.Id));
        }

        public static IEnumerable<IPublishedContent> ExamineAliasAndName(string lookupAlias, string lookupName)
        {
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
            if (helper == null)
            {
                return null;
            }

            var searchCriteria = ExamineManager.Instance.DefaultSearchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            var query = searchCriteria.Field("nodeTypeAlias", lookupAlias).And().Field("nodeName", lookupName);
            return ExamineManager.Instance.DefaultSearchProvider.Search(query.Compile()).Select((x) => helper.TypedContent(x.Id));
        }
    }
}
