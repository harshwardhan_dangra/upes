﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace upes_website.GUtils
{
    /// <summary>
    /// PROJECT: GUTILS
    /// DESCRIPTION: GENERIC UTILITIES THAT ARE USED IN MOST APPLICATIONS. EXAMPLE: MAIL, ERRORHANDLING, LOGGING, DAL, ETC.
    /// ARCHITECTURE: ARUN R (arun@epagemaker.com)
    /// AUTHORS: ARUN R (arun@epagemaker.com), LAKSHMI (lakshmi@epagemaker.com), MOHAN (mohan@epagemaker.com), RAJESH (rajesh@epagemaker.com)
    /// </summary>

    public class Mailer
    {
        public static string USERNAME = ConfigurationManager.AppSettings["mail_username"].ToString();
        public static string PASSWORD = ConfigurationManager.AppSettings["mail_password"].ToString();
        public static string SMTP_HOST = ConfigurationManager.AppSettings["mail_smtphost"].ToString();
        public static int SMTP_PORT = Convert.ToInt32(ConfigurationManager.AppSettings["mail_smtpport"].ToString());

        private string m_toname;
        private string m_toaddress;
        private string m_fromname;
        private string m_fromaddress;
        private string m_subject;
        private string m_body;
        private string m_cc;
        private string m_bcc;
        private bool m_ishtml;
        private string m_attachment, m_htmlurl;

        public Mailer()
        {
            m_toname = "";
            m_toaddress = "";
            m_fromname = "";
            m_fromaddress = "";
            m_subject = "";
            m_body = "";
            m_cc = "";
            m_bcc = "";
            m_ishtml = true;
            m_attachment = "";

        }

        #region

        public string attachment
        {
            set
            {
                m_attachment = value;
            }

            get
            {
                return m_attachment;
            }
        }
        public bool is_html
        {
            set
            {
                m_ishtml = value;
            }

            get
            {
                return m_ishtml;
            }
        }
        public string bcc
        {
            set
            {
                m_bcc = value;
            }

            get
            {
                return m_bcc;
            }
        }
        public string cc
        {
            set
            {
                m_cc = value;
            }

            get
            {
                return m_cc;
            }
        }
        public string body
        {
            set
            {
                m_body = value;
            }

            get
            {
                return m_body;
            }
        }
        public string subject
        {
            set
            {
                m_subject = value;
            }

            get
            {
                return m_subject;
            }
        }
        public string fromaddress
        {
            set
            {
                m_fromaddress = value;
            }

            get
            {
                return m_fromaddress;
            }
        }
        public string fromname
        {
            set
            {
                m_fromname = value;
            }

            get
            {
                return m_fromname;
            }
        }
        public string toaddress
        {
            set
            {
                m_toaddress = value;
            }

            get
            {
                return m_toaddress;
            }
        }
        public string toname
        {
            set
            {
                m_toname = value;
            }

            get
            {
                return m_toname;
            }
        }
        public string htmlurl
        {
            set
            {
                m_htmlurl = value;
            }

            get
            {
                return m_htmlurl;
            }
        }

        #endregion

        public string send()
        {
            string strBody = "";

            try
            {
                System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage();

                m.From = new MailAddress(m_fromaddress);
                m.To.Add(new MailAddress(m_toaddress));

                if (m_cc != null && m_cc != "")
                {
                    m.CC.Add(m_cc);
                }

                if (m_bcc != null && m_bcc != "")
                {
                    m.Bcc.Add(m_bcc);
                }

                if (m_attachment != null && m_attachment != "")
                {
                    m.Attachments.Add(new Attachment(m_attachment));
                }

                m.Subject = subject;
                m.IsBodyHtml = m_ishtml;
                m.Body = m_body;

                SmtpClient client = new SmtpClient();
                client.Host = SMTP_HOST;
                client.Port = SMTP_PORT;
                client.Credentials = new System.Net.NetworkCredential(USERNAME, PASSWORD);
                //client.EnableSsl = true;
                client.Send(m);
                return "1";
            }
            catch (Exception ex)
            {
                throw ex;
                return ex.Message.ToString();
            }
        }
    }
}
