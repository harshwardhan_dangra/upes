﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data;
using upes_website.Gutils;

namespace upes_website.GUtils
{
    public class Utils
    {
        string strOutput = "";
        DBUtils dbu = new DBUtils();
        public void addMetaTag(Page page, string TagName, string TagContent)
        {
            HtmlMeta tag = new HtmlMeta();
            tag.Name = TagName;
            tag.Content = TagContent;
            page.Header.Controls.Add(tag);
        }

        public string getRandomKey()
        {
            Random rn = new Random(50);
            return "rn" + rn.NextDouble().ToString().Replace(".", "VC");
        }
        public string CleanQueryString(string strParam) //to avoid SQL Injection
        {
            strParam = strParam.Replace("'", "''").Replace("\"\"", "").Replace(")", "").Replace("(", "").Replace("-", "").Replace("|", "").Replace(";", "");
            return strParam;
        }
        public string SqlFriendly(string value)
        {
            return value.Replace("'", "''");
        }
        public string CleanPageUrl(string strParam) //to avoid SQL Injection
        {
            strParam = strParam.Replace("'", "''").Replace("\"\"", "").Replace(")", "").Replace("(", "").Replace("declare", "").Replace("cast", "").Replace("|", "").Replace(";", "");
            return strParam;
        }
        public bool hasData(DataSet dsTemp)
        {
            if (dsTemp != null)
                if (dsTemp.Tables.Count > 0)
                    if (dsTemp.Tables[0].Rows.Count > 0)
                        return true;
            return false;
        }
    }
}
