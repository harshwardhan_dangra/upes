﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using upes_website.GUtils;

namespace upes_website.Gutils
{
    /// <summary>
    /// PROJECT: GUTILS
    /// DESCRIPTION: GENERIC UTILITIES THAT ARE USED IN MOST APPLICATIONS. EXAMPLE: MAIL, ERRORHANDLING, LOGGING, DAL, ETC.
    /// ARCHITECTURE: ARUN R (arun@epagemaker.com)
    /// AUTHORS: ARUN R (arun@epagemaker.com), LAKSHMI (lakshmi@epagemaker.com), MOHAN (mohan@epagemaker.com), RAJESH (rajesh@epagemaker.com)
    /// Modified by ATHI on 09-11-2011 for encrypting connection string.
    /// Add <appSettings> <add key="ConnectionEncryption" value="true"/> </appSettings> in web.config if you encrypt the connectionstring.
    /// </summary>

    public class DBUtils
    {
        Crypt iCrypt = new Crypt();
        string CONNECTION_NAME = "conAdmission";//Default connectionString name
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        SqlDataAdapter da;

        public DBUtils()
        {

        }
        public DBUtils(string ConnectionName)
        {
            CONNECTION_NAME = ConnectionName;
        }
        private string GetConnectionString()
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings[CONNECTION_NAME].ConnectionString;
                if (ConfigurationManager.AppSettings["ConnectionEncryption"] != null)
                {
                    bool ENCRYPTED = Convert.ToBoolean(ConfigurationManager.AppSettings["ConnectionEncryption"].ToString());
                    if (ENCRYPTED)
                    {
                        connectionString = iCrypt.Decrypt(connectionString);
                    }
                }
                return connectionString;
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }


        public void openCon()
        {
            try
            {
                string connectionString = GetConnectionString();
                con = new SqlConnection(connectionString);
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void closeCon()
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }

        //EXECUTE AND RETURN ID
        public string getValueOf(string fieldname, string qry)
        {
            openCon();
            try
            {
                cmd = new SqlCommand(qry, con);
                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    string res = dr[fieldname].ToString();
                    dr.Close();
                    return res;
                }
                else
                {
                    dr.Close();
                    return "";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                closeCon();

            }
        }

        //THIS IS USED TO EXECUTE ALL YOUR INSERT,UPDATE,DELETE QUERIES 
        public void executeNonQuery(string commandText)
        {
            //SqlConnection icon = new SqlConnection(ConfigurationSettings.AppSettings["conn"].ToString());
            //if (con.State != ConnectionState.Open)
            //        con.Open();
            openCon();
            SqlCommand sqlCommand = new SqlCommand(commandText, con);
            try
            {
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandTimeout = 900;
                sqlCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
            }
            catch (Exception ec)
            {
                string strOutput = ec.Message.ToString();
            }
            finally
            {
                sqlCommand.Dispose();
                closeCon();//lal
            }
        }

        public void executeNonQuery(bool isStoreProcedure, string commandText, SqlParameter[] commandParameters, out string strOutput)
        {
            openCon();
            // SqlConnection icon = new SqlConnection(ConfigurationManager.ConnectionStrings["educonnectdb"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(commandText, con);
            try
            {
                SqlParameter[] sqlParameterArr = commandParameters;
                for (int i = 0; i < sqlParameterArr.Length; i++)
                {
                    SqlParameter sqlParameter = sqlParameterArr[i];
                    if ((sqlParameter.Direction == ParameterDirection.InputOutput) && (sqlParameter.Value == null))
                    { sqlParameter.Value = DBNull.Value; }
                    sqlCommand.Parameters.Add(sqlParameter);
                }

                //if (icon.State != ConnectionState.Open)
                //{ icon.Open(); }//lak

                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                sqlCommand.ExecuteNonQuery();
                string strProcRes = sqlCommand.Parameters["@result"].Value.ToString();
                if (strProcRes == "0")
                {
                    strOutput = strProcRes;
                }
                else
                {
                    strOutput = strProcRes;
                }

                // icon.Close();
                sqlCommand.Dispose();
            }
            catch (Exception ec)
            {
                strOutput = ec.Message.ToString();
            }
            finally
            {
                // icon.Close();
                closeCon();
                sqlCommand.Dispose();
            }
        }


        public DataSet executeQuery(string qry)
        {
            try
            {
                openCon();
                DataSet ds;

                cmd = new SqlCommand(qry, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                closeCon();
            }
        }

        public void executeSP(bool isStoreProcedure, string commandText, SqlParameter[] commandParameters, out string strOutput)
        {
            //SqlConnection icon = new SqlConnection(ConfigurationManager.ConnectionStrings["educonnectdb"].ConnectionString);
            openCon();
            SqlCommand sqlCommand = new SqlCommand(commandText, con);

            try
            {
                SqlParameter[] sqlParameterArr = commandParameters;
                for (int i = 0; i < sqlParameterArr.Length; i++)
                {
                    SqlParameter sqlParameter = sqlParameterArr[i];
                    if ((sqlParameter.Direction == ParameterDirection.InputOutput) && (sqlParameter.Value == null))
                    { sqlParameter.Value = DBNull.Value; }
                    sqlCommand.Parameters.Add(sqlParameter);
                }

                //if (icon.State != ConnectionState.Open)
                //{ icon.Open(); }//lak

                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                SqlDataReader dr = sqlCommand.ExecuteReader();
                if (dr.Read())
                {
                    strOutput = dr.GetValue(0).ToString();
                }
                else
                {
                    strOutput = null;
                }
                dr.Close();
                //icon.Close();//lak
                sqlCommand.Dispose();
            }
            catch (Exception ec)
            {
                strOutput = ec.Message.ToString();
            }
            finally
            {
                //icon.Close();//lak
                closeCon();
                sqlCommand.Dispose();
            }
        }

        public DataSet executeSP(bool isStoreProcedure, string commandText, SqlParameter[] commandParameters)
        {
            try
            {
                openCon();
                SqlCommand sqlCommand = new SqlCommand(commandText, con);

                SqlParameter[] sqlParameterArr = commandParameters;
                for (int i = 0; i < sqlParameterArr.Length; i++)
                {
                    SqlParameter sqlParameter = sqlParameterArr[i];
                    if ((sqlParameter.Direction == ParameterDirection.InputOutput) && (sqlParameter.Value == null))
                    { sqlParameter.Value = DBNull.Value; }
                    sqlCommand.Parameters.Add(sqlParameter);
                }


                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(ds, "tb1");
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                closeCon();
            }
        }

        public SqlDataReader executeSP(bool isStoreProcedure, string commandText, SqlParameter[] commandParameters, bool shouldReturn)
        {
            openCon();
            SqlCommand sqlCommand = new SqlCommand(commandText, con);

            try
            {
                SqlParameter[] sqlParameterArr = commandParameters;
                for (int i = 0; i < sqlParameterArr.Length; i++)
                {
                    SqlParameter sqlParameter = sqlParameterArr[i];
                    if ((sqlParameter.Direction == ParameterDirection.InputOutput) && (sqlParameter.Value == null))
                    { sqlParameter.Value = DBNull.Value; }
                    sqlCommand.Parameters.Add(sqlParameter);
                }


                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                SqlDataReader dr = sqlCommand.ExecuteReader();

                return dr;
            }
            catch (Exception ec)
            {

                return null;
            }
        }

        public DataSet executeSP(bool isStoreProcedure, string commandText)
        {
            try
            {
                openCon();
                SqlCommand sqlCommand = new SqlCommand(commandText, con);

                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(ds, "tb1");
                closeCon();
                return ds;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
            }
        }

        public SqlDataReader executeReader(string qry)
        {
            openCon();
            try
            {
                cmd = new SqlCommand(qry, con);
                dr = cmd.ExecuteReader();
                return dr;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {

            }
            //return dr;//lak
        }

        public string addDelimiter(string exValue, string delimiter)
        {
            return exValue.Replace(delimiter, "#delimiter#") + delimiter;
        }

        public void fillDataGrid(DataGrid dg, string qry)
        {
            openCon();
            DataSet ds;
            cmd = new SqlCommand(qry, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);
            dg.DataSource = ds;
            dg.DataBind();
            closeCon();

        }

        public void fillGridView(GridView gv, string qry)
        {
            openCon();
            DataSet ds;
            cmd = new SqlCommand(qry, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);
            gv.DataSource = ds;
            gv.DataBind();
            closeCon();

        }
        public void fillGrid(Repeater gv, string qry)
        {
            openCon();
            DataSet ds;
            cmd = new SqlCommand(qry, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);
            gv.DataSource = ds;
            gv.DataBind();
            closeCon();
        }
        public void fillGrid(Repeater gv, string qry, bool paging, Repeater rptPage, int PageNumber)
        {
            openCon();
            SqlDataAdapter da = new SqlDataAdapter(qry, con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            PagedDataSource pds = new PagedDataSource();
            DataView dv = new DataView(dt);
            pds.DataSource = dv;
            pds.AllowPaging = true;
            pds.PageSize = 12;
            pds.CurrentPageIndex = PageNumber;

            if (pds.PageCount > 1)
            {
                rptPage.Visible = true;
                ArrayList pages = new ArrayList();
                for (int i = 0; i < pds.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPage.DataSource = pages;
                rptPage.DataBind();
            }
            else
            {
                rptPage.Visible = false;
            }
            gv.DataSource = pds;
            gv.DataBind();
            closeCon();
        }


        public DataSet DataAccess(string strQuery, out string strReturn)
        {
            SqlConnection iConnection = new SqlConnection(ConfigurationSettings.AppSettings["lntidpl"].ToString());
            SqlCommand cmd = new SqlCommand(strQuery, iConnection);
            cmd.CommandTimeout = 900;
            SqlParameter param = new SqlParameter();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = cmd;

            try
            {
                da.Fill(ds);
                strReturn = "Success";
                return ds;
            }
            catch (Exception iException)
            {
                strReturn = iException.Message.ToString();
                return null;
            }
        }

        public string ExecuteNonQuery(bool isStoreProcedure, string commandText, SqlParameter[] commandParameters, out string strOutput)
        {
            openCon();
            SqlCommand sqlCommand = new SqlCommand(commandText, con);
            try
            {
                SqlParameter[] sqlParameterArr = commandParameters;
                for (int i = 0; i < sqlParameterArr.Length; i++)
                {
                    SqlParameter sqlParameter = sqlParameterArr[i];
                    if ((sqlParameter.Direction == ParameterDirection.InputOutput) && (sqlParameter.Value == null))
                    { sqlParameter.Value = DBNull.Value; }
                    sqlCommand.Parameters.Add(sqlParameter);
                }

                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                sqlCommand.ExecuteNonQuery();
                strOutput = "success";
                sqlCommand.Dispose();
                closeCon();
                return strOutput;
            }
            catch (Exception ec)
            {
                strOutput = ec.Message.ToString();
                return strOutput;
            }
            finally
            {
                closeCon();
                sqlCommand.Dispose();
            }
        }
        public Int32 ExecuteNonQueryWithOutParameter(bool isStoreProcedure, string commandText, SqlParameter[] commandParameters, out string strOutput)
        {
            openCon();
            SqlCommand sqlCommand = new SqlCommand(commandText, con);
            try
            {
                SqlParameter[] sqlParameterArr = commandParameters;
                for (int i = 0; i < sqlParameterArr.Length; i++)
                {
                    SqlParameter sqlParameter = sqlParameterArr[i];
                    if ((sqlParameter.Direction == ParameterDirection.InputOutput) && (sqlParameter.Value == null))
                    { sqlParameter.Value = DBNull.Value; }
                    sqlCommand.Parameters.Add(sqlParameter);
                }
                if (isStoreProcedure)
                { sqlCommand.CommandType = CommandType.StoredProcedure; }
                else
                { sqlCommand.CommandType = CommandType.Text; }

                sqlCommand.CommandTimeout = 900;
                sqlCommand.ExecuteNonQuery();
                Int32 messageID = Convert.ToInt32(sqlCommand.Parameters[sqlParameterArr.Length - 1].Value.ToString());
                strOutput = "success";
                sqlCommand.Dispose();
                closeCon();
                return messageID;
            }
            catch (Exception ec)
            {
                strOutput = ec.Message.ToString();
                return 0;
            }

        }
    }
}