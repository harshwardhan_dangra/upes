﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace upes_website.GUtils
{
    /// <summary>
    /// PROJECT: GUTILS
    /// DESCRIPTION: GENERIC UTILITIES THAT ARE USED IN MOST APPLICATIONS. EXAMPLE: MAIL, ERRORHANDLING, LOGGING, DAL, ETC.
    /// ARCHITECTURE: ARUN R (arun@epagemaker.com)
    /// AUTHORS: ARUN R (arun@epagemaker.com), LAKSHMI (lakshmi@epagemaker.com), MOHAN (mohan@epagemaker.com), RAJESH (rajesh@epagemaker.com)
    /// </summary>

    public class ErrorHandler
    {
        public static string ERROR_FILE_PATH = ConfigurationManager.AppSettings["errorfilepath"].ToString();

        public ErrorHandler()
        { }

        public ErrorHandler(string error_file_path)
        {
            ERROR_FILE_PATH = error_file_path;
        }

        public static void log2(string entry)
        {
            Logger l = new Logger(ERROR_FILE_PATH);
            l.write(entry);
        }

        public void log(string entry)
        {
            Logger l = new Logger(ERROR_FILE_PATH);
            l.write(entry);
        }
    }
}
