﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;

namespace upes_website.GUtils
{
    /// <summary>
    /// PROJECT: GUTILS
    /// DESCRIPTION: GENERIC UTILITIES THAT ARE USED IN MOST APPLICATIONS. EXAMPLE: MAIL, ERRORHANDLING, LOGGING, DAL, ETC.
    /// ARCHITECTURE: ARUN R (arun@epagemaker.com)
    /// AUTHORS: ARUN R (arun@epagemaker.com), LAKSHMI (lakshmi@epagemaker.com), MOHAN (mohan@epagemaker.com), RAJESH (rajesh@epagemaker.com)
    /// </summary>

    public class Logger
    {
        public static string LOG_FILE_PATH = ConfigurationManager.AppSettings["logfilepath"].ToString();
        public static string LOG_EXTENSION = ".log";
        public static bool ENABLE_LOG = true;

        //SET LOG FILE SIZE
        //       MB    KB      B
        //5 MB = 5 * 1024 * 1024
        public static int MAX_LOGFILE_SIZE = 5 * 1024 * 1024; //IN MB

        //TESTED:??
        public Logger()
        {
        }

        //TESTED:??
        public Logger(string log_file_path)
        {
            LOG_FILE_PATH = log_file_path;
        }

        //TESTED:??
        public void write(string entry)
        {
            //THE LOG FILE NAME IS NOT OPTIONAL. CAN BE CHANGED HERE IF NEEDED.
            string logFile = LOG_FILE_PATH;

            //LOG CAN BE CONTROLLED BY THE GLOBAL VARIABLE "ENABLE_LOG"
            //LOG WILL BE GENERATED ONLY IF "ENABLE_LOG" IS SET TO TRUE
            if (!ENABLE_LOG)
            {
                return;
            }

            //IF LOG FILE PATH NOT EXISTS, CREATE
            if (!Directory.Exists(LOG_FILE_PATH))
            {
                Directory.CreateDirectory(LOG_FILE_PATH);
            }

            try
            {
                StreamWriter SW;
                if (!File.Exists(logFile))
                {
                    SW = File.CreateText(logFile);
                }
                else
                {
                    SW = File.AppendText(logFile);
                }

                SW.WriteLine(DateTime.Now.ToString() + " : " + entry);
                SW.Close();
            }
            catch (Exception er)
            {
                //LOG ERROR
            }
            finally
            {
                if (File.Exists(logFile))
                {
                    //IF LOG FILE SIZE IS MORE THAN THE SET LIMIT IT RENAMES THE FILE WITH TIMESTAMP
                    //AND CREATES A NEW LOG FILE
                    FileInfo fi = new FileInfo(logFile);
                    if (fi.Length > (MAX_LOGFILE_SIZE))
                    {
                        fi.MoveTo(logFile.Replace(LOG_EXTENSION, "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + LOG_EXTENSION));
                    }
                }
            }
        }
    }
}
