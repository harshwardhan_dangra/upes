﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace upes_website.Gutils
{
    public class Crypt
    {
        public string passPhrase = "up3SuN!v";        // can be any string
        public string saltValue = "s@1tV@lue";        // can be any string
        public string hashAlgorithm = "SHA1";             // can be "MD5"
        public int passwordIterations = 2;                  // can be any number
        public string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
        public int keySize = 256;                // can be 192 or 128

        public Crypt()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        // <summary>
        // Encrypts specified plaintext using Rijndael symmetric key algorithm
        // and returns a base64-encoded result.
        // </summary>
        // <param name="plainText">
        // Plaintext value to be encrypted.
        // </param>
        // <param name="passPhrase">
        // Passphrase from which a pseudo-random password will be derived. The 
        // derived password will be used to generate the encryption key. 
        // Passphrase can be any string. In this example we assume that this 
        // passphrase is an ASCII string.
        // </param>
        // <param name="saltValue">
        // Salt value used along with passphrase to generate password. Salt can 
        // be any string. In this example we assume that salt is an ASCII string.
        // </param>
        // <param name="hashAlgorithm">
        // Hash algorithm used to generate password. Allowed values are: "MD5" and
        // "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        // </param>
        // <param name="passwordIterations">
        // Number of iterations used to generate password. One or two iterations
        // should be enough.
        // </param>
        // <param name="initVector">
        // Initialization vector (or IV). This value is required to encrypt the 
        // first block of plaintext data. For RijndaelManaged class IV must be 
        // exactly 16 ASCII characters long.
        // </param>
        // <param name="keySize">
        // Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
        // Longer keys are more secure than shorter keys.
        // </param>
        // <returns>
        // Encrypted value formatted as a base64-encoded string.
        // </returns>
        //public string Encrypt(string plainText)
        //{
        //    // Convert strings into byte arrays.
        //    // Let us assume that strings only contain ASCII codes.
        //    // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
        //    // encoding.
        //    byte[] initVectorBytes = null;
        //    initVectorBytes = Encoding.ASCII.GetBytes(initVector);

        //    byte[] saltValueBytes = null;
        //    saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

        //    // Convert our plaintext into a byte array.
        //    // Let us assume that plaintext contains UTF8-encoded characters.
        //    byte[] plainTextBytes = null;
        //    plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        //    // First, we must create a password, from which the key will be derived.
        //    // This password will be generated from the specified passphrase and 
        //    // salt value. The password will be created using the specified hash 
        //    // algorithm. Password creation can be done in several iterations.
        //    PasswordDeriveBytes password = default(PasswordDeriveBytes);
        //    password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

        //    // Use the password to generate pseudo-random bytes for the encryption
        //    // key. Specify the size of the key in bytes (instead of bits).
        //    byte[] keyBytes = null;
        //    keyBytes = password.GetBytes(keySize / 8);

        //    // Create uninitialized Rijndael encryption object.
        //    RijndaelManaged symmetricKey = default(RijndaelManaged);
        //    symmetricKey = new RijndaelManaged();

        //    // It is reasonable to set encryption mode to Cipher Block Chaining
        //    // (CBC). Use default options for other symmetric key parameters.
        //    symmetricKey.Mode = CipherMode.CBC;

        //    // Generate encryptor from the existing key bytes and initialization 
        //    // vector. Key size will be defined based on the number of the key 
        //    // bytes.
        //    ICryptoTransform encryptor = default(ICryptoTransform);
        //    encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

        //    // Define memory stream which will be used to hold encrypted data.
        //    MemoryStream memoryStream = default(MemoryStream);
        //    memoryStream = new MemoryStream();

        //    // Define cryptographic stream (always use Write mode for encryption).
        //    CryptoStream cryptoStream = default(CryptoStream);
        //    cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        //    // Start encrypting.
        //    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

        //    // Finish encrypting.
        //    cryptoStream.FlushFinalBlock();

        //    // Convert our encrypted data from a memory stream into a byte array.
        //    byte[] cipherTextBytes = null;
        //    cipherTextBytes = memoryStream.ToArray();

        //    // Close both streams.
        //    memoryStream.Close();
        //    cryptoStream.Close();

        //    // Convert encrypted data into a base64-encoded string.
        //    string cipherText = null;
        //    cipherText = Convert.ToBase64String(cipherTextBytes);

        //    // Return encrypted string.
        //    return cipherText;
        //}

        // <summary>
        // Decrypts specified ciphertext using Rijndael symmetric key algorithm.
        // </summary>
        // <param name="cipherText">
        // Base64-formatted ciphertext value.
        // </param>
        // <param name="passPhrase">
        // Passphrase from which a pseudo-random password will be derived. The 
        // derived password will be used to generate the encryption key. 
        // Passphrase can be any string. In this example we assume that this 
        // passphrase is an ASCII string.
        // </param>
        // <param name="saltValue">
        // Salt value used along with passphrase to generate password. Salt can 
        // be any string. In this example we assume that salt is an ASCII string.
        // </param>
        // <param name="hashAlgorithm">
        // Hash algorithm used to generate password. Allowed values are: "MD5" and
        // "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        // </param>
        // <param name="passwordIterations">
        // Number of iterations used to generate password. One or two iterations
        // should be enough.
        // </param>
        // <param name="initVector">
        // Initialization vector (or IV). This value is required to encrypt the 
        // first block of plaintext data. For RijndaelManaged class IV must be 
        // exactly 16 ASCII characters long.
        // </param>
        // <param name="keySize">
        // Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
        // Longer keys are more secure than shorter keys.
        // </param>
        // <returns>
        // Decrypted string value.
        // </returns>
        // <remarks>
        // Most of the logic in this function is similar to the Encrypt 
        // logic. In order for decryption to work, all parameters of this function
        // - except cipherText value - must match the corresponding parameters of 
        // the Encrypt function which was called to generate the 
        // ciphertext.
        // </remarks>
        //public string Decrypt(string cipherText)
        //{

        //    // Convert strings defining encryption key characteristics into byte
        //    // arrays. Let us assume that strings only contain ASCII codes.
        //    // If strings include Unicode characters, use Unicode, UTF7, or UTF8
        //    // encoding.
        //    byte[] initVectorBytes = null;
        //    initVectorBytes = Encoding.ASCII.GetBytes(initVector);

        //    byte[] saltValueBytes = null;
        //    saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

        //    // Convert our ciphertext into a byte array.
        //    byte[] cipherTextBytes = null;
        //    cipherTextBytes = Convert.FromBase64String(cipherText);

        //    // First, we must create a password, from which the key will be 
        //    // derived. This password will be generated from the specified 
        //    // passphrase and salt value. The password will be created using
        //    // the specified hash algorithm. Password creation can be done in
        //    // several iterations.
        //    PasswordDeriveBytes password = default(PasswordDeriveBytes);
        //    password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

        //    // Use the password to generate pseudo-random bytes for the encryption
        //    // key. Specify the size of the key in bytes (instead of bits).
        //    byte[] keyBytes = null;
        //    keyBytes = password.GetBytes(keySize / 8);

        //    // Create uninitialized Rijndael encryption object.
        //    RijndaelManaged symmetricKey = default(RijndaelManaged);
        //    symmetricKey = new RijndaelManaged();

        //    // It is reasonable to set encryption mode to Cipher Block Chaining
        //    // (CBC). Use default options for other symmetric key parameters.
        //    symmetricKey.Mode = CipherMode.CBC;

        //    // Generate decryptor from the existing key bytes and initialization 
        //    // vector. Key size will be defined based on the number of the key 
        //    // bytes.
        //    ICryptoTransform decryptor = default(ICryptoTransform);
        //    decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);

        //    // Define memory stream which will be used to hold encrypted data.
        //    MemoryStream memoryStream = default(MemoryStream);
        //    memoryStream = new MemoryStream(cipherTextBytes);

        //    // Define memory stream which will be used to hold encrypted data.
        //    CryptoStream cryptoStream = default(CryptoStream);
        //    cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

        //    // Since at this point we don't know what the size of decrypted data
        //    // will be, allocate the buffer long enough to hold ciphertext;
        //    // plaintext is never longer than ciphertext.
        //    byte[] plainTextBytes = null;
        //    plainTextBytes = new byte[cipherTextBytes.Length + 1];

        //    // Start decrypting.
        //    int decryptedByteCount = 0;
        //    decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

        //    // Close both streams.
        //    memoryStream.Close();
        //    cryptoStream.Close();

        //    // Convert decrypted data into a string. 
        //    // Let us assume that the original plaintext string was UTF8-encoded.
        //    string plainText = null;
        //    plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);

        //    // Return decrypted string.
        //    return plainText;
        //}

        public string Encrypt(string plainText)
        {
            string cipherText = "";
            string salt = "aknpd";
            cipherText = AES_Encrypt(plainText, salt);
            return cipherText;
        }
        public string Decrypt(string cipherText)
        {
            string plainText = "";
            string salt = "aknpd";
            plainText = AES_Decrypt(cipherText, salt);
            return plainText;
        }
            public string EncryptForQueryString(string plainText)
        {
            string encText = Encrypt(plainText).Replace("&", "|||").Replace("+", "$$$").Replace("#", "^^^");
            return encText;
        }
        public string DecryptForQueryString(string encText)
        {
            string plainText = Decrypt(encText.Replace("|||", "&").Replace("$$$", "+").Replace("^^^", "#"));
            return plainText;
        }


        public static RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            RijndaelManaged Key = new RijndaelManaged();
            Key.Mode = CipherMode.CBC;
            Key.Padding = PaddingMode.PKCS7;
            Key.KeySize = 128;
            Key.BlockSize = 128;
            Key.Key = keyBytes;
            Key.IV = keyBytes;
            return Key;
        }

        public static byte[] AES_Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor().TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        public static byte[] AES_Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        public string AES_Encrypt(string plainText, string key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(AES_Encrypt(plainBytes, GetRijndaelManaged(key)));
        }

        public string AES_Decrypt(string encryptedText, string key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(AES_Decrypt(encryptedBytes, GetRijndaelManaged(key)));
        }
    }
}
