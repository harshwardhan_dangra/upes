﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;

namespace upes_website.Controller
{
    public class MediaAndPressController : SurfaceController
    {
        // GET: MediaAndPress
        public ActionResult Index()
        {
            var news = Umbraco.TypedContent(2920);
            var newsItems = news.Children;
            List<NewsMedia> allNews = new List<NewsMedia>();
            List<Year> allYear = new List<Year>();
            List<Month> allMonth = new List<Month>();
            foreach (var newsItem in newsItems)
            {
                if (newsItem.HasValue("publishedDate"))
                {
                    Year y = new Year();
                    var title = newsItem.GetPropertyValue("showcaseTitle");
                    DateTime publishedDate = newsItem.GetPropertyValue<DateTime>("publishedDate");
                    y.Years = publishedDate.Year;
                    allYear.Add(y);
                }

            }
            return View();
        }
        public ActionResult bindYear()
        {
            string htmlString = "";
            var news = Umbraco.TypedContent(2920);
            var newsItems = news.Children;
            List<Year> allYear = new List<Year>();
            List<Year> disYear = new List<Year>();

            foreach (var newsItem in newsItems)
            {
                if (newsItem.HasValue("publishedDate"))
                {
                    Year y = new Year();
                    allYear.Add(y);
                    DateTime publishedDate = newsItem.GetPropertyValue<DateTime>("publishedDate");
                    y.Years = publishedDate.Year;
                }

            }
            foreach (var year in allYear.OrderByDescending(x => x.Years).Select(x => x.Years).Distinct())
            {
                htmlString += "<option value='" + year + "'>" + year + "</option>";
            }
            return Content(htmlString);
        }
        public ActionResult bindMonth(string selectedYear)
        {
            string htmlString = "";
            var news = Umbraco.TypedContent(2920);
            var newsItems = news.Children;
            List<Month> allMonth = new List<Month>();
            foreach (var newsItem in newsItems)
            {
                if (newsItem.HasValue("publishedDate"))
                {
                    Month newMonth = new Month();
                    DateTime publishedDate = newsItem.GetPropertyValue<DateTime>("publishedDate");
                    if (publishedDate.Year.ToString() == selectedYear)
                    {
                        newMonth.Months = publishedDate;
                        allMonth.Add(newMonth);
                    }
                }

            }
            var monthSorting = allMonth.OrderByDescending(x => DateTime.ParseExact(Convert.ToDateTime(x.Months).ToString("MMMM"),"MMMM",new CultureInfo("en-US")));
            htmlString += "<option value='all'>ALL</option>";
            foreach (var month in monthSorting.Select(x => x.Months.ToString("MMMM")).Distinct())
            {
                htmlString += "<option value='" + month + "'>" + month + "</option>";
            }
            return Content(htmlString);
        }
        public ActionResult returnNews(string selectedYear, string selectedMonth)
        {
            string htmlString = "";
            var news = Umbraco.TypedContent(2920);
            var newsItems = news.Children;
            List<NewsMedia> allNews = new List<NewsMedia>();
            foreach (var newsItem in newsItems)
            {
                if (newsItem.HasValue("publishedDate"))
                {
                    NewsMedia newsList = new NewsMedia();
                    DateTime publishedDate = newsItem.GetPropertyValue<DateTime>("publishedDate");
                    if (newsItem.HasValue("showcaseTitle"))
                    {
                        newsList.Title = newsItem.GetPropertyValue<string>("showcaseTitle");
                    }
                    else
                    {
                        newsList.Title = "";
                    }
                    if (newsItem.HasValue("showcaseImage"))
                    {
                        newsList.imgPath = newsItem.GetPropertyValue<IPublishedContent>("showcaseImage");
                    }
                    else
                    {
                        newsList.imgPath = news.GetPropertyValue<IPublishedContent>("defaultNewsImage");
                    }
                    if (newsItem.HasValue("publishedDate"))
                    {
                        newsList.PublishedDate = newsItem.GetPropertyValue<DateTime>("publishedDate");
                    }
                    else
                    {
                        newsList.PublishedDate = DateTime.Now.Date;
                    }
                    if (newsItem.HasValue("shortDescription"))
                    {
                        newsList.Description = newsItem.GetPropertyValue<string>("shortDescription");
                    }
                    else
                    {
                        newsList.Description = "";
                    }
                    if (newsItem.HasValue("snippetPDF"))
                    {
                        newsList.MoreDetailSnippetPDF = newsItem.GetPropertyValue<IPublishedContent>("snippetPDF");
                    }
                    else if (newsItem.HasValue("snippetImage"))
                    {
                        newsList.MoreDetailSnippetImage = newsItem.GetPropertyValue<IPublishedContent>("snippetImage");
                    }
                    else if (newsItem.HasValue("knowmoreLink"))
                    {
                        newsList.ExternalLink = newsItem.GetPropertyValue<RelatedLinks>("knowmoreLink");
                    }
                    else
                    {
                        newsList.ExternalLink = null;
                    }
                    if (newsItem.HasValue("showcaseImage"))
                    {
                        newsList.imgAltText = newsItem.GetPropertyValue<IPublishedContent>("showcaseImage");
                    }
                    else
                    {
                        newsList.imgAltText = null;
                    }
                    //    newsList.Title = newsItem.GetPropertyValue<string>("showcaseTitle");
                    //newsList.imgPath = newsItem.GetPropertyValue<IPublishedContent>("showcaseImage").Url;
                    //newsList.PublishedDate = newsItem.GetPropertyValue<DateTime>("publishedDate");
                    //newsList.Description = newsItem.GetPropertyValue<string>("shortDescription");
                    //newsList.ClipImgPath = newsItem.GetPropertyValue<IPublishedContent>("knowmoreMedia").Url;
                    //newsList.imgAltText = newsItem.GetPropertyValue<IPublishedContent>("showcaseImage").GetPropertyValue<string>("altTag");
                    allNews.Add(newsList);
                }

            }
            var monthSorting = allNews.OrderByDescending(x => DateTime.ParseExact(Convert.ToDateTime(x.PublishedDate).ToString("MMMM"), "MMMM", new CultureInfo("en-US")));
            foreach(var allNew in monthSorting)
            {
                if (selectedMonth == "all" && allNew.PublishedDate.Year.ToString() == selectedYear)
                {
                    if (allNew.imgPath != null)
                    {
                        htmlString += "<div class='col-lg-4 col-sm-6 newList'> <div class='newsListInner'> <div class='newsImg'> <img src='" + allNew.imgPath.GetCropUrl("News_Temp_Crop") + "' class='img-fluid' alt='" + allNew.imgAltText + "'> </div><div class='newsContext'><p class='fontBold'> " + allNew.Title + " </p> ";
                    }
                    else
                    {
                        htmlString += "<div class='col-lg-4 col-sm-6 newList'> <div class='newsListInner'> <div class='newsImg'><img src='/media/2171/452x403.png?anchor=center&amp;mode=crop&amp;width=108&amp;height=151&amp;rnd=132183134640000000' class='img-fluid' alt='2622'></div><div class='newsContext'><p class='fontBold'> " + allNew.Title + " </p> ";
                    }
                    if (allNew.MoreDetailSnippetImage != null)
                    {
                        htmlString += "<a onClick='doOpeNewsImagePopup(&apos;" + allNew.MoreDetailSnippetImage.Url + "&apos;);' class='text-uppercase fontBold knowMoreStyle'> Know More </a> ";
                    }
                    if (allNew.MoreDetailSnippetPDF != null)
                    {
                        htmlString += "<a href='" + allNew.MoreDetailSnippetPDF.Url + "' target='_blank' class='text-uppercase fontBold knowMoreStyle'> Know More </a> ";
                    }
                    if (allNew.ExternalLink != null)
                    {
                        string newWindow = "";
                        if (allNew.ExternalLink.First().NewWindow)
                        {
                            newWindow = "_blank";
                        }
                        else
                        {
                            newWindow = "_self";
                        }
                        htmlString += "<a href='" + allNew.ExternalLink.First().Link + "' target='" + newWindow + "' class='text-uppercase fontBold knowMoreStyle'> " + allNew.ExternalLink.First().Caption + " </a> ";
                    }
                    htmlString += "</div></div></div>";
                }
                else if (allNew.PublishedDate.Year.ToString() == selectedYear && allNew.PublishedDate.ToString("MMMM") == selectedMonth)
                {
                    if (allNew.imgPath != null)
                    {
                        htmlString += "<div class='col-lg-4 col-sm-6 newList'> <div class='newsListInner'> <div class='newsImg'> <img src='" + allNew.imgPath.GetCropUrl("News_Temp_Crop") + "' class='img-fluid' alt='" + allNew.imgAltText + "'> </div><div class='newsContext'><p class='fontBold'> " + allNew.Title + " </p> ";
                    }
                    else
                    {
                        htmlString += "<div class='col-lg-4 col-sm-6 newList'> <div class='newsListInner'> <div class='newsImg'><img src='/media/2171/452x403.png?anchor=center&amp;mode=crop&amp;width=108&amp;height=151&amp;rnd=132183134640000000' class='img-fluid' alt='2622'></div><div class='newsContext'><p class='fontBold'> " + allNew.Title + " </p> ";
                    }
                    if (allNew.MoreDetailSnippetImage != null)
                    {
                        htmlString += "<a onClick='doOpeNewsImagePopup(&apos;" + allNew.MoreDetailSnippetImage.Url + "&apos;);' class='text-uppercase fontBold knowMoreStyle'> Know More </a> ";
                    }
                    if (allNew.MoreDetailSnippetPDF != null)
                    {
                        htmlString += "<a href='" + allNew.MoreDetailSnippetPDF.Url + "' target='_blank' class='text-uppercase fontBold knowMoreStyle'> Know More </a> ";
                    }
                    if (allNew.ExternalLink != null)
                    {
                        string newWindow = "";
                        if (allNew.ExternalLink.First().NewWindow)
                        {
                            newWindow = "_blank";
                        }
                        else
                        {
                            newWindow = "_self";
                        }
                        htmlString += "<a href='" + allNew.ExternalLink.First().Link + "' target='" + newWindow + "' class='text-uppercase fontBold knowMoreStyle'> " + allNew.ExternalLink.First().Caption + " </a> ";
                    }
                    htmlString += "</div></div></div>";
                    //if (allNews[i].imgPath != null)
                    //{
                    //    htmlString += "<div class='col-md-4 EventPanelOne  plr7'><img src = '" + allNews[i].imgPath.GetCropUrl("News_Temp_Crop") + "' alt = '" + allNews[i].imgAltText + "'class='img-fluid' /><div class='AchieveContext '><div class='AchieveContextInn '><p class='fontBold'>" + allNews[i].Title + "</p>";
                    //    if (allNews[i].ClipImgPath != null)
                    //    {
                    //        htmlString += "<a onClick='doOpeNewsImagePopup(&apos;" + allNews[i].ClipImgPath.First().Link + "&apos;);' class='text-uppercase fontBold knowMoreStyle'>" + allNews[i].ClipImgPath.First().Caption + "</a>";
                    //    }
                    //    htmlString += "</div></div></div>";
                    //}
                }
            }
            for (int i = 0; i < allNews.Count; i++)
            {
                
            }
            return Content(htmlString);
        }
        public ActionResult GetAllNews()
        {
            string newsData = "";
            int newsCount = 0;
            try
            {
                var news = Umbraco.TypedContent(2620);
                var newsItems = news.Children.OrderByDescending(x => x.GetPropertyValue<DateTime>("publishedDate")); //OrderBy(x => x.GetPropertyValue<DateTime>("publishedDate"));

                foreach (var newsItem in newsItems)
                {
                    if (newsItem.HasValue("publishedDate"))
                    {
                        newsData += "{";
                        if (newsItem.HasValue("showcaseTitle"))
                        {
                            newsData += "\"title\":\"" + newsItem.GetPropertyValue<string>("showcaseTitle") + "\",";
                        }
                        else
                        {
                            newsData += "\"title\":\"\",";
                        }
                        if (newsItem.HasValue("shortDescription"))
                        {
                            newsData += "\"short_description\":\"" + newsItem.GetPropertyValue<string>("shortDescription") + "\",";
                        }
                        else
                        {
                            newsData += "\"short_description\":\"\",";
                        }
                        if (newsItem.HasValue("detailContent"))
                        {
                            newsData += "\"description\":\"" + newsItem.GetPropertyValue<string>("detailContent").Trim() + "\",";
                        }
                        else
                        {
                            newsData += "\"description\":\"\",";
                        }

                        if (newsItem.HasValue("publishedDate"))
                        {
                            newsData += "\"date\":\"" + newsItem.GetPropertyValue<DateTime>("publishedDate").ToString("dd/MM/yyyy HH:mm") + "\",";
                        }
                        else
                        {
                            newsData += "\"date\":\"\",";
                        }

                        if (newsItem.HasValue("thumbImage"))
                        {
                            newsData += "\"img\":\"https://www.upes.ac.in" + newsItem.GetPropertyValue<IPublishedContent>("thumbImage").Url + "\"";
                        }
                        else
                        {
                            newsData += "\"img\":\"\"";
                        }

                        newsData += "},";
                        newsCount++;
                    }
                }
                if (newsData != "")
                {
                    newsData = newsData.Substring(0, (newsData.Length - 1));
                }
                if (newsCount > 0)
                {
                    newsData = "{\"status\": \"OK\", \"message\": \"success\",\"list\": [" + newsData + "]}";
                }
                else
                {
                    newsData = "{\"status\": \"OK\", \"message\": \"no news data found\",\"list\": []}";
                }
            }
            catch (Exception exe)
            {
                newsData = "{\"status\": \"ERROR\", \"message\": \"" + exe.ToString().Replace("\\", "") + "\",\"list\": []}";
            }
            return Content(newsData);
        }
    }
}