﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using upes_website.Gutils;

namespace upes_website.Controller
{
    public class StudentController : SurfaceController
    {
        DBUtils objDBU = new DBUtils();

        public void getStudentDetails(int studentID, out string courseName, out int inputStatus)
        {
            SqlParameter[] Parms = new SqlParameter[3];
            Parms[0] = new SqlParameter("@studentID", SqlDbType.Int);
            Parms[0].Value = studentID;
            Parms[1] = new SqlParameter("@course_name", SqlDbType.VarChar, 256);
            Parms[1].Direction = ParameterDirection.Output;
            Parms[1].Value = "";
            Parms[2] = new SqlParameter("@result", SqlDbType.Int);
            Parms[2].Direction = ParameterDirection.Output;
            Parms[2].Value = 0;
            objDBU.executeSP(true, "sp_validate_studentId", Parms);

            courseName = Parms[1].Value.ToString();
            inputStatus = Convert.ToInt32(Parms[2].Value.ToString());
        }

        public void validateAccessToken(string accessToken, out int inputStatus)
        {
            SqlParameter[] Parms = new SqlParameter[2];
            Parms[0] = new SqlParameter("@accessToken", SqlDbType.VarChar, 100);
            Parms[0].Value = accessToken;
            Parms[1] = new SqlParameter("@result", SqlDbType.Int);
            Parms[1].Direction = ParameterDirection.Output;
            Parms[1].Value = 0;
            objDBU.executeSP(true, "sp_validate_accessToken", Parms);

            inputStatus = Convert.ToInt32(Parms[1].Value.ToString());
        }
        public void validateAccessTokenAndProgramId(string accessToken, int programID, out string courseName, out int inputStatus)
        {
            SqlParameter[] Parms = new SqlParameter[4];
            Parms[0] = new SqlParameter("@accessToken", SqlDbType.VarChar, 100);
            Parms[0].Value = accessToken;
            Parms[1] = new SqlParameter("@programID", SqlDbType.Int);
            Parms[1].Value = programID;
            Parms[2] = new SqlParameter("@courseName", SqlDbType.VarChar, 50);
            Parms[2].Direction = ParameterDirection.Output;
            Parms[2].Value = "";
            Parms[3] = new SqlParameter("@result", SqlDbType.Int);
            Parms[3].Direction = ParameterDirection.Output;
            Parms[3].Value = 0;
            objDBU.executeSP(true, "sp_validate_accessTokenAndProgramId", Parms);

            courseName = Parms[2].Value.ToString();
            inputStatus = Convert.ToInt32(Parms[3].Value.ToString());
        }
        public string removeHTML(string input)
        {
            try
            {
                // regex which match tags
                System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("<[^>]*>");
                // replace all matches with empty strin
                input = rx.Replace(input, "");
            }
            catch
            {
                input = "";
            }

            return input;
        }
    }
}