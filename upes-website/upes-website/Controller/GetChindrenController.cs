﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace upes_website.Controller
{
    public class GetChindrenController : SurfaceController
    {
        // GET: GetChindren
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult child(string alias)
        {
            //var children = upes_website.Core.ExamineExtensions.ExamineAliasAndName("courses", alias);
            //IPublishedContent childcourseContent = children.Any() ? children.First() : null;
            var result = upes_website.Core.ExamineExtensions.ExamineAlias("financialAssistance");
            IPublishedContent coursesContent = result.Any() ? result.First() : null;
            string alis = "";
            foreach (var child in coursesContent.Children)
            {
                if (child.Name == alias)
                {
                    alis = child.Id.ToString();
                }
            }
            //var course = upes_website.Core.ExamineExtensions.ExamineAlias(alis);
            //IPublishedContent courseChild = course.Any() ? course.First() : null;
            //var courseChild = Umbraco.TypedContent(Convert.ToInt32(alis));
            //List<Dictionary<string, string>> courseNamelist = new List<Dictionary<string, string>>();
            //foreach (var courses in courseChild.Children)
            //{
            //    Dictionary<string, string> coursedic = new Dictionary<string, string>();
            //    coursedic.Add("Id", courses.Id.ToString());
            //    coursedic.Add("Name", courses.Name);
            //    //courseName += "<option value=\"" + courses.Id + "\">" + courses.Name + "</option>";
            //    courseNamelist.Add(coursedic);

            //}
            var courseChild = Umbraco.TypedContent(Convert.ToInt32(alis));
            string courseName = "";
            foreach (var courses in courseChild.Children)
            {
                courseName += "<option value=\"" + courses.Id + "\">" + courses.Name + "</option>";
            }
            return Content(courseName);
            //var children = JsonConvert.SerializeObject(courseNamelist);
            //return Content(children);
        }
    }
}