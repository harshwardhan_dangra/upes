﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using upes_website.Gutils;

namespace upes_website.Controller
{
    public class AdmissionEventRegistrationController : SurfaceController
    {
        DBUtils dbu = new DBUtils();

        [HttpGet]
        public string getCourseList()
        {
            string courseList = "";
            DataSet ds = dbu.executeQuery("select c_id,c_name from STBL_COURSE where status = 1 and c_id in (select c_id from specialevent where status = 1 and eventsdate >= getdate())");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    courseList += "<option value=\"" + ds.Tables[0].Rows[i]["c_id"].ToString() + "\">" + ds.Tables[0].Rows[i]["c_name"].ToString() + "</option>";
                }
                courseList = "<option value=\"0\">Applying for Course*</option>" + courseList;
            }
            else
            {
                courseList = "<option value=\"0\">No Upcoming Registrations</option>" + courseList;
            }
            return courseList;
        }

        [HttpGet]
        public string getEventsList(string c_id)
        {
            string eventsList = "";
            DataSet ds = dbu.executeQuery("select e.eventsid, e.eventstitle + ' ['+e.location+' - '+CONVERT(VARCHAR(10), e.eventsdate, 103)+' '+e.timeslot+']' as 'events' from specialevent e join STBL_COURSE c on e.c_id = c.c_id where e.status = 1 and c.STATUS = 1 and e.eventsdate >= getdate() and e.c_id = " + c_id);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    eventsList += "<option value=\"" + ds.Tables[0].Rows[i]["eventsid"].ToString() + "\">" + ds.Tables[0].Rows[i]["events"].ToString() + "</option>";
                }
                eventsList = "<option value=\"0\">Select Event*</option>" + eventsList;
            }
            else
            {
                eventsList = "<option value=\"0\">No Events to list</option>" + eventsList;
            }
            return eventsList;
        }

        [HttpGet]
        public string addRegistrationToEvent(Int32 Eventsid, string fname, string mname, string lname, string dob, string city, string email, string mobile, string appno)
        {
            string result = "";
            try
            {
                DateTime dtDOB = new DateTime();
                string[] arDOB = dob.Split('/');
                dtDOB = new DateTime(Convert.ToInt32(arDOB[2]), Convert.ToInt32(arDOB[0]), Convert.ToInt32(arDOB[1]));
                SqlParameter[] Parms = new SqlParameter[17];
                Parms[0] = new SqlParameter("@everegid", SqlDbType.Int);
                Parms[0].Direction = ParameterDirection.Output;
                Parms[0].Value = 0;
                Parms[1] = new SqlParameter("@Eventsid", SqlDbType.Int);
                Parms[1].Value = Eventsid;
                Parms[2] = new SqlParameter("@fname", SqlDbType.VarChar, 200);
                Parms[2].Value = fname;
                Parms[3] = new SqlParameter("@mname", SqlDbType.VarChar, 200);
                Parms[3].Value = mname;
                Parms[4] = new SqlParameter("@lname", SqlDbType.VarChar, 200);
                Parms[4].Value = lname;
                Parms[5] = new SqlParameter("@dob", SqlDbType.DateTime);
                Parms[5].Value = dtDOB;
                Parms[6] = new SqlParameter("@city", SqlDbType.VarChar, 200);
                Parms[6].Value = city;
                Parms[7] = new SqlParameter("@email", SqlDbType.VarChar, 200);
                Parms[7].Value = email;
                Parms[8] = new SqlParameter("@mobile", SqlDbType.VarChar, 200);
                Parms[8].Value = mobile;
                Parms[9] = new SqlParameter("@status", SqlDbType.VarChar, 50);
                Parms[9].Value = "1";
                Parms[10] = new SqlParameter("@refno", SqlDbType.Int);
                Parms[10].Value = 0;
                Parms[11] = new SqlParameter("@appno", SqlDbType.VarChar, 200);
                Parms[11].Value = appno;
                Parms[12] = new SqlParameter("@uname", SqlDbType.VarChar, 20);
                Parms[12].Value = "user";
                Parms[13] = new SqlParameter("@date", SqlDbType.VarChar);
                Parms[13].Value = "";
                Parms[14] = new SqlParameter("@time", SqlDbType.VarChar);
                Parms[14].Value = "";
                Parms[15] = new SqlParameter("@skypeid", SqlDbType.VarChar);
                Parms[15].Value = "";
                Parms[16] = new SqlParameter("@Mode", SqlDbType.Int);
                Parms[16].Value = 1;

                dbu.executeSP(true, "eventregistrationSP", Parms);

                if (Convert.ToInt32(Parms[0].Value) > 0)
                {
                    result = "1";
                    DataSet dsRes = dbu.executeQuery("select er.everegid, er.refno, convert(varchar,se.eventsdate,103) as eventdate, c.cityname, course.c_name from eventregistration er join specialevent se on er.eventsid = se.eventsid join STBL_CITY c on c.city_id = se.cityid join STBL_COURSE course on course.c_id = se.c_id where er.everegid=" + Parms[0].Value);
                    if (dsRes != null && dsRes.Tables.Count > 0 && dsRes.Tables[0].Rows.Count > 0)
                    {
                        result += "|" + dsRes.Tables[0].Rows[0]["refno"] + "|" + dsRes.Tables[0].Rows[0]["cityname"] + "|" + dsRes.Tables[0].Rows[0]["eventdate"] + "|" + dsRes.Tables[0].Rows[0]["c_name"];
                    }
                    else
                    {
                        result = "-1";
                    }
                }
                else
                {
                    result = "0";
                }
            }
            catch (Exception exe)
            {
                result = "error";
            }
            return result;
        }
    }
}