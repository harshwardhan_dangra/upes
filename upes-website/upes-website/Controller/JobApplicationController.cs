﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using upes_website.Gutils;

namespace upes_website.Controller
{
    public class JobApplicationController : SurfaceController
    {
        DBUtils dbu = new DBUtils();

        [HttpGet]
        public string getCountry()
        {
            string countryList = "";
            DataSet ds = dbu.executeQuery("select countrycode,countryname from country where status = 1 order by countryname");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    countryList += "<option value=\"" + ds.Tables[0].Rows[i]["countryname"].ToString() + "\">" + ds.Tables[0].Rows[i]["countryname"].ToString() + "</option>";
                }
                countryList = "<option value=\"0\">Select your country</option>" + countryList;
            }
            return countryList;
        }

        [HttpPost]
        public string ResumeCheck()
        {
            string result = "";
            // Get the uploaded image from the Files collection
            var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadedCV"].FileName.ToString();
            string extension = httpPostedFile.Substring(httpPostedFile.IndexOf('.') + 1);
            string PostedFileName = DateTime.Now.Ticks.ToString() + "." + extension;

            if (httpPostedFile != null)
            {
                var fileSavePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/files/resume"), PostedFileName);

                // Save the uploaded file to "UploadedFiles" folder
                System.Web.HttpContext.Current.Request.Files["UploadedCV"].SaveAs(fileSavePath);
                Session["uploadedCV"] = PostedFileName;
            }

            return PostedFileName;
        }

        [HttpGet]
        public string AddJobApplicationDetails(int Jobid, string Jobcategory, string designation, string FName, string LName, DateTime App_DOB, string Gender, string MaritalStatus, string Father_HusbandName, string App_Address, string Telephone, string Mobile, string App_Email, string Country, string State, string City, int App_Expyear, int App_Expmonth, string tenboard, string tenper, string interboard, string interper, string grdboard, string grdper, string pgrdboard, string pgrdper, string othboard, string othper, string App_Skills, string cemployer, string csalary, string cindustry, string funarea, string plocation, string Areaofintrst, string AttachCV)
        {
            string ten = "", inter = "", graduation = "", postgraduation = "", other = "";
            if (tenboard != "")
                ten = "Highschool";
            if (interboard != "")
                inter = "Intermediate";
            if (grdboard != "")
                graduation = "Gradution";
            if (pgrdboard != "")
                postgraduation = "Post Gradution";
            if (othboard != "")
                other = "Other";

            //SqlParameter[] Parms = new SqlParameter[44];
            //Parms[0] = new SqlParameter("@Jobid", SqlDbType.Int);
            //Parms[0].Value = Jobid;
            //Parms[1] = new SqlParameter("@Jobcategory", SqlDbType.VarChar, 50);
            //Parms[1].Value = Jobcategory;
            //Parms[2] = new SqlParameter("@designation", SqlDbType.VarChar, 50);
            //Parms[2].Value = designation;
            //Parms[3] = new SqlParameter("@FName", SqlDbType.VarChar, 50);
            //Parms[3].Value = FName;
            //Parms[4] = new SqlParameter("@LName", SqlDbType.VarChar, 50);
            //Parms[4].Value = LName;
            //Parms[5] = new SqlParameter("@App_DOB", SqlDbType.DateTime);
            //Parms[5].Value = App_DOB;
            //Parms[6] = new SqlParameter("@Gender", SqlDbType.VarChar, 10);
            //Parms[6].Value = Gender;
            //Parms[7] = new SqlParameter("@MaritalStatus", SqlDbType.VarChar, 50);
            //Parms[7].Value = MaritalStatus;
            //Parms[8] = new SqlParameter("@Father_HusbandName", SqlDbType.VarChar, 50);
            //Parms[8].Value = Father_HusbandName;
            //Parms[9] = new SqlParameter("@App_Address", SqlDbType.VarChar, 50);
            //Parms[9].Value = App_Address;
            //Parms[10] = new SqlParameter("@Telephone", SqlDbType.VarChar, 50);
            //Parms[10].Value = Telephone;
            //Parms[11] = new SqlParameter("@Mobile", SqlDbType.VarChar, 50);
            //Parms[11].Value = Mobile;
            //Parms[12] = new SqlParameter("@App_Email", SqlDbType.VarChar, 50);
            //Parms[12].Value = App_Email;
            //Parms[13] = new SqlParameter("@City", SqlDbType.VarChar, 50);
            //Parms[13].Value = City;
            //Parms[14] = new SqlParameter("@State", SqlDbType.VarChar, 50);
            //Parms[14].Value = State;
            //Parms[15] = new SqlParameter("@App_Qualification", SqlDbType.VarChar, 100);
            //Parms[15].Value = "";
            //Parms[16] = new SqlParameter("@App_Expyear", SqlDbType.Int);
            //Parms[16].Value = App_Expyear;
            //Parms[17] = new SqlParameter("@App_Expmonth", SqlDbType.Int);
            //Parms[17].Value = App_Expmonth;
            //Parms[18] = new SqlParameter("@App_Skills", SqlDbType.VarChar, 500);
            //Parms[18].Value = App_Skills;
            //Parms[19] = new SqlParameter("@AttachCV", SqlDbType.VarChar, 100);
            //Parms[19].Value = AttachCV;
            //Parms[20] = new SqlParameter("@funarea", SqlDbType.VarChar, 100);
            //Parms[20].Value = funarea;
            //Parms[21] = new SqlParameter("@cindustry", SqlDbType.VarChar, 500);
            //Parms[21].Value = cindustry;
            //Parms[22] = new SqlParameter("@plocation", SqlDbType.VarChar, 100);
            //Parms[22].Value = plocation;
            //Parms[23] = new SqlParameter("@cemployer", SqlDbType.VarChar, 500);
            //Parms[23].Value = cemployer;
            //Parms[24] = new SqlParameter("@csalary", SqlDbType.VarChar, 50);
            //Parms[24].Value = csalary;
            //Parms[25] = new SqlParameter("@Uname", SqlDbType.VarChar, 50);
            //Parms[25].Value = "User";
            //Parms[26] = new SqlParameter("@Country", SqlDbType.VarChar, 100);
            //Parms[26].Value = Country;
            //Parms[27] = new SqlParameter("@Areaofintrst", SqlDbType.VarChar);
            //Parms[27].Value = Areaofintrst;
            //Parms[28] = new SqlParameter("@ten", SqlDbType.VarChar, 100);
            //Parms[28].Value = ten;
            //Parms[29] = new SqlParameter("@tenboard", SqlDbType.VarChar, 500);
            //Parms[29].Value = tenboard;
            //Parms[30] = new SqlParameter("@tenper", SqlDbType.VarChar, 50);
            //Parms[30].Value = tenper;
            //Parms[31] = new SqlParameter("@inter", SqlDbType.VarChar, 100);
            //Parms[31].Value = inter;
            //Parms[32] = new SqlParameter("@interboard", SqlDbType.VarChar, 500);
            //Parms[32].Value = interboard;
            //Parms[33] = new SqlParameter("@interper", SqlDbType.VarChar, 50);
            //Parms[33].Value = interper;
            //Parms[34] = new SqlParameter("@graduation", SqlDbType.VarChar, 100);
            //Parms[34].Value = graduation;
            //Parms[35] = new SqlParameter("@grdboard", SqlDbType.VarChar, 500);
            //Parms[35].Value = grdboard;
            //Parms[36] = new SqlParameter("@grdper", SqlDbType.VarChar, 50);
            //Parms[36].Value = grdper;
            //Parms[37] = new SqlParameter("@postgraduation", SqlDbType.VarChar, 100);
            //Parms[37].Value = postgraduation;
            //Parms[38] = new SqlParameter("@pgrdboard", SqlDbType.VarChar, 500);
            //Parms[38].Value = pgrdboard;
            //Parms[39] = new SqlParameter("@pgrdper", SqlDbType.VarChar, 50);
            //Parms[39].Value = pgrdper;
            //Parms[40] = new SqlParameter("@other", SqlDbType.VarChar, 100);
            //Parms[40].Value = other;
            //Parms[41] = new SqlParameter("@othboard", SqlDbType.VarChar, 500);
            //Parms[41].Value = othboard;
            //Parms[42] = new SqlParameter("@othper", SqlDbType.VarChar, 50);
            //Parms[42].Value = othper;
            //Parms[43] = new SqlParameter("@result", SqlDbType.Int);
            //Parms[43].Direction = ParameterDirection.Output;
            //Parms[43].Value = 0;

            //dbu.executeSP(true, "sp_insert_job_application", Parms);
            string result = "";
            //if (Convert.ToInt32(Parms[43].Value) > 0)
            //{
            //    result = "1";
            //}
            //else
            //{
            //    result = "0";
            //}


            return result;
        }
    }
}