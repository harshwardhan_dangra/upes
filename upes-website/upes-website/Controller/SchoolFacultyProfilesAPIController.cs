﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace upes_website.Controller
{
    public class SchoolFacultyProfilesAPIController : SurfaceController
    {
        // GET: SchoolFacultyProfilesAPI
        StudentController objStu = new StudentController();
        public ActionResult Index()
        {
            string FacultyListData = "";
            int facultyListDataID = 0;
            FacultyListData += "[";
            try
            {
                var schoolsOfLawFaculty = Umbraco.TypedContent(4846);
                var schoolOfLawFacultyPicker = schoolsOfLawFaculty.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                if (schoolOfLawFacultyPicker.Count() != 0)
                {
                    FacultyListData += "{\"schoolName\":\"Schools Of Law\",";
                    FacultyListData += "\"Profiles\":[";
                    foreach (var schoolsOfLawProfiles in schoolOfLawFacultyPicker)
                    {
                        string comma = ",";
                        if (schoolOfLawFacultyPicker.Last() == schoolsOfLawProfiles)
                        {
                            comma = "";
                        }
                        FacultyListData += "{";
                        if (schoolsOfLawProfiles.HasValue("studentName"))
                        {
                            FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                        }
                        else
                        {
                            FacultyListData += "\"name\":\"\",";
                        }
                        if (schoolsOfLawProfiles.HasValue("studentDegree"))
                        {
                            FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                        }
                        else
                        {
                            FacultyListData += "\"departmentName\":\"\",";
                        }
                        if (schoolsOfLawProfiles.HasValue("pageContent"))
                        {
                            FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"","'")).Trim() + "\",";
                        }
                        else
                        {
                            FacultyListData += "\"shortDescription\":\"\",";
                        }
                        if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                        {
                            FacultyListData += "\"imageLink\":\"https://www.upes.ac.in" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500,667) + "\"";
                        }
                        else
                        {
                            FacultyListData += "\"imageLink\":\"\"";
                        }
                        FacultyListData += "}" + comma + "";
                    }
                    FacultyListData += "]";
                    FacultyListData += "},";
                    var schoolsOfComputerFaculty = Umbraco.TypedContent(4907);
                    var schoolOfComputerFacultyPicker = schoolsOfComputerFaculty.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                    if (schoolOfLawFacultyPicker.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Computer Science\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsOfLawProfiles in schoolOfComputerFacultyPicker)
                        {
                            string comma = ",";
                            if (schoolOfComputerFacultyPicker.Last() == schoolsOfLawProfiles)
                            {
                                comma = "";
                            }
                            FacultyListData += "{";
                            if (schoolsOfLawProfiles.HasValue("studentName"))
                            {
                                FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"name\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentDegree"))
                            {
                                FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"departmentName\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("pageContent"))
                            {
                                FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"shortDescription\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                            {
                                FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                            }
                            else
                            {
                                FacultyListData += "\"imageLink\":\"\"";
                            }
                            FacultyListData += "}" + comma + "";
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyDesign = Umbraco.TypedContent(5032);
                    var schoolsFacultyDesignPicker = schoolsFacultyDesign.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                    if (schoolsFacultyDesignPicker.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Design\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsOfLawProfiles in schoolsFacultyDesignPicker)
                        {
                            string comma = ",";
                            if (schoolsFacultyDesignPicker.Last() == schoolsOfLawProfiles)
                            {
                                comma = "";
                            }
                            FacultyListData += "{";
                            if (schoolsOfLawProfiles.HasValue("studentName"))
                            {
                                FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"name\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentDegree"))
                            {
                                FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"departmentName\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("pageContent"))
                            {
                                FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"shortDescription\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                            {
                                FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                            }
                            else
                            {
                                FacultyListData += "\"imageLink\":\"\"";
                            }
                            FacultyListData += "}" + comma + "";
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyEngineering = Umbraco.TypedContent(4643);
                    if (schoolsFacultyEngineering.Children.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Engineering\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsFacultyEngi in schoolsFacultyEngineering.Children)
                        {
                            bool lastChild = false;
                            if (schoolsFacultyEngineering.Children.Last() == schoolsFacultyEngi)
                            {
                                lastChild = true;
                            }
                            var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {

                                foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                {
                                    string comma = ",";
                                    if (lastChild)
                                    {
                                        if(schoolsFacultyEngiPicker.Last()==schoolsOfLawProfiles)
                                        {
                                            comma = "";
                                        }
                                    }
                                    FacultyListData += "{";
                                    if (schoolsOfLawProfiles.HasValue("studentName"))
                                    {
                                        FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"name\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                    {
                                        FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"departmentName\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("pageContent"))
                                    {
                                        FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"shortDescription\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                    {
                                        FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"imageLink\":\"\"";
                                    }
                                    FacultyListData += "}" + comma + "";
                                }                                
                            }
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyAppliedSciences = Umbraco.TypedContent(5047);
                    if (schoolsFacultyAppliedSciences.Children.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Applied Science\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsFacultyEngi in schoolsFacultyAppliedSciences.Children)
                        {
                            bool lastChild = false;
                            if (schoolsFacultyAppliedSciences.Children.Last() == schoolsFacultyEngi)
                            {
                                lastChild = true;
                            }
                            var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                {
                                    string comma = ",";
                                    if (lastChild)
                                    {
                                        if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                        {
                                            comma = "";
                                        }
                                    }
                                    FacultyListData += "{";
                                    if (schoolsOfLawProfiles.HasValue("studentName"))
                                    {
                                        FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"name\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                    {
                                        FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"departmentName\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("pageContent"))
                                    {
                                        FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"shortDescription\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                    {
                                        FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"imageLink\":\"\"";
                                    }
                                    FacultyListData += "}" + comma + "";
                                }
                            }
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyHealthScience = Umbraco.TypedContent(5047);
                    if (schoolsFacultyHealthScience.Children.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Health Science\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsFacultyEngi in schoolsFacultyHealthScience.Children)
                        {
                            bool lastChild = false;
                            if (schoolsFacultyHealthScience.Children.Last() == schoolsFacultyEngi)
                            {
                                lastChild = true;
                            }
                            var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                {
                                    string comma = ",";
                                    if (lastChild)
                                    {
                                        if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                        {
                                            comma = "";
                                        }
                                    }
                                    FacultyListData += "{";
                                    if (schoolsOfLawProfiles.HasValue("studentName"))
                                    {
                                        FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"name\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                    {
                                        FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"departmentName\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("pageContent"))
                                    {
                                        FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"shortDescription\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                    {
                                        FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"imageLink\":\"\"";
                                    }  
                                FacultyListData += "}" + comma + "";
                                }
                            }
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                        var schoolsFacultyBusiness = Umbraco.TypedContent(2939);
                        if (schoolsFacultyBusiness.Children.Count() != 0)
                        {
                            FacultyListData += "{\"schoolName\":\"School Of Health Science\",";
                            FacultyListData += "\"Profiles\":[";
                            foreach (var schoolsFacultyEngi in schoolsFacultyBusiness.Children)
                            {
                                bool  lastChild = false;
                                if (schoolsFacultyBusiness.Children.Last() == schoolsFacultyEngi)
                                {
                                    lastChild =true;
                                }
                                var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                                if (schoolsFacultyEngiPicker.Count() != 0)
                                {
                                    foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                    {
                                        string comma = ",";
                                        if (lastChild)
                                        {
                                            if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                            {
                                                comma = "";
                                            }
                                        }
                                        FacultyListData += "{";
                                        if (schoolsOfLawProfiles.HasValue("studentName"))
                                        {
                                            FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"name\":\"\",";
                                        }
                                        if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                        {
                                            FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"departmentName\":\"\",";
                                        }
                                        if (schoolsOfLawProfiles.HasValue("pageContent"))
                                        {
                                            FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"shortDescription\":\"\",";
                                        }
                                        if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                        {
                                            FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"imageLink\":\"\"";
                                        }   
                                    FacultyListData += "}" + comma + "";

                                    }
                                }
                            }
                            FacultyListData += "]";
                            FacultyListData += "}";
                           
                        }
                    }
                }
                FacultyListData += "]";
            }
            catch (Exception exe)
            {
                FacultyListData = "{\"status\": \"ERROR\", \"message\": \"" + exe.ToString().Replace("\\", "") + "\",\"list\": []}";
            }
            return Content(FacultyListData);
        }
        public JObject FacultyProfilesList()
        {

            string FacultyListData = "";
            //int facultyListDataID = 0;
            FacultyListData += "[";
            try
            {
                var schoolsOfLawFaculty = Umbraco.TypedContent(4846);
                var schoolOfLawFacultyPicker = schoolsOfLawFaculty.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                if (schoolOfLawFacultyPicker.Count() != 0)
                {
                    FacultyListData += "{\"schoolName\":\"Schools Of Law\",";
                    FacultyListData += "\"Profiles\":[";
                    foreach (var schoolsOfLawProfiles in schoolOfLawFacultyPicker)
                    {
                        string comma = ",";
                        if (schoolOfLawFacultyPicker.Last() == schoolsOfLawProfiles)
                        {
                            comma = "";
                        }
                        FacultyListData += "{";
                        if (schoolsOfLawProfiles.HasValue("studentName"))
                        {
                            FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                        }
                        else
                        {
                            FacultyListData += "\"name\":\"\",";
                        }
                        if (schoolsOfLawProfiles.HasValue("studentDegree"))
                        {
                            FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                        }
                        else
                        {
                            FacultyListData += "\"departmentName\":\"\",";
                        }
                        if (schoolsOfLawProfiles.HasValue("pageContent"))
                        {
                            FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                        }
                        else
                        {
                            FacultyListData += "\"shortDescription\":\"\",";
                        }
                        if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                        {
                            FacultyListData += "\"imageLink\":\"https://www.upes.ac.in" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                        }
                        else
                        {
                            FacultyListData += "\"imageLink\":\"\"";
                        }
                        FacultyListData += "}" + comma + "";
                    }
                    FacultyListData += "]";
                    FacultyListData += "},";
                    var schoolsOfComputerFaculty = Umbraco.TypedContent(4907);
                    var schoolOfComputerFacultyPicker = schoolsOfComputerFaculty.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                    if (schoolOfLawFacultyPicker.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Computer Science\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsOfLawProfiles in schoolOfComputerFacultyPicker)
                        {
                            string comma = ",";
                            if (schoolOfComputerFacultyPicker.Last() == schoolsOfLawProfiles)
                            {
                                comma = "";
                            }
                            FacultyListData += "{";
                            if (schoolsOfLawProfiles.HasValue("studentName"))
                            {
                                FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"name\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentDegree"))
                            {
                                FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"departmentName\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("pageContent"))
                            {
                                FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"shortDescription\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                            {
                                FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                            }
                            else
                            {
                                FacultyListData += "\"imageLink\":\"\"";
                            }
                            FacultyListData += "}" + comma + "";
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyDesign = Umbraco.TypedContent(5032);
                    var schoolsFacultyDesignPicker = schoolsFacultyDesign.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                    if (schoolsFacultyDesignPicker.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Design\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsOfLawProfiles in schoolsFacultyDesignPicker)
                        {
                            string comma = ",";
                            if (schoolsFacultyDesignPicker.Last() == schoolsOfLawProfiles)
                            {
                                comma = "";
                            }
                            FacultyListData += "{";
                            if (schoolsOfLawProfiles.HasValue("studentName"))
                            {
                                FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"name\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentDegree"))
                            {
                                FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"departmentName\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("shortDescription"))
                            {
                                FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                            }
                            else
                            {
                                FacultyListData += "\"shortDescription\":\"\",";
                            }
                            if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                            {
                                FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                            }
                            else
                            {
                                FacultyListData += "\"imageLink\":\"\"";
                            }
                            FacultyListData += "}" + comma + "";
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyEngineering = Umbraco.TypedContent(4643);
                    if (schoolsFacultyEngineering.Children.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Engineering\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsFacultyEngi in schoolsFacultyEngineering.Children)
                        {
                            bool lastChild = false;
                            if (schoolsFacultyEngineering.Children.Last() == schoolsFacultyEngi)
                            {
                                lastChild = true;
                            }
                            var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {

                                foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                {
                                    string comma = ",";
                                    if (lastChild)
                                    {
                                        if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                        {
                                            comma = "";
                                        }
                                    }
                                    FacultyListData += "{";
                                    if (schoolsOfLawProfiles.HasValue("studentName"))
                                    {
                                        FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"name\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                    {
                                        FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"departmentName\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("shortDescription"))
                                    {
                                        FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"shortDescription\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                    {
                                        FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"imageLink\":\"\"";
                                    }
                                    FacultyListData += "}" + comma + "";
                                }
                            }
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyAppliedSciences = Umbraco.TypedContent(5047);
                    if (schoolsFacultyAppliedSciences.Children.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Applied Science\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsFacultyEngi in schoolsFacultyAppliedSciences.Children)
                        {
                            bool lastChild = false;
                            if (schoolsFacultyAppliedSciences.Children.Last() == schoolsFacultyEngi)
                            {
                                lastChild = true;
                            }
                            var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                {
                                    string comma = ",";
                                    if (lastChild)
                                    {
                                        if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                        {
                                            comma = "";
                                        }
                                    }
                                    FacultyListData += "{";
                                    if (schoolsOfLawProfiles.HasValue("studentName"))
                                    {
                                        FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"name\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                    {
                                        FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"departmentName\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("shortDescription"))
                                    {
                                        FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"shortDescription\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                    {
                                        FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"imageLink\":\"\"";
                                    }
                                    FacultyListData += "}" + comma + "";
                                }
                            }
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                    }
                    var schoolsFacultyHealthScience = Umbraco.TypedContent(5047);
                    if (schoolsFacultyHealthScience.Children.Count() != 0)
                    {
                        FacultyListData += "{\"schoolName\":\"School Of Health Science\",";
                        FacultyListData += "\"Profiles\":[";
                        foreach (var schoolsFacultyEngi in schoolsFacultyHealthScience.Children)
                        {
                            bool lastChild = false;
                            if (schoolsFacultyHealthScience.Children.Last() == schoolsFacultyEngi)
                            {
                                lastChild = true;
                            }
                            var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                {
                                    string comma = ",";
                                    if (lastChild)
                                    {
                                        if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                        {
                                            comma = "";
                                        }
                                    }
                                    FacultyListData += "{";
                                    if (schoolsOfLawProfiles.HasValue("studentName"))
                                    {
                                        FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"name\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                    {
                                        FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"departmentName\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("shortDescription"))
                                    {
                                        FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"shortDescription\":\"\",";
                                    }
                                    if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                    {
                                        FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                    }
                                    else
                                    {
                                        FacultyListData += "\"imageLink\":\"\"";
                                    }
                                    FacultyListData += "}" + comma + "";
                                }
                            }
                        }
                        FacultyListData += "]";
                        FacultyListData += "},";
                        var schoolsFacultyBusiness = Umbraco.TypedContent(2939);
                        if (schoolsFacultyBusiness.Children.Count() != 0)
                        {
                            FacultyListData += "{\"schoolName\":\"School Of Health Science\",";
                            FacultyListData += "\"Profiles\":[";
                            foreach (var schoolsFacultyEngi in schoolsFacultyBusiness.Children)
                            {
                                bool lastChild = false;
                                if (schoolsFacultyBusiness.Children.Last() == schoolsFacultyEngi)
                                {
                                    lastChild = true;
                                }
                                var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                                if (schoolsFacultyEngiPicker.Count() != 0)
                                {
                                    foreach (var schoolsOfLawProfiles in schoolsFacultyEngiPicker)
                                    {
                                        string comma = ",";
                                        if (lastChild)
                                        {
                                            if (schoolsFacultyEngiPicker.Last() == schoolsOfLawProfiles)
                                            {
                                                comma = "";
                                            }
                                        }
                                        FacultyListData += "{";
                                        if (schoolsOfLawProfiles.HasValue("studentName"))
                                        {
                                            FacultyListData += "\"name\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentName") + "\",";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"name\":\"\",";
                                        }
                                        if (schoolsOfLawProfiles.HasValue("studentDegree"))
                                        {
                                            FacultyListData += "\"departmentName\":\"" + schoolsOfLawProfiles.GetPropertyValue<string>("studentDegree") + "\",";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"departmentName\":\"\",";
                                        }
                                        if (schoolsOfLawProfiles.HasValue("shortDescription"))
                                        {
                                            FacultyListData += "\"shortDescription\":\"" + objStu.removeHTML(schoolsOfLawProfiles.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim() + "\",";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"shortDescription\":\"\",";
                                        }
                                        if (schoolsOfLawProfiles.HasValue("studentPhoto"))
                                        {
                                            FacultyListData += "\"imageLink\":\"https://www.upes.ac.in/" + schoolsOfLawProfiles.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667) + "\"";
                                        }
                                        else
                                        {
                                            FacultyListData += "\"imageLink\":\"\"";
                                        }
                                        FacultyListData += "}" + comma + "";

                                    }
                                }
                            }
                            FacultyListData += "]";
                            FacultyListData += "}";

                        }
                    }
                }
                FacultyListData += "]";
            }
            catch (Exception exe)
            {
                FacultyListData = "{\"status\": \"ERROR\", \"message\": \"" + exe.ToString().Replace("\\", "") + "\",\"list\": []}";
            }
            return JObject.Parse(FacultyListData);
        }
    }
}