﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using upes_website.Gutils;

namespace upes_website.Controller
{
    public class stRecords
    {
        public string eventid { get; set; }
        public string groupid { get; set; }
        public string studid { get; set; }
    }

    public class GDPIController : SurfaceController
    {
        DBUtils dbu = new DBUtils();
        Crypt cr = new Crypt();

        [HttpGet]
        public string getEventList()
        {
            string courseList = "";

            string strEventQuery = "";

            strEventQuery += " select e.eventsid, e.eventstitle + ' ['+e.location+' - '+CONVERT(VARCHAR(10), e.eventsdate, 103)+' '+e.timeslot+']' as 'events' ";
            strEventQuery += " from STBL_GroupMaster g join specialevent e on e.eventsid = g.eventsid join STBL_COURSE c on e.c_id = c.c_id ";
            strEventQuery += " where g.status = 1 and e.status = 1 and c.STATUS = 1 and ";
            strEventQuery += " datediff(day, convert(datetime, e.eventsdate), getdate()) <= 10 and datediff(day, convert(datetime, e.eventsdate), getdate())>= 0 ";
            strEventQuery += " and g.grpid in (select grpid from STBL_Map_Facultygroup where facultyid in (" + Session["facultyid"] + ")) ";

            DataSet ds = dbu.executeQuery(strEventQuery);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    courseList += "<option value=\"" + ds.Tables[0].Rows[i]["eventsid"].ToString() + "\">" + ds.Tables[0].Rows[i]["events"].ToString() + "</option>";
                }
                courseList = "<option value=\"0\">Select Event Name*</option>" + courseList;
            }
            else
            {
                courseList = "<option value=\"0\">No Upcoming Events</option>" + courseList;
            }
            return courseList;
        }

        [HttpGet]
        public string getGroupList(string c_id)
        {
            string eventsList = "";
            DataSet ds = dbu.executeQuery("select * from STBL_GroupMaster where status=1 and eventsid=" + c_id);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    eventsList += "<option value=\"" + ds.Tables[0].Rows[i]["grpid"].ToString() + "\">" + ds.Tables[0].Rows[i]["groupname"].ToString() + "</option>";
                }
                eventsList = "<option value=\"0\">Select Group Name*</option>" + eventsList;
            }
            else
            {
                eventsList = "<option value=\"0\">No Groups to List</option>" + eventsList;
            }
            return eventsList;
        }

        [HttpGet]
        public String getSearchResult(int eventid, int groupid, int appno, string process, int rowcount)
        {
            string strresult = "";
            try
            {
                string eventsList = "";
                SqlParameter[] ParmsST = new SqlParameter[5];
                ParmsST[0] = new SqlParameter("@eventid", SqlDbType.Int);
                ParmsST[0].Value = eventid;
                ParmsST[1] = new SqlParameter("@groupid", SqlDbType.Int);
                ParmsST[1].Value = groupid;
                ParmsST[2] = new SqlParameter("@appno", SqlDbType.Int);
                ParmsST[2].Value = appno;
                ParmsST[3] = new SqlParameter("@facultyid", SqlDbType.Int);
                ParmsST[3].Value = Session["facultyid"].ToString();
                ParmsST[4] = new SqlParameter("@rowcount", SqlDbType.Int);
                ParmsST[4].Value = rowcount;

                DataSet ds = dbu.executeSP(true, "SP_Search_Event_Results", ParmsST);

                string strQueryPara = "";

                if (process == "GD")
                {
                    strQueryPara += " select distinct '999' as parameter_id,'10' as percentage,'Group Discussion' as parameter_name ";
                }
                else
                {
                    strQueryPara += " select distinct ip.parameter_id,ip.percentage,ipp.parameter_name,ipp.displayorder ";
                    strQueryPara += " from STBL_invite i join STBL_MAP_INTERVIEWPARAMS ip on ip.course_id = i.courseid  ";
                    strQueryPara += " join STBL_INTERVIEW_PARAMS ipp on ipp.ipm_id = ip.parameter_id ";
                    strQueryPara += " where i.status = 1 and ip.status = 1 and ipp.status = 1 and i.groupid = " + groupid + " order by ipp.displayorder";
                }

                DataSet dsPara = dbu.executeQuery(strQueryPara);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    strresult += " <div id=\"marknote\">Total No of Records : " + ds.Tables[1].Rows[0]["totcount"].ToString() + "</div> ";

                    strresult += " <div class=\"table-responsive\"><table class=\"table table-hover \">";
                    strresult += " <thead><tr><td class=\"th_SubFielsGD no - gutters\" width=\"05%\" rowspan=\"2\">S.No.</td> ";
                    strresult += " <td class=\"th_SubFielsGD no - gutters\" width=\"15%\" rowspan=\"2\">Application No.</td> ";
                    strresult += " <td class=\"th_SubFielsGD no - gutters\" width=\"30%\" rowspan=\"2\">Student Name</td> ";
                    strresult += " <td class=\"th_SubFielsGD no - gutters\" width=\"50%\" colspan=\"" + dsPara.Tables[0].Rows.Count + "\">Marks</td></tr> ";
                    strresult += " <tr>";

                    for (int j = 0; j < dsPara.Tables[0].Rows.Count; j++)
                    {
                        strresult += "<td class=\"th_SubFielsGD no - gutters\">" + dsPara.Tables[0].Rows[j]["parameter_name"].ToString() + "</td>";
                    }

                    strresult += " </tr></thead>";

                    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                    {
                        strresult += " <tr>";
                        strresult += "<td class=\"th_SubFielsGD no - gutters\">" + ds.Tables[0].Rows[k]["sort_row"].ToString() + "</td>";
                        strresult += "<td class=\"th_SubFielsGD no - gutters\"><a target=\"_blank\" href=\"view-student-record?studid=" + cr.EncryptForQueryString(ds.Tables[0].Rows[k]["studentid"].ToString()) + "&eventid=" + cr.EncryptForQueryString(eventid.ToString()) + "&groupid=" + cr.EncryptForQueryString(groupid.ToString()) + "\">" + ds.Tables[0].Rows[k]["appno"].ToString() + "</td>";
                        //strresult += "<td class=\"th_SubFielsGD no - gutters\"><a target=\"_blank\" href=\"view-student-record?studid=" + ds.Tables[0].Rows[k]["studentid"].ToString() + "&eventid=" + eventid + "&groupid=" + groupid + "\">" + ds.Tables[0].Rows[k]["appno"].ToString() + "</td>";
                        strresult += "<td class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[k]["name"].ToString() + "</td>";

                        string arParameterID = "";
                        for (int j = 0; j < dsPara.Tables[0].Rows.Count; j++)
                        {
                            string strQueryParaMarks = "";

                            if (process == "GD")
                            {
                                strQueryParaMarks += " select gdmarks as marks from STBL_GDWORKEXPMARKS ";
                                strQueryParaMarks += " where eventid=" + ds.Tables[0].Rows[k]["eventid"].ToString() + " and ";
                                strQueryParaMarks += " studid=" + ds.Tables[0].Rows[k]["studentid"].ToString();
                            }
                            else
                            {
                                strQueryParaMarks += " select marks from STBL_MARKS where eventid=" + ds.Tables[0].Rows[k]["eventid"].ToString() + " and ";
                                strQueryParaMarks += " studid=" + ds.Tables[0].Rows[k]["studentid"].ToString() + " and ";
                                strQueryParaMarks += " subid=" + dsPara.Tables[0].Rows[j]["parameter_id"].ToString();
                            }

                            DataSet dsParaMarks = dbu.executeQuery(strQueryParaMarks);

                            if (dsParaMarks != null && dsParaMarks.Tables[0].Rows.Count > 0)
                            {
                                //strresult += "<td class=\"th_SubFielsGD no - gutters\"><input type=\"text\" class=\"forMarkText1\" id=\"txt" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" onchange=\"UpdateMarks('" + ds.Tables[0].Rows[k]["studentid"].ToString() + "','" + ds.Tables[0].Rows[k]["eventid"].ToString() + "','" + ds.Tables[0].Rows[k]["groupid"].ToString() + "','" + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "','" + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "'," + ds.Tables[0].Rows.Count + "," + (k + 1) + ")\"  value=\"" + dsParaMarks.Tables[0].Rows[0]["marks"].ToString() + "\"/><span>/ " + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "</span></td>";
                                strresult += "<td class=\"th_SubFielsGD no - gutters\"><input type=\"text\" class=\"forMarkText1\" onkeypress=\"onEditMarks(" + (k + 1) + ");\" id=\"txt" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" value=\"" + dsParaMarks.Tables[0].Rows[0]["marks"].ToString() + "\"/><span>/ " + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "</span><input type=\"hidden\" id=\"hfParameterID" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" value=\"" + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" /><input type=\"hidden\" id=\"hfPercentageID" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" value=\"" + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "\" /></td>";
                                arParameterID += dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "|";
                            }
                            else
                            {
                                //strresult += "<td class=\"th_SubFielsGD no - gutters\"><input type=\"text\" class=\"forMarkText1\" id=\"txt" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" onchange=\"UpdateMarks('" + ds.Tables[0].Rows[k]["studentid"].ToString() + "','" + ds.Tables[0].Rows[k]["eventid"].ToString() + "','" + ds.Tables[0].Rows[k]["groupid"].ToString() + "','" + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "','" + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "'," + ds.Tables[0].Rows.Count + "," + (k + 1) + ")\" /><span>/ " + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "</span></td>";
                                strresult += "<td class=\"th_SubFielsGD no - gutters\"><input type=\"text\" class=\"forMarkText1\" onkeypress=\"onEditMarks(" + (k + 1) + ");\" id=\"txt" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" /><span>/ " + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "</span><input type=\"hidden\" id=\"hfParameterID" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" value=\"" + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" /><input type=\"hidden\" id=\"hfPercentageID" + (k + 1) + dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "\" value=\"" + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "\" /></td>";
                                arParameterID += dsPara.Tables[0].Rows[j]["parameter_id"].ToString() + "|";
                            }
                        }
                        if (arParameterID != "")
                            arParameterID = arParameterID.Remove((arParameterID.Length - 1), 1);
                        strresult += "<td class=\"th_SubFielsGD no - gutters\"><input type=\"button\" value=\"SAVE\" id=\"btnSave" + (k + 1) + "\" class=\"br30 fontBold btn btn-primary nextbtn btnSave\" onclick=\"UpdateMarks('" + ds.Tables[0].Rows[k]["studentid"].ToString() + "','" + ds.Tables[0].Rows[k]["eventid"].ToString() + "','" + ds.Tables[0].Rows[k]["groupid"].ToString() + "'," + (k + 1) + ",'" + arParameterID + "')\" /><input type=\"label\" id=\"lblResponse" + (k + 1) + "\" value=\"\" class=\"lblSaveResponse\" /></td>";
                        arParameterID = "";
                        strresult += " </tr>";
                    }

                    strresult += " </table></div> ";
                    //strresult += " <div class=\"FormBtn row no-gutters justify-content-center\"><div class=\"btnInn text-center text-uppercase\"> ";
                    //strresult += " <button class=\"br30  fontBold  btn btn-primary SubmitBtnBlue FormBtnStyle\" id=\"FormButtonTwoSubmit\" onclick=\"return doSubmit();\">Submit</button> ";
                    //strresult += " </div></div> ";

                    strresult += " <div id=\"div_pagination\"> ";

                    if (Convert.ToInt32(ds.Tables[1].Rows[0]["pagecounts"].ToString()) > 10)
                    {
                        if (rowcount != 1)
                        {
                            strresult += "<input type=\"button\" class=\"button\" value=\" << \"  onclick=\"return getSearchResult(1);\"/>";
                            strresult += "<input type=\"button\" class=\"button\" value=\" < \"  onclick=\"return getSearchResult(" + (rowcount - 1) + ");\"/>";
                        }

                        int orgrowcount = rowcount;

                        if ((rowcount + 10) > Convert.ToInt32(ds.Tables[1].Rows[0]["pagecounts"].ToString()))
                        {
                            rowcount = rowcount - 9;
                        }

                        for (int k = rowcount; k < rowcount + 10; k++)
                        {
                            if (orgrowcount == k)
                            {
                                strresult += "<input type=\"button\" class=\"button active\" value=\"" + k + "\"  onclick=\"return getSearchResult(" + k + ");\"/>";
                            }
                            else
                            {
                                strresult += "<input type=\"button\" class=\"button\" value=\"" + k + "\"  onclick=\"return getSearchResult(" + k + ");\"/>";
                            }
                        }

                        if (orgrowcount != Convert.ToInt32(ds.Tables[1].Rows[0]["pagecounts"].ToString()))
                        {
                            strresult += "<input type=\"button\" class=\"button\" value=\" > \"  onclick=\"return getSearchResult(" + (orgrowcount + 1) + ");\"/>";
                            strresult += "<input type=\"button\" class=\"button\" value=\" >> \"  onclick=\"return getSearchResult(" + ds.Tables[1].Rows[0]["pagecounts"].ToString() + ");\"/>";
                        }
                    }
                    else
                    {
                        int orgrowcount = rowcount;

                        for (int k = 1; k <= Convert.ToInt32(ds.Tables[1].Rows[0]["pagecounts"].ToString()); k++)
                        {
                            if (orgrowcount == k)
                            {
                                strresult += "<input type=\"button\" class=\"button active\" value=\"" + k + "\"  onclick=\"return getSearchResult(" + k + ");\"/>";
                            }
                            else
                            {
                                strresult += "<input type=\"button\" class=\"button\" value=\"" + k + "\"  onclick=\"return getSearchResult(" + k + ");\"/>";
                            }
                        }
                    }

                    strresult += " </div> ";
                    strresult += " </div> ";
                }
                else
                {
                    if (appno == 0)
                        strresult = "<div>No Groups to List</div>" + eventsList;
                    else
                        strresult = "<div>Appication number is not mapped to your ID</div>" + eventsList;
                }
            }
            catch (Exception e)
            {
                strresult = "Error";
            }

            return strresult;
        }


        [HttpGet]
        //public String getStudentRecords(Int32 eventid, Int32 groupid, Int32 studid)
        public String getStudentRecords([System.Web.Http.FromBody]stRecords objRecords)
        {
            Int32 eventid = Convert.ToInt32(cr.DecryptForQueryString(objRecords.eventid.ToString()));
            Int32 groupid = Convert.ToInt32(cr.DecryptForQueryString(objRecords.groupid.ToString()));
            Int32 studid = Convert.ToInt32(cr.DecryptForQueryString(objRecords.studid.ToString()));

            string strresult = "";
            try
            {
                string eventsList = "";
                string strQuery = "";

                strQuery += " select i.inviteid,i.studentid,i.eventid,i.courseid,i.groupid,s.studid,(s.fname +' '+ s.mname+' '+s.lname) as name,s.appno,s.email, ";
                strQuery += " e.eventstitle + ' ['+e.location+' - '+CONVERT(VARCHAR(10), e.eventsdate, 103)+' '+e.timeslot+']' as 'events',g.groupname,c.c_name,*  ";
                strQuery += " from STBL_invite i join student_online s on s.studid = i.studentid ";
                strQuery += " join specialevent e on e.eventsid=i.eventid join STBL_GroupMaster g on g.eventsid = e.eventsid join STBL_COURSE c on e.c_id = c.c_id ";
                strQuery += " where i.eventid = " + eventid + " and groupid = " + groupid + " and i.studentid=" + studid + " and i.status = 1 ";

                DataSet ds = dbu.executeQuery(strQuery);

                string strQueryPara = "";

                //strQueryPara += " select distinct '999' as parameter_id,'10' as percentage,'Group Discussion' as parameter_name,999 as displayorder union all ";
                strQueryPara += " select distinct ip.parameter_id,ip.percentage,ipp.parameter_name,ipp.displayorder ";
                strQueryPara += " from STBL_invite i join STBL_MAP_INTERVIEWPARAMS ip on ip.course_id = i.courseid  ";
                strQueryPara += " join STBL_INTERVIEW_PARAMS ipp on ipp.ipm_id = ip.parameter_id ";
                strQueryPara += " where i.status = 1 and ip.status = 1 and ipp.status = 1 and i.groupid = " + groupid + " order by displayorder";

                DataSet dsPara = dbu.executeQuery(strQueryPara);

                string strQueryGD = "";

                strQueryGD += " select gdmarks as marks from STBL_GDWORKEXPMARKS ";
                strQueryGD += " where eventid=" + ds.Tables[0].Rows[0]["eventid"].ToString() + " and ";
                strQueryGD += " studid=" + ds.Tables[0].Rows[0]["studentid"].ToString();
                DataSet dsParaGD = dbu.executeQuery(strQueryGD);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    strresult += "<div class=\"table-responsive\"> <table class=\"table table-hover \">";
                    strresult += " <tr> ";
                    strresult += " <td width=\"25%\" class=\"th_SubFielsGDL no - gutters\">Name</td> ";
                    strresult += " <td width=\"30%\" class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[0]["name"].ToString() + "</td> ";
                    strresult += " <td width=\"20%\" class=\"th_SubFielsGDL no - gutters\">Application Number</td> ";
                    strresult += " <td width=\"25%\" class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[0]["appno"].ToString() + "</td> ";
                    strresult += " <tr> ";
                    strresult += " <tr> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">Email</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[0]["email"].ToString() + "</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">Mobile</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[0]["mobile"].ToString() + "</td> ";
                    strresult += " <tr> ";
                    strresult += " <tr> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">Course Name</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[0]["c_name"].ToString() + "</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">Group Name</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">" + ds.Tables[0].Rows[0]["groupname"].ToString() + "</td> ";
                    strresult += " <tr> ";
                    strresult += " <tr> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">Event Name</td> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\" colspan=\"3\">" + ds.Tables[0].Rows[0]["events"].ToString() + "</td> ";
                    strresult += " <tr> ";
                    strresult += " <tr> ";
                    strresult += " <td class=\"th_SubFielsGDL no - gutters\">Group Discussion (Marks)</td> ";

                    if (dsParaGD.Tables[0].Rows.Count > 0)
                    {
                        strresult += "<td class=\"th_SubFielsGDL no - gutters\" colspan=\"3\">" + dsParaGD.Tables[0].Rows[0]["marks"].ToString() + " <span>/ 10</span></td>";
                    }
                    else
                    {
                        strresult += "<td class=\"th_SubFielsGDL no - gutters\" colspan=\"3\">0 <span>/ 10</span></td>";
                    }

                    strresult += " <tr> ";
                    strresult += " </table>";

                    strresult += " <table class=\"table table-hover\">";
                    strresult += " <thead> ";
                    strresult += " <tr><td class=\"th_SubFielsGD no - gutters\" colspan=\"" + dsPara.Tables[0].Rows.Count + "\">PI Marks</td></tr> ";
                    strresult += " <tr>";

                    for (int j = 0; j < dsPara.Tables[0].Rows.Count; j++)
                    {
                        strresult += "<td class=\"th_SubFielsGD no - gutters\">" + dsPara.Tables[0].Rows[j]["parameter_name"].ToString() + "</td>";
                    }

                    strresult += " </tr></thead>";

                    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                    {
                        strresult += " <tr>";

                        for (int j = 0; j < dsPara.Tables[0].Rows.Count; j++)
                        {
                            string strQueryParaMarks = "";

                            strQueryParaMarks += " select marks from STBL_MARKS where eventid=" + ds.Tables[0].Rows[k]["eventid"].ToString() + " and ";
                            strQueryParaMarks += " studid=" + ds.Tables[0].Rows[k]["studentid"].ToString() + " and ";
                            strQueryParaMarks += " subid=" + dsPara.Tables[0].Rows[j]["parameter_id"].ToString();

                            DataSet dsParaMarks = dbu.executeQuery(strQueryParaMarks);

                            if (dsParaMarks != null && dsParaMarks.Tables[0].Rows.Count > 0)
                            {
                                strresult += "<td class=\"th_SubFielsGD no - gutters\">" + dsParaMarks.Tables[0].Rows[0]["marks"].ToString() + " <span>/ " + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "</span></td>";
                            }
                            else
                            {
                                strresult += "<td class=\"th_SubFielsGD no - gutters\">0 <span>/ " + dsPara.Tables[0].Rows[j]["percentage"].ToString() + "</span></td>";
                            }
                        }

                        strresult += " </tr>";
                    }

                    strresult += " </table></div><br> ";
                }
                else
                {
                    strresult = "<div>No Groups to List</div>" + eventsList;
                }
            }
            catch (Exception e)
            {
                strresult = "Error  : " + e.ToString();
            }

            return strresult;
        }

        public string UpdateMarks(Int32 Eventsid, Int32 groupid, long studid, Int32 parameterid, Int32 maxmarks, Int32 marks)
        {
            string result = "";
            try
            {
                SqlParameter[] Parms = new SqlParameter[8];
                Parms[0] = new SqlParameter("@Eventsid", SqlDbType.Int);
                Parms[0].Value = Eventsid;
                Parms[1] = new SqlParameter("@groupid", SqlDbType.Int);
                Parms[1].Value = groupid;
                Parms[2] = new SqlParameter("@studid", SqlDbType.Int);
                Parms[2].Value = studid;
                Parms[3] = new SqlParameter("@parameterid", SqlDbType.Int);
                Parms[3].Value = parameterid;
                Parms[4] = new SqlParameter("@maxmarks", SqlDbType.Int);
                Parms[4].Value = maxmarks;
                Parms[5] = new SqlParameter("@marks", SqlDbType.Decimal);
                Parms[5].Value = marks;
                Parms[6] = new SqlParameter("@uname", SqlDbType.Int);
                Parms[6].Value = Convert.ToInt32(Session["facultyid"].ToString());
                Parms[7] = new SqlParameter("@result", SqlDbType.Int);
                Parms[7].Direction = ParameterDirection.Output;
                Parms[7].Value = 0;

                dbu.executeSP(true, "SP_UpdateMarksGDPI", Parms);

                if (Convert.ToInt32(Parms[7].Value) == 1)
                {
                    result = "success";
                }
                else
                {
                    result = "error";
                }
            }
            catch (Exception exe)
            {
                result = "error : " + exe.ToString();
            }
            return result;
        }
    }
}