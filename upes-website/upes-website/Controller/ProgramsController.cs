﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;

namespace upes_website.Controller
{
    public class ProgramsController : UmbracoApiController
    {
        StudentController objStu = new StudentController();
        public class programsModel
        {
            public string programID { get; set; }
            public string programName { get; set; }
        }
        public class ResponseModel
        {
            public object data { set; get; }
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            String accessToken = "";
            int studentStatus = 0;
            try
            {
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];
                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    var courseContent = Umbraco.TypedContent(2938);
                    List<programsModel> objPrograms = new List<programsModel>();
                    //UPCOMING EVENTS
                    if (courseContent.HasValue("contentPicker"))
                    {
                        var studentAchievementItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                        foreach (var cbContent in studentAchievementItems)
                        {
                            programsModel objProgram = new programsModel();
                            objProgram.programID = cbContent.GetKey().ToString();
                            
                            if (cbContent.HasValue("smallTitle"))
                            {
                                objProgram.programName = objStu.removeHTML(cbContent.GetPropertyValue("smallTitle").ToString());
                            }
                            else
                            {
                                objProgram.programName = "";
                            }


                            objPrograms.Add(objProgram);
                        }
                    }

                    ResponseModel responseModel = new ResponseModel();
                    responseModel.data = objPrograms;

                    return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }
    }
}
