﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;

namespace upes_website.Controller
{
    public class FacultyController : UmbracoApiController
    {
        StudentController objStu = new StudentController();

        public class programsModel
        {
            public string programID { get; set; }
            public string programName { get; set; }
        }
        public class facultyModel
        {
            public string facultyID { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string imageURL { get; set; }
            public string designation { get; set; }
            public string description { get; set; }
            public string department { get; set; }
            public object program { set; get; }
        }
        public class ResponseModel
        {
            public object data { set; get; }
        }

        [HttpGet]
        public HttpResponseMessage Get(string programID)
        {
            String accessToken = "", hostURL = "";
            int studentStatus = 0;
            try
            {
                hostURL = ConfigurationManager.AppSettings["api_host"].ToString();
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];
                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    if (programID == "")
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Program ID is missing");
                    }

                    if (programID == "1c399fe4-bd17-4287-8cd7-1088c5c21ac4" || programID == "568266df-05e6-4894-ae88-b449f2d8cad9" || programID == "e585ff46-c689-4520-8e17-7972e8b1f588" || programID == "38e8c8bd-3e62-49ef-81ff-35aea74528a6" || programID == "b40a94d7-1ef5-4b44-b550-f5982c35313c" || programID == "2267ca28-d71e-4ad0-8e9c-3318f08d2d9e")
                    {

                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid program ID");
                    }

                    if (programID == "1c399fe4-bd17-4287-8cd7-1088c5c21ac4")
                    {
                        List<facultyModel> objFaculties = new List<facultyModel>();
                        programsModel objProgram = new programsModel();
                        objProgram.programID = "1c399fe4-bd17-4287-8cd7-1088c5c21ac4";
                        objProgram.programName = "School of Engineering";

                        var schoolsFacultyEngineering = Umbraco.TypedContent(4643);
                        if (schoolsFacultyEngineering.Children.Count() > 0)
                        {
                            foreach (var schoolsFacultyEngi in schoolsFacultyEngineering.Children)
                            {
                                var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                                if (schoolsFacultyEngiPicker.Count() != 0)
                                {
                                    foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                    {
                                        facultyModel objFaculty = new facultyModel();
                                        objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                        if (facultyProfile.HasValue("studentName"))
                                        {
                                            objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                        }
                                        else
                                        {
                                            objFaculty.firstName = "";
                                        }
                                        objFaculty.lastName = "";
                                        objFaculty.designation = "";
                                        objFaculty.program = objProgram;

                                        if (facultyProfile.HasValue("studentDegree"))
                                        {
                                            objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                        }
                                        else
                                        {
                                            objFaculty.department = "";
                                        }
                                        if (facultyProfile.HasValue("shortDescription"))
                                        {
                                            objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                        }
                                        else
                                        {
                                            objFaculty.description = "";
                                        }
                                        if (facultyProfile.HasValue("studentPhoto"))
                                        {
                                            if(schoolsFacultyEngiPicker.First()==facultyProfile)
                                            {
                                                objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                            }
                                            else
                                            {
 objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                            }
                                           
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = "";
                                        }

                                        objFaculties.Add(objFaculty);
                                    }
                                }
                            }
                        }

                        var schoolsFacultyAppliedSciencesandHumanities = Umbraco.TypedContent(5047);
                        if (schoolsFacultyAppliedSciencesandHumanities.Children.Count() > 0)
                        {
                            foreach (var schoolsFacultyEngi in schoolsFacultyAppliedSciencesandHumanities.Children)
                            {
                                var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                                if (schoolsFacultyEngiPicker.Count() != 0)
                                {
                                    foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                    {
                                        facultyModel objFaculty = new facultyModel();
                                        objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                        if (facultyProfile.HasValue("studentName"))
                                        {
                                            objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                        }
                                        else
                                        {
                                            objFaculty.firstName = "";
                                        }
                                        objFaculty.lastName = "";
                                        objFaculty.designation = "";
                                        objFaculty.program = objProgram;

                                        if (facultyProfile.HasValue("studentDegree"))
                                        {
                                            objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                        }
                                        else
                                        {
                                            objFaculty.department = "";
                                        }
                                        if (facultyProfile.HasValue("shortDescription"))
                                        {
                                            objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                        }
                                        else
                                        {
                                            objFaculty.description = "";
                                        }
                                        if (facultyProfile.HasValue("studentPhoto"))
                                        {
                                            if (schoolsFacultyEngiPicker.First() == facultyProfile)
                                            {
                                                objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                            }
                                            else
                                            {
                                                objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                            }
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = "";
                                        }

                                        objFaculties.Add(objFaculty);
                                    }
                                }
                            }
                        }

                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objFaculties;
                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else if (programID == "568266df-05e6-4894-ae88-b449f2d8cad9")
                    {
                        List<facultyModel> objFaculties = new List<facultyModel>();
                        programsModel objProgram = new programsModel();
                        objProgram.programID = "568266df-05e6-4894-ae88-b449f2d8cad9";
                        objProgram.programName = "School of Law";

                        var schoolsFacultyEngineering = Umbraco.TypedContent(4846);
                        var test = Umbraco.TypedContent(8397);
                        if (schoolsFacultyEngineering.HasValue("contentPicker"))
                        {
                                var schoolsFacultyEngiPicker = schoolsFacultyEngineering.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                                if (schoolsFacultyEngiPicker.Count() != 0)
                                {
                                    foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                    {
                                        facultyModel objFaculty = new facultyModel();
                                        objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                        if (facultyProfile.HasValue("studentName"))
                                        {
                                            objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                        }
                                        else
                                        {
                                            objFaculty.firstName = "";
                                        }
                                        objFaculty.lastName = "";
                                        objFaculty.designation = "";
                                        objFaculty.program = objProgram;

                                        if (facultyProfile.HasValue("studentDegree"))
                                        {
                                            objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                        }
                                        else
                                        {
                                            objFaculty.department = "";
                                        }
                                        if (facultyProfile.HasValue("shortDescription"))
                                        {
                                        objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                    }
                                        else
                                        {
                                            objFaculty.description = "";
                                        }
                                        if (facultyProfile.HasValue("studentPhoto"))
                                        {
                                        if (schoolsFacultyEngiPicker.First() == facultyProfile)
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                        }
                                    }
                                        else
                                        {
                                            objFaculty.imageURL = "";
                                        }

                                        objFaculties.Add(objFaculty);
                                    }
                                }
                        }

                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objFaculties;
                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else if (programID == "e585ff46-c689-4520-8e17-7972e8b1f588")
                    {
                        List<facultyModel> objFaculties = new List<facultyModel>();
                        programsModel objProgram = new programsModel();
                        objProgram.programID = "e585ff46-c689-4520-8e17-7972e8b1f588";
                        objProgram.programName = "School of Design";

                        var schoolsFacultyEngineering = Umbraco.TypedContent(5032);
                        if (schoolsFacultyEngineering.HasValue("contentPicker"))
                        {
                            var schoolsFacultyEngiPicker = schoolsFacultyEngineering.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                {
                                    facultyModel objFaculty = new facultyModel();
                                    objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                    if (facultyProfile.HasValue("studentName"))
                                    {
                                        objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                    }
                                    else
                                    {
                                        objFaculty.firstName = "";
                                    }
                                    objFaculty.lastName = "";
                                    objFaculty.designation = "";
                                    objFaculty.program = objProgram;

                                    if (facultyProfile.HasValue("studentDegree"))
                                    {
                                        objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                    }
                                    else
                                    {
                                        objFaculty.department = "";
                                    }
                                    if (facultyProfile.HasValue("shortDescription"))
                                    {
                                        objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                    }
                                    else
                                    {
                                        objFaculty.description = "";
                                    }
                                    if (facultyProfile.HasValue("studentPhoto"))
                                    {
                                        if (schoolsFacultyEngiPicker.First() == facultyProfile)
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                        }
                                    }
                                    else
                                    {
                                        objFaculty.imageURL = "";
                                    }

                                    objFaculties.Add(objFaculty);
                                }
                            }
                        }

                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objFaculties;
                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else if (programID == "38e8c8bd-3e62-49ef-81ff-35aea74528a6")
                    {
                        List<facultyModel> objFaculties = new List<facultyModel>();
                        programsModel objProgram = new programsModel();
                        objProgram.programID = "38e8c8bd-3e62-49ef-81ff-35aea74528a6";
                        objProgram.programName = "School of Business";

                        var schoolsFacultyEngineering = Umbraco.TypedContent(2939);
                        if (schoolsFacultyEngineering.Children.Count() > 0)
                        {
                            foreach (var schoolsFacultyEngi in schoolsFacultyEngineering.Children)
                            {
                                var schoolsFacultyEngiPicker = schoolsFacultyEngi.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                                if (schoolsFacultyEngiPicker.Count() != 0)
                                {
                                    foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                    {
                                        facultyModel objFaculty = new facultyModel();
                                        objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                        if (facultyProfile.HasValue("studentName"))
                                        {
                                            objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                        }
                                        else
                                        {
                                            objFaculty.firstName = "";
                                        }
                                        objFaculty.lastName = "";
                                        objFaculty.designation = "";
                                        objFaculty.program = objProgram;

                                        if (facultyProfile.HasValue("studentDegree"))
                                        {
                                            objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                        }
                                        else
                                        {
                                            objFaculty.department = "";
                                        }
                                        if (facultyProfile.HasValue("shortDescription"))
                                        {
                                            objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                        }
                                        else
                                        {
                                            objFaculty.description = "";
                                        }
                                        if (facultyProfile.HasValue("studentPhoto"))
                                        {
                                            if (schoolsFacultyEngiPicker.First() == facultyProfile)
                                            {
                                                objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                            }
                                            else
                                            {
                                                objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                            }
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = "";
                                        }

                                        objFaculties.Add(objFaculty);
                                    }
                                }
                            }
                        }

                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objFaculties;
                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else if (programID == "b40a94d7-1ef5-4b44-b550-f5982c35313c")
                    {
                        List<facultyModel> objFaculties = new List<facultyModel>();
                        programsModel objProgram = new programsModel();
                        objProgram.programID = "b40a94d7-1ef5-4b44-b550-f5982c35313c";
                        objProgram.programName = "School of Computer Science";

                        var schoolsFacultyEngineering = Umbraco.TypedContent(4907);
                        if (schoolsFacultyEngineering.HasValue("contentPicker"))
                        {
                            var schoolsFacultyEngiPicker = schoolsFacultyEngineering.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                {
                                    facultyModel objFaculty = new facultyModel();
                                    objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                    if (facultyProfile.HasValue("studentName"))
                                    {
                                        objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                    }
                                    else
                                    {
                                        objFaculty.firstName = "";
                                    }
                                    objFaculty.lastName = "";
                                    objFaculty.designation = "";
                                    objFaculty.program = objProgram;

                                    if (facultyProfile.HasValue("studentDegree"))
                                    {
                                        objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                    }
                                    else
                                    {
                                        objFaculty.department = "";
                                    }
                                    if (facultyProfile.HasValue("shortDescription"))
                                    {
                                        objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                    }
                                    else
                                    {
                                        objFaculty.description = "";
                                    }
                                    if (facultyProfile.HasValue("studentPhoto"))
                                    {
                                        if (schoolsFacultyEngiPicker.First() == facultyProfile)
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                        }
                                    }
                                    else
                                    {
                                        objFaculty.imageURL = "";
                                    }

                                    objFaculties.Add(objFaculty);
                                }
                            }
                        }

                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objFaculties;
                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else if (programID == "2267ca28-d71e-4ad0-8e9c-3318f08d2d9e")
                    {
                        List<facultyModel> objFaculties = new List<facultyModel>();
                        programsModel objProgram = new programsModel();
                        objProgram.programID = "2267ca28-d71e-4ad0-8e9c-3318f08d2d9e";
                        objProgram.programName = "School of Health Sciences";

                        var schoolsFacultyEngineering = Umbraco.TypedContent(5465);
                        if (schoolsFacultyEngineering.HasValue("contentPicker"))
                        {
                            var schoolsFacultyEngiPicker = schoolsFacultyEngineering.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            if (schoolsFacultyEngiPicker.Count() != 0)
                            {
                                foreach (var facultyProfile in schoolsFacultyEngiPicker)
                                {
                                    facultyModel objFaculty = new facultyModel();
                                    objFaculty.facultyID = facultyProfile.GetKey().ToString();

                                    if (facultyProfile.HasValue("studentName"))
                                    {
                                        objFaculty.firstName = facultyProfile.GetPropertyValue<string>("studentName");
                                    }
                                    else
                                    {
                                        objFaculty.firstName = "";
                                    }
                                    objFaculty.lastName = "";
                                    objFaculty.designation = "";
                                    objFaculty.program = objProgram;

                                    if (facultyProfile.HasValue("studentDegree"))
                                    {
                                        objFaculty.department = facultyProfile.GetPropertyValue<string>("studentDegree");
                                    }
                                    else
                                    {
                                        objFaculty.department = "";
                                    }
                                    if (facultyProfile.HasValue("shortDescription"))
                                    {
                                        objFaculty.description = objStu.removeHTML(facultyProfile.GetGridHtml("pageContent", "bootstrap3").ToString().Replace("\"", "'")).Trim();
                                    }
                                    else
                                    {
                                        objFaculty.description = "";
                                    }
                                    if (facultyProfile.HasValue("studentPhoto"))
                                    {
                                        if (schoolsFacultyEngiPicker.First() == facultyProfile)
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 375);
                                        }
                                        else
                                        {
                                            objFaculty.imageURL = hostURL + facultyProfile.GetPropertyValue<IPublishedContent>("studentPhoto").GetCropUrl(500, 667);
                                        }
                                    }
                                    else
                                    {
                                        objFaculty.imageURL = "";
                                    }

                                    objFaculties.Add(objFaculty);
                                }
                            }
                        }

                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objFaculties;
                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                    }
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }
    }
}
