﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace upes_website.Controller
{
    public class SearchController : SurfaceController
    {
        const string accessKey = "21bbbdaa2f6646ecb4b018d1a3e75702";
        const string uriBase = "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/search";

        [HttpGet]
        public String getSearchResult(string searchTerm, string page)
        {
            string strresult = "";
            try
            {
                int offset = 1;
                if (page != null && page != "" && page != "undefined")
                {
                    try
                    {
                        int.TryParse(page, out offset);
                    }
                    catch { }
                }
                strresult = BingWebSearch(searchTerm, offset);
            }
            catch (Exception e)
            {
                //strresult = "Error occured, Please try again...";
                strresult = "Error";
            }

            return strresult;
        }

        static string BingWebSearch(string searchQuery, int offset)
        {
            string searchResult = "", apiResonpse = "";
            try
            {
                var customConfigId = "d3c1de75-2899-4431-94ff-20cb8b1ae5e3";
                // Construct the URI of the search request
                var uriQuery = uriBase + "?q=" + Uri.EscapeDataString(searchQuery) + "&count=10&offset=" + (offset - 1) + "&responseFilter=Webpages&customconfig=" + customConfigId;

                // Perform the Web request and get the response
                WebRequest request = HttpWebRequest.Create(uriQuery);
                request.Headers["Ocp-Apim-Subscription-Key"] = accessKey;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();// GetResponseAsync().Result;
                string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
                apiResonpse = json;
                //convert json object
                var obj = JObject.Parse(json);
                JToken jtWebPage = obj["webPages"];
                if (jtWebPage != null)
                {
                    var url = obj["webPages"]["value"].ToArray();
                    if (url.Length > 0)
                    {
                        int totalMatches = Convert.ToInt32(obj["webPages"]["totalEstimatedMatches"]);
                        searchResult = totalMatches + "||";
                        for (int i = 0; i < url.Length; i++)
                        {
                            searchResult += "<div class='SearchRow'><article>";
                            searchResult += "<h2>" + url[i]["name"] + "</h2><a href='" + url[i]["url"] + "'>" + url[i]["url"] + "</a><p>" + url[i]["snippet"] + "</p>";
                            searchResult += "</article></div>";
                        }
                        searchResult += "||" + totalMatches + " results for <span>keyword: " + searchQuery + "</span>||";

                        string strPaging = "";
                        int totalPages = 0;
                        if (totalMatches > 10)
                        {
                            int pageIndex = 1;
                            totalPages = totalMatches / 10;

                            if(totalPages < offset)
                            {
                                searchResult = "Error";
                                return searchResult;
                            }

                            if (offset == 1)
                            {
                                strPaging += "<li class='page-item disabled'><a class='page-link' >Previous</a></li>";
                            }
                            else
                            {
                                strPaging += "<li class='page-item'><a class='page-link' href='/search/" + searchQuery + "/" + (offset - 1) + "'>Previous</a></li>";
                            }

                            if (offset > 5)
                            {
                                pageIndex = (offset - 5);
                                for (int i = pageIndex; i <= (offset + 5); i++)
                                {
                                    if (i <= totalPages)
                                    {
                                        strPaging += "<li class='page-item " + ((offset == i) ? "active" : "") + "'><a class='page-link' href='/search/" + searchQuery + "/" + i + "'>" + i + ((offset == i) ? "<span class='sr-only'>(current)</span>" : "") + "</a></li>";
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = pageIndex; i <= 10; i++)
                                {
                                    if (i <= totalPages)
                                    {
                                        strPaging += "<li class='page-item " + ((offset == i) ? "active" : "") + "'><a class='page-link' href='/search/" + searchQuery + "/" + i + "'>" + i + ((offset == i) ? "<span class='sr-only'>(current)</span>" : "") + "</a></li>";
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            if (offset >= totalPages)
                            {
                                strPaging += "<li class='page-item disabled'><a class='page-link'>Next</a></li>";
                            }
                            else
                            {
                                strPaging += "<li class='page-item'><a class='page-link' href='/search/" + searchQuery + "/" + (offset + 1) + "'>Next</a></li>";
                            }

                            strPaging = "<nav aria-label='...'><ul class='pagination'>" + strPaging + "</ul></nav>";
                        }
                        searchResult += strPaging;
                    }
                }
                else
                {
                    searchResult = "0|| ||0 results for <span>keyword: " + searchQuery + "</span>||";
                }
            }
            catch (Exception ex)
            {
                searchResult = "Error";// "Error occured, Please try again..." + ex.Message;
            }
            return searchResult;
        }

        [HttpGet]
        public String getSearchSuggestion(string searchTerm)
        {
            string strresult = "";
            try
            {
                strresult = BingSearchSuggestion(searchTerm);
            }
            catch (Exception e)
            {
                strresult = "Error";
            }

            return strresult;
        }

        static string BingSearchSuggestion(string searchQuery)
        {
            string searchResult = "", apiResonpse = "";
            try
            {
                var customConfigId = "7c26ee75-6ad6-4446-a84e-3db736bfce51";
                // Construct the URI of the search request
                var uriQuery = "https://api.cognitive.microsoft.com/bing/v7.0/suggestions" + "?q=" + Uri.EscapeDataString(searchQuery) + "&customconfig=" + customConfigId;

                // Perform the Web request and get the response
                WebRequest request = HttpWebRequest.Create(uriQuery);
                request.Headers["Ocp-Apim-Subscription-Key"] = "36e125afd67e45b2b65efedcf56b7828";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();// GetResponseAsync().Result;
                string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
                apiResonpse = json;
                //convert json object
                var obj = JObject.Parse(json);
                searchResult = json;
            }
            catch (Exception ex)
            {
                searchResult = "Error occured, Please try again..." + ex.Message + " ---- apiResonpse: " + apiResonpse;
            }
            return searchResult;
        }
    }
}