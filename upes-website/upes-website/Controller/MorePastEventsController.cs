﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;

namespace upes_website.Controller
{
    public class MorePastEventsController : UmbracoApiController
    {
        StudentController objStu = new StudentController();
        public class eventsModel
        {
            public string eventID { get; set; }
            public string title { get; set; }
            public string imageURL { get; set; }
            public string description { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public Boolean isRegistered { get; set; }
        }
        public class ResponseModel
        {
            public object data { set; get; }
        }
        [HttpGet]
        public HttpResponseMessage Get()
        {
            String accessToken = "", hostURL = "";
            int studentStatus = 0;
            try
            {
                hostURL = ConfigurationManager.AppSettings["api_host"].ToString();
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];
                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    var courseContent = Umbraco.TypedContent(1237);
                    List<eventsModel> objPastEvents = new List<eventsModel>();

                    //PAST EVENTS
                    if (courseContent.HasValue("contentPicker2"))
                    {
                        var studentAchievementItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker2");
                        foreach (var cbContent in studentAchievementItems)
                        {
                            eventsModel objEvent = new eventsModel();

                            objEvent.eventID = cbContent.GetKey().ToString();
                            if (cbContent.HasValue("showcaseImage"))
                            {
                                var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("showcaseImage");
                                if (ImgPath != null)
                                {
                                    objEvent.imageURL = hostURL + ImgPath.Url;
                                }
                                else
                                {
                                    objEvent.imageURL = "";
                                }
                            }
                            else
                            {
                                objEvent.imageURL = "";
                            }
                            if (cbContent.HasValue("shortDescription"))
                            {
                                objEvent.title = objStu.removeHTML(cbContent.GetPropertyValue("shortDescription").ToString());
                            }
                            else
                            {
                                objEvent.title = "";
                            }

                            if (cbContent.HasValue("showcaseTitle"))
                            {
                                objEvent.description = objStu.removeHTML(cbContent.GetPropertyValue("showcaseTitle").ToString());
                            }
                            else
                            {
                                objEvent.description = "";
                            }
                            if (cbContent.HasValue("publishedDate"))
                            {
                                objEvent.startDate = cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
                                objEvent.endDate = cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
                            }
                            else
                            {
                                objEvent.startDate = "";
                                objEvent.endDate = "";
                            }
                            objEvent.isRegistered = false;
                            objPastEvents.Add(objEvent);
                        }
                    }

                    ResponseModel responseModel = new ResponseModel();
                    responseModel.data = objPastEvents;

                    return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }
    }
}
