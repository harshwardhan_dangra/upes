﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;

namespace upes_website.Controller
{
    public class lifeAtUpesController : UmbracoApiController
    {
        StudentController objStu = new StudentController();
        public class apiInputParameters
        {
            public int programID { get; set; }
            public string tag { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage Get(string tag)
        {
            String result = "", accessToken = "", hostURL = "";
            JObject objResponse;
            int studentStatus = 0;
            Boolean containsData = false;
            try
            {
                hostURL = ConfigurationManager.AppSettings["api_host"].ToString();
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];

                //IEnumerable<string> inputStringTag;
                //if (!Request.Headers.TryGetValues("tag", out inputStringTag))
                //{
                //    objResponse = JObject.Parse("{\"message\":\"Invalid input parameters\"}");
                //    return Request.CreateResponse<JObject>(HttpStatusCode.Unauthorized, objResponse);
                //}
                //var customInputArrayTag = inputStringTag.ToArray();
                //tag = customInputArrayTag[0].ToString();
                if(tag == "")
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Tag can't be empty");
                }

                if (tag == "studentAchievements" || tag == "alumniStories" || tag == "parentStories" || tag == "facilities")
                {

                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid tag");
                }

                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    if (tag == "studentAchievements")
                    {
                        var courseContent = Umbraco.TypedContent(1059);
                        if (courseContent.HasValue("contentPicker"))
                        {
                            var studentAchievementItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            foreach (var cbContent in studentAchievementItems)
                            {
                                if (cbContent.HasValue("smallTitle"))
                                {
                                    containsData = true;
                                    result += "{";
                                    result += "\"articleID\" : \"" + cbContent.GetKey().ToString() + "\"";
                                    if (cbContent.HasValue("thumbImage"))
                                    {
                                        var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("thumbImage");
                                        if (ImgPath != null)
                                        {
                                            result += ", \"imageURL\" : \"" + hostURL + ImgPath.GetCropUrl(500,281) + "\"";
                                        }
                                        else
                                        {
                                            result += ", \"imageURL\" : \"\"";
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"imageURL\" : \"\"";
                                    }
                                    if (cbContent.HasValue("smallTitle"))
                                    {
                                        result += ",\"title\" : \"" + cbContent.GetPropertyValue("smallTitle") + "\"";
                                    }
                                    else
                                    {
                                        result += ",\"title\" : \"\"";
                                    }

                                    if (cbContent.HasValue("authorName"))
                                    {
                                        result += ", \"author\" : \"" + cbContent.GetPropertyValue("authorName") + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"author\" : \"\"";
                                    }
                                    if (cbContent.HasValue("publishedDate"))
                                    {
                                        result += ", \"datePublished\" : \"" + cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'") + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"datePublished\" : \"\"";
                                    }
                                    if (cbContent.HasValue("pageContent"))
                                    {
                                        if (cbContent.GetGridHtml("pageContent").ToString() != null && cbContent.GetGridHtml("pageContent").ToString() != "")
                                        {
                                            result += ", \"description\" : \"" + objStu.removeHTML(cbContent.GetGridHtml("pageContent").ToString()).Replace("\"", "'").Trim() + "\"";
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"description\" : \"\"";
                                    }
                                    if (cbContent.HasValue("knowmoreLink"))
                                    {
                                        var link = cbContent.GetPropertyValue<RelatedLinks>("knowmoreLink").First();
                                        if (link != null)
                                        {
                                            if(link.Link.StartsWith("/"))
                                            {
                                                result += ", \"articleURL\" : \"" + hostURL+link.Link + "\"";
                                            }
                                            else
                                            {
                                                result += ", \"articleURL\" : \"" + link.Link + "\"";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"articleURL\" : \"\"";
                                    }

                                    result += ", \"program\": {}";
                                    result += "},";
                                }
                            }
                            if (containsData)
                                result = result.Remove((result.Length - 1), 1);
                        }
                        result = "{ \"data\" : [" + result + "] }";
                        objResponse = JObject.Parse(result);
                        return Request.CreateResponse<JObject>(HttpStatusCode.OK, objResponse);
                    }
                    else if (tag == "alumniStories")
                    {
                        var courseContent = Umbraco.TypedContent(8952);//(1116); 
                        if (courseContent.HasValue("contentPicker2"))
                        {
                            var parentStoriesItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker2");
                            foreach (var cbContent in parentStoriesItems)
                            {
                                if (cbContent.HasValue("studentName"))
                                {
                                    containsData = true;
                                    result += "{";
                                    result += "\"articleID\" : \"" + cbContent.GetKey().ToString() + "\"";
                                    if (cbContent.HasValue("studentPhoto"))
                                    {
                                        var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("studentPhoto");
                                        if (ImgPath != null)
                                        {
                                            result += ", \"imageURL\" : \"" + hostURL + ImgPath.GetCropUrl(500,281) + "\"";
                                        }
                                        else
                                        {
                                            result += ", \"imageURL\" : \"\"";
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"imageURL\" : \"\"";
                                    }
                                    if (cbContent.HasValue("studentName"))
                                    {
                                        result += ",\"title\" : \"" + cbContent.GetPropertyValue("studentName") + "\"";
                                    }
                                    else
                                    {
                                        result += ",\"title\" : \"\"";
                                    }

                                    if (cbContent.HasValue("authorName"))
                                    {
                                        result += ", \"author\" : \"" + cbContent.GetPropertyValue("authorName") + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"author\" : \"\"";
                                    }
                                    if (cbContent.HasValue("publishedDate"))
                                    {
                                        result += ", \"datePublished\" : \"" + cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'") + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"datePublished\" : \"\"";
                                    }
                                    if (cbContent.HasValue("pageContent"))
                                    {
                                        if (cbContent.GetGridHtml("pageContent").ToString() != null && cbContent.GetGridHtml("pageContent").ToString() != "")
                                        {
                                            result += ", \"description\" : \"" + objStu.removeHTML(cbContent.GetGridHtml("pageContent").ToString()).Replace("\"", "'").Trim() + "\"";
                                        }
                                    }
                                    else if (cbContent.HasValue("shortDescription"))
                                    {
                                        result += ", \"description\" : \"" + objStu.removeHTML(cbContent.GetPropertyValue<string>("shortDescription").ToString()).Replace("\"", "'").Trim() + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"description\" : \"\"";
                                    }
                                    if (cbContent.HasValue("pSKnowMoreLink") && cbContent.HasValue("pSKnowMoreLink"))
                                    {
                                        var link = cbContent.GetPropertyValue<RelatedLinks>("pSKnowMoreLink").First();
                                        if(link!=null)
                                        {
                                            if (link.Link.StartsWith("/"))
                                            {
                                                result += ", \"articleURL\" : \"" + hostURL + link.Link + "\"";
                                            }
                                            else
                                            {
                                                result += ", \"articleURL\" : \"" + link.Link + "\"";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"articleURL\" : \"\"";
                                    }
                                    if (cbContent.HasValue("studentDegree"))
                                    {
                                        result += ", \"program\" : { \"programID\": \"" + cbContent.GetKey().ToString() + "\", \"programName\" : \"" + cbContent.GetPropertyValue("studentDegree").ToString().Replace("\"", "'") + "\" }";
                                    }
                                    else
                                    {
                                        result += ", \"program\" : {}";
                                    }
                                    result += "},";
                                }
                            }
                            if (containsData)
                                result = result.Remove((result.Length - 1), 1);
                        }
                        result = "{ \"data\" : [" + result + "] }";
                        objResponse = JObject.Parse(result);
                        return Request.CreateResponse<JObject>(HttpStatusCode.OK, objResponse);
                    }
                    else if (tag == "parentStories")
                    {
                        var courseContent = Umbraco.TypedContent(1140);
                        if (courseContent.HasValue("contentPicker"))
                        {
                            var parentStoriesItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                            foreach (var cbContent in parentStoriesItems)
                            {
                                if (cbContent.HasValue("parentName"))
                                {
                                    containsData = true;
                                    result += "{";
                                    result += "\"articleID\" : \"" + cbContent.GetKey().ToString() + "\"";
                                    if (cbContent.HasValue("parentPhoto"))
                                    {
                                        var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("parentPhoto");
                                        if (ImgPath != null)
                                        {
                                            result += ", \"imageURL\" : \"" + hostURL + ImgPath.GetCropUrl(500, 281) + "\"";
                                        }
                                        else
                                        {
                                            result += ", \"imageURL\" : \"\"";
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"imageURL\" : \"\"";
                                    }
                                    if (cbContent.HasValue("parentName"))
                                    {
                                        result += ",\"title\" : \"" + cbContent.GetPropertyValue("parentName") + "\"";
                                    }
                                    else
                                    {
                                        result += ",\"title\" : \"\"";
                                    }

                                    if (cbContent.HasValue("authorName"))
                                    {
                                        result += ", \"author\" : \"" + cbContent.GetPropertyValue("authorName") + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"author\" : \"\"";
                                    }
                                    if (cbContent.HasValue("publishedDate"))
                                    {
                                        result += ", \"datePublished\" : \"" + cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'") + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"datePublished\" : \"\"";
                                    }
                                    
                                     if (cbContent.HasValue("pageContent"))
                                    {
                                        if (cbContent.GetGridHtml("pageContent").ToString() != null&& cbContent.GetGridHtml("pageContent").ToString()!="")
                                            {
                                            result += ", \"description\" : \"" + objStu.removeHTML(cbContent.GetGridHtml("pageContent").ToString()).Replace("\"", "'").Trim() + "\"";
                                        }                                       
                                    }
                                     else if (cbContent.HasValue("shortDescription"))
                                    {
                                        result += ", \"description\" : \"" + objStu.removeHTML(cbContent.GetPropertyValue<string>("shortDescription").ToString()).Replace("\"", "'").Trim() + "\"";
                                    }
                                    else
                                    {
                                        result += ", \"description\" : \"\"";
                                    }
                                    if (cbContent.HasProperty("pSKnowMoreLink") && cbContent.HasValue("pSKnowMoreLink"))
                                    {
                                        var link = cbContent.GetPropertyValue<RelatedLinks>("pSKnowMoreLink").First();
                                        if (link != null)
                                        {
                                            if (link.Link.StartsWith("/"))
                                            {
                                                result += ", \"articleURL\" : \"" + hostURL + link.Link + "\"";
                                            }
                                            else
                                            {
                                                result += ", \"articleURL\" : \"" + link.Link + "\"";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result += ", \"articleURL\" : \"\"";
                                    }
                                    result += ", \"program\": {}";
                                    result += "},";
                                }
                            }
                            if (containsData)
                                result = result.Remove((result.Length - 1), 1);
                        }
                        result = "{ \"data\" : [" + result + "] }";
                        objResponse = JObject.Parse(result);
                        return Request.CreateResponse<JObject>(HttpStatusCode.OK, objResponse);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid tag");
                    }
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }
    }
}
