﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace upes_website.Controller
{
    public class FacultyLoginAPIController : SurfaceController
    {
        // GET: FacultyLoginAPI
        public ActionResult Index()
        {
            string schoolsListData = "";
            return Content(schoolsListData);
        }
        public JObject GetAllSchoolsList()
        {

            string schoolsListData = "";
            int schoolsListId = 0;
            try
            {
                var schoolsFaculty = Umbraco.TypedContent(2938);
                var schoolsPicker = schoolsFaculty.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                if (schoolsPicker.Count() != 0)
                {
                    schoolsListData += "{";
                    schoolsListData += "\"School List\":[";
                    foreach (var schoolList in schoolsPicker)
                    {
                        string comma = ",";
                        if (schoolsPicker.Last() == schoolList)
                        {
                            comma = "";
                        }
                        else
                        {
                            comma = ",";
                        }
                        schoolsListData += "{";
                        schoolsListData += "\"schoolName\":\"" + schoolList.Name + "\",";
                        schoolsListData += "\"schoolId\":\"" + schoolList.Id + "\"";
                        schoolsListData += "}" + comma + "";
                    }
                    schoolsListData += "]";
                    schoolsListData += "}";
                }
            }
            catch (Exception e)
            {
                schoolsListData = "{\"status\": \"ERROR\", \"message\": \"" + e.ToString().Replace("\\", "") + "\",\"list\": []}";
            }
                return JObject.Parse(schoolsListData);
            }
    }
}