﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using upes_website.Gutils;
using upes_website.GUtils;
using upes_website.Models;
namespace upes_website.Controller
{

    public class RequestCallController : SurfaceController
    {
        // GET: RequestCall
        DBUtils dbu = new DBUtils();
        
        public ActionResult Index()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var general = Umbraco.TypedContent(1941);
            if (general.HasValue("tooltipMessage"))
            {
                Session["tooltipMessage"] = general.GetPropertyValue<string>("tooltipMessage");
            }
            else
            {
                Session["tooltipMessage"] = "";
            }
            return PartialView("Forms/CallBackForm");
        }
        [HttpPost]
        public ActionResult HandleSubmit(CallBack model)
        {
            string status = "",state="",city="",courseID="",courseType="";
            //string log_file_path = ConfigurationManager.AppSettings["leadsquaredlogfilepath"].ToString();
            //string log_file_path_pg = ConfigurationManager.AppSettings["logfilepath"].ToString();
            //Logger logger = new Logger(log_file_path);
            //Logger loggerpg = new Logger();
            string TokenURL = "https://login.salesforce.com/services/oauth2/token?password=P@55w0rd@21&client_secret=3E9FA44833FD5DF9B1B2672285FC09451B45BC776CFDDBF9AA21115FE3E1C893&client_id=3MVG9G9pzCUSkzZvwO4lhD68m6r7VS9I.BYScwhAim6E07s8Lh9tteGSfPyWMKlzRkpRGhHJZgBVYT2gZjRdC&username=system.admin@haloocom.com&grant_type=password";
            string SalesForceURL = "https://upes.my.salesforce.com/services/apexrest/CreateLead1/";
            string SalesForceEditURL = "https://upes.my.salesforce.com/services/data/v45.0/sobjects/Lead/";
            if (ModelState.IsValid)
            {
                var db = DatabaseContext.ConnectionString;

                string qry2 = "select STATENAME from STBL_STATE where STID="+model.State+"";
                string qry1 = "select cityname from STBL_CITY where city_id="+model.City+"";
                string query = "select C_NAME,COURSETYPE from STBL_COURSE where C_ID=" + model.CourseName+"";
                string Datasource = "Online Leads", Lead_Subsource1= "Education/Other Websites",Source= "enquiry form";
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("");
                //string LeadSquareurl = "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Capture?accessKey=u$r91ef34d96f2e1098b3f12ee79bf97bf1&secretKey=c1d4155825ecce7c85241fc7316e55c0518a8db1";

                //var httpWebRequest = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
                //httpWebRequest.ContentType = "application/json";
                //httpWebRequest.Method = "POST";
                try
                {
                    DataSet ds = dbu.executeQuery(query);
                    if(ds.Tables[0].Rows[0]!=null)
                    {
                        courseID = ds.Tables[0].Rows[0]["C_NAME"].ToString();
                        courseType = ds.Tables[0].Rows[0]["COURSETYPE"].ToString();
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
                try
                {
                    DataSet ds = dbu.executeQuery(qry2);
                    if(ds.Tables[0].Rows[0]!=null)
                    {
                        state = ds.Tables[0].Rows[0]["STATENAME"].ToString();
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
                try
                {
                    DataSet ds = dbu.executeQuery(qry1);
                    if (ds.Tables[0].Rows[0] != null)
                    {
                        city = ds.Tables[0].Rows[0]["cityname"].ToString();
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
                string maxiD = "";
                string qry4 = "select max(eid)+1 as id from quick_enquiry";
                try
                {
                    DataSet ds = dbu.executeQuery(qry4);
                    if (ds.Tables[0].Rows[0] != null)
                    {
                        maxiD = ds.Tables[0].Rows[0]["id"].ToString();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                string qry = "insert into quick_enquiry(eid,fname,mobile,email,city,program,trdate,query,status,uname,query_type,crminsertflag,iscrmexception,exceptiondetails) values("+maxiD+",'" + model.Name + "','" + model.PhoneNo + "','" + model.Email + "','" + city + "','" + courseID + "',getdate(),'NULL',1,'"+model.Name+"','NULL',0,0,'NULL')";
                try
                {
                    DataSet redulu =dbu.executeQuery(qry);
                    if (redulu != null)
                    {
                        var status1 = true;
                    }
                }
               catch(Exception e)
                {
                    return Content("Data insertion Failure");
                }
                if (courseType == "UG")
                {
                    courseType = "Under Graduate";
                }
                else if (courseType == "PG")
                {
                    courseType = "Post Graduate";
                }
                string salesForceData = "{", SalesForceID="";
                salesForceData += "\"FirstName\":\"\",";
                salesForceData += "\"MiddleName\":\"\",";
                salesForceData += "\"LastName\":\"" +model.Name+ "\",";
                salesForceData += "\"Email\":\"" + model.Email + "\",";
                salesForceData += "\"MobilePhone\":\"" + model.PhoneNo + "\",";
                salesForceData += "\"SelectCourse\":\"" + courseID + "\",";
                salesForceData += "\"CourseType\":\"" + courseType + "\",";
                //salesForceData += "\"SelectCourse\":\"" + courseName + "\",";
                salesForceData += "\"ApplingYear\":\"" + model.Year + "\",";
                salesForceData += "\"Country\":\"India\",";
                salesForceData += "\"State\":\"" + state + "\",";
                salesForceData += "\"IndiaCity\":\"" + city + "\",";
                //salesForceData += "\"CourseType\":\"" + city + "\",";
                salesForceData += "\"Status\":\"Open\",";
                salesForceData += "\"DataSource\":\"" + Datasource + "\",";
                salesForceData += "\"Source\":\"" + Source + "\",";
                salesForceData += "\"SubSource\":\"" + Lead_Subsource1 + "\"";
                salesForceData += "}";
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var webRequest = (HttpWebRequest)WebRequest.Create(TokenURL);
                webRequest.ContentType = "application/json";
                webRequest.Method = "POST";

                try
                {
                    //Json input to API
                    using (var SF_streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        //logger.write("///////////////////International Admission LSQ REQ Json : " + json + "///////////////////");
                        //streamWriter.Write(json);
                        SF_streamWriter.Flush();
                        SF_streamWriter.Close();
                    }
                    var webResponse = (HttpWebResponse)webRequest.GetResponse();

                    using (var SF_streamReader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        var SF_texttoken = SF_streamReader.ReadToEnd();
                        if (SF_texttoken != null && SF_texttoken != "")
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            dynamic SF_item = serializer.Deserialize<object>(SF_texttoken);
                            if (!string.IsNullOrEmpty(SF_item["access_token"]))
                            {
                                string SF_Access_token = "Bearer " + SF_item["access_token"];

                                webRequest = (HttpWebRequest)WebRequest.Create(SalesForceURL);
                                webRequest.Headers.Add("authorization", SF_Access_token);
                                webRequest.ContentType = "application/json";
                                webRequest.Method = "POST";
                                webRequest.ContentLength = salesForceData.Length;
                                webRequest.KeepAlive = false;

                                using (StreamWriter SF_requestWriter2 = new StreamWriter(webRequest.GetRequestStream()))
                                {
                                    SF_requestWriter2.Write(salesForceData);
                                }

                                var SF_httpResponse1 = (HttpWebResponse)webRequest.GetResponse();
                                using (var SF_responseReader = new StreamReader(SF_httpResponse1.GetResponseStream()))
                                {
                                    var SF_text_Data = SF_responseReader.ReadToEnd();
                                    if (SF_text_Data != null && SF_text_Data != "")
                                    {
                                        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                                        dynamic SF_item1 = json_serializer.Deserialize<object>(SF_text_Data);
                                        if (SF_item1 != null)
                                        {
                                            string pushSaleforceData = "insert into SalesForceLog(Stud_Id,Message,created_on) values('" + model.Name + "','" + SF_text_Data + "',GETDATE())";
                                            try
                                            {
                                                DataSet redulu = dbu.executeQuery(pushSaleforceData);
                                                if (redulu != null)
                                                {
                                                    status = "true";
                                                }
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                        if (SF_item1["id"] != null)
                                        {
                                            if (!string.IsNullOrEmpty(SF_item1["id"]))
                                            {
                                                SalesForceID = Convert.ToString(SF_item1["id"]);
                                            }
                                        }
                                        else if(SF_item1["message"]== "Email already exist")
                                        {
                                            string msg = Convert.ToString(SF_item1["message"]);
                                            SalesForceID = msg;
                                        }
                                        else if(SF_item1["message"]== "Email already exist")
                                        {
                                            string msg = Convert.ToString(SF_item1["message"]);
                                            SalesForceID = msg;
                                        }
                                        else
                                        {
                                            string msg = Convert.ToString(SF_item1["message"]);
                                            SalesForceID = msg;
                                        }

                                    }


                                }



                                //crmleadguid = item["Message"]["RelatedId"].ToString();
                                //logger.write("///////////////////International Admission LSQ for " + obj.name + "  Success : RelatedId" + crmleadguid + " ///");
                            }
                            else if (SF_item["Status"] == "Error")
                            {


                            }
                        }
                    }
                }
                catch (WebException ex)
                {
                    string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();


                }
                //string json = "[";
                //json = json + "{\"Attribute\":\"FirstName\",\"Value\":\"" + model.Name.TrimEnd() + "\"},";
                ////json = json + "{\"Attribute\":\"LastName\",\"Value\":\"" + lastname + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Date_of_Birth\",\"Value\":\"" + dob + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Country\",\"Value\":\"" + getCountryName(country) + "\"},";
                //json = json + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
                //json = json + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
                //json = json + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + model.Email + "\"},";
                //json = json + "{\"Attribute\":\"Phone\",\"Value\":\"" + model.PhoneNo + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Apply_Year\",\"Value\":\"" + DateTime.Now.Year.ToString() + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Nationality\",\"Value\":\"" + typeofnation + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Level\",\"Value\":\"" + study + "\"},";
                //json = json + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
                //json = json + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
                ////json = json + "{\"Attribute\":\"SourceMedium\",\"Value\":\"" + utm_medium + "\"},";
                ////json = json + "{\"Attribute\":\"SourceCampaign\",\"Value\":\"" + utm_campaign + "\"},";
                //if (model.ProspectId != "NA" && model.ProspectId != "" && model.ProspectId != null)
                //{
                //    json = json + "{\"Attribute\":\"ProspectID\",\"Value\":\"" + model.ProspectId + "\"},";
                //}
                ////json = json + "{\"Attribute\":\"ProspectStage\",\"Value\":\"ANP- Verified\"},";
                //json = json + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
                //json = json + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + model.CourseName + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Programs_Interested\",\"Value\":\"" + courseID + "\"}";
                //json = json + "{\"Attribute\":\"SearchBy\",\"Value\":\"Phone\"}";
                //json = json + "]";
                //string json2 = "[";
                //json2 = json2 + "{\"Attribute\":\"FirstName\",\"Value\":\"" + model.Name.TrimEnd() + "\"},";
                ////json = json + "{\"Attribute\":\"LastName\",\"Value\":\"" + lastname + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Date_of_Birth\",\"Value\":\"" + dob + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Country\",\"Value\":\"" + getCountryName(country) + "\"},";
                //json2 = json2 + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
                //json2 = json2 + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
                //json2 = json2 + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + model.Email + "\"},";
                ////json2 = json2 + "{\"Attribute\":\"Phone\",\"Value\":\"" + model.PhoneNo + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Apply_Year\",\"Value\":\"" + DateTime.Now.Year.ToString() + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Nationality\",\"Value\":\"" + typeofnation + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Level\",\"Value\":\"" + study + "\"},";
                //json2 = json2 + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
                //json2 = json2 + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
                ////json = json + "{\"Attribute\":\"SourceMedium\",\"Value\":\"" + utm_medium + "\"},";
                ////json = json + "{\"Attribute\":\"SourceCampaign\",\"Value\":\"" + utm_campaign + "\"},";
                ////if (model.ProspectId != "NA" && model.ProspectId != "" && model.ProspectId != null)
                ////{
                ////    json = json + "{\"Attribute\":\"ProspectID\",\"Value\":\"" + model.ProspectId + "\"},";
                ////}
                ////json = json + "{\"Attribute\":\"ProspectStage\",\"Value\":\"ANP- Verified\"},";
                //json2 = json2 + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
                //json2 = json2 + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + model.CourseName + "\"},";
                ////json = json + "{\"Attribute\":\"mx_Programs_Interested\",\"Value\":\"" + courseID + "\"}";
                //json2 = json2 + "]";
                //try
                //{
                //using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                //{                   
                //    //logger.write("///////////////////Registration Stage JSON " + json + "///////////////////");
                //    streamWriter.Write(json);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}
                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //{
                //    var text = streamReader.ReadToEnd();
                //    JavaScriptSerializer serializer = new JavaScriptSerializer();
                //    dynamic item = serializer.Deserialize<object>(text);
                //    if (item["Status"] == "Success")
                //    {
                //        var crmleadguid = item["Message"]["RelatedId"].ToString();
                //        string qry5 = "insert into tbl_enquirynow_log(studId,response,relatedId) values("+maxiD+",'"+text+"','"+crmleadguid+"')";
                //        try
                //        {
                //            DataSet redulu = dbu.executeQuery(qry5);
                //            if (redulu != null)
                //            {
                //                status = "true";
                //            }
                //        }
                //        catch(Exception e)
                //        {
                //            return Content("Logger insertion Failure");
                //        }
                //    }
                    
                //    //logger.write("///////////////////Response for Registration Stage Student Name:" + firstname + " Response Received : " + text + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");
                //}
                //}
                //catch(WebException e)
                //{
                //    string message = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();                    
                //    try
                //    {
                //        using (StreamWriter streamWriter2 = new StreamWriter(httpWebRequest.GetRequestStream()))
                //        {
                //            //logger.write("///////////////////Registration Stage JSON " + json + "///////////////////");
                //            streamWriter2.Write(json2);
                //            streamWriter2.Flush();
                //            streamWriter2.Close();
                //        }
                //        var httpResponse1 = (HttpWebResponse)httpWebRequest.GetResponse();
                //        using (var streamReader2 = new StreamReader(httpResponse1.GetResponseStream()))
                //        {
                //            var text = streamReader2.ReadToEnd();
                //            JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                //            dynamic item = serializer1.Deserialize<object>(text);
                //            if (item["Status"] == "Success")
                //            {
                //                var crmleadguid = item["Message"]["RelatedId"].ToString();
                //                string qry5 = "insert into tbl_enquirynow_log(studId,response,relatedId) values(" + maxiD + ",'" + text + "','" + crmleadguid + "')";
                //                try
                //                {
                //                    DataSet redulu = dbu.executeQuery(qry5);
                //                    if (redulu != null)
                //                    {
                //                        status = "true";
                //                    }
                //                }
                //                catch (Exception ex)
                //                {
                //                    return Content("Logger insertion Failure");
                //                }
                //            }

                //            //logger.write("///////////////////Response for Registration Stage Student Name:" + firstname + " Response Received : " + text + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");
                //        }
                //    }
                //    catch(Exception excep)
                //    {
                //        return Content(excep.ToString());
                //    }
                //    return Content(message);
                //}
                //catch(Exception exc)
                //{
                //    return Content(exc.ToString());
                //}

            }
            else
            {

                status = "false";
                return CurrentUmbracoPage();
            }

            //return Content(status);
            return Content("Success");
        }
        public ActionResult bindcity(string stateid)
        {
            string strDispTable = "";
            

            string query = "select city_id,cityname from  STBL_CITY where STID='" + stateid + "' order by cityname asc";
            DataSet ds = dbu.executeQuery(query);


            strDispTable += "<option Value selected >-- Please select a City --</option> ";
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {
                    strDispTable += "<option Value='" + ds.Tables[0].Rows[intCount]["city_id"].ToString() + "' >" + ds.Tables[0].Rows[intCount]["cityname"].ToString() + "</option> ";
                }
            }

            return Content(strDispTable);
        }
        public ActionResult bindstate()
        {
            string strDispTable = "";


            string query = "select STID, STATENAME from STBL_STATE where Countrycode = 111 and status=1  order by STATENAME asc";
            DataSet ds = dbu.executeQuery(query);


            strDispTable += "<option Value='0' selected >Select State</option> ";
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {
                    strDispTable += "<option Value='" + ds.Tables[0].Rows[intCount]["STID"].ToString() + "' >" + ds.Tables[0].Rows[intCount]["STATENAME"].ToString() + "</option> ";
                }
            }
            // Response.Write(strDispTable);



            return Content(strDispTable);
        }
        //public void UpdateSalesforce(clsAdmission obj)
        //{
        //    string TokenURL = "https://login.salesforce.com/services/oauth2/token?password=Zoxima@2019&client_secret=3E9FA44833FD5DF9B1B2672285FC09451B45BC776CFDDBF9AA21115FE3E1C893&client_id=3MVG9G9pzCUSkzZvwO4lhD68m6r7VS9I.BYScwhAim6E07s8Lh9tteGSfPyWMKlzRkpRGhHJZgBVYT2gZjRdC&username=shubham.verma1@zoxima.com&grant_type=password";
        //    string SalesForceURL = "https://upes.my.salesforce.com/services/apexrest/CreateLead1/";
        //    string SalesForceEditURL = "https://upes.my.salesforce.com/services/data/v45.0/sobjects/Lead/";
        //    try
        //    {

        //        string name1 = obj.name;
        //        string[] ss = name1.Split(' ');

        //        string name = ss.Length == 1 ? "" : ss[0];
        //        string lname = ss.Length > 1 ? ss[1] : ss[0];

        //        string CourseName = GetCourseName(obj.program);
        //        string Coursetype = GetCourseType(obj.program);

        //        //Landing Page     
        //        string json = "{";
        //        json = json + "\"Status\":\"" + obj.ProspectStage + "\"";
        //        json = json + "}";




        //        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

        //        var SF_httpWebRequest = (HttpWebRequest)WebRequest.Create(TokenURL);
        //        SF_httpWebRequest.ContentType = "application/json";
        //        SF_httpWebRequest.Method = "POST";

        //        try
        //        {
        //            //Json input to API
        //            using (var SF_streamWriter = new StreamWriter(SF_httpWebRequest.GetRequestStream()))
        //            {
        //                //logger.write("///////////////////International Admission LSQ REQ Json : " + json + "///////////////////");
        //                //streamWriter.Write(json);
        //                SF_streamWriter.Flush();
        //                SF_streamWriter.Close();
        //            }
        //            var SF_httpResponse = (HttpWebResponse)SF_httpWebRequest.GetResponse();

        //            using (var SF_streamReader = new StreamReader(SF_httpResponse.GetResponseStream()))
        //            {
        //                var SF_texttoken = SF_streamReader.ReadToEnd();
        //                if (SF_texttoken != null && SF_texttoken != "")
        //                {
        //                    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //                    dynamic SF_item = serializer.Deserialize<object>(SF_texttoken);
        //                    if (!string.IsNullOrEmpty(SF_item["access_token"]))
        //                    {
        //                        string SF_Access_token = "Bearer " + SF_item["access_token"];

        //                        SF_httpWebRequest = (HttpWebRequest)WebRequest.Create(SalesForceEditURL + obj.SalesForceID);
        //                        SF_httpWebRequest.Headers.Add("authorization", SF_Access_token);
        //                        SF_httpWebRequest.ContentType = "application/json";
        //                        SF_httpWebRequest.Method = "PATCH";
        //                        SF_httpWebRequest.ContentLength = json.Length;
        //                        SF_httpWebRequest.KeepAlive = false;

        //                        using (StreamWriter SF_requestWriter2 = new StreamWriter(SF_httpWebRequest.GetRequestStream()))
        //                        {
        //                            SF_requestWriter2.Write(json);
        //                        }

        //                        var SF_httpResponse1 = (HttpWebResponse)SF_httpWebRequest.GetResponse();
        //                        using (var SF_responseReader = new StreamReader(SF_httpResponse1.GetResponseStream()))
        //                        {
        //                            var SF_text_Data = SF_responseReader.ReadToEnd();
        //                            if (SF_text_Data != null && SF_text_Data != "")
        //                            {
        //                                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        //                                dynamic SF_item1 = json_serializer.Deserialize<object>(SF_text_Data);
        //                                if (SF_item1["id"] != null)
        //                                {
        //                                    if (!string.IsNullOrEmpty(SF_item1["id"]))
        //                                    {
        //                                        obj.SalesForceID = Convert.ToString(SF_item1["id"]);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    string msg = Convert.ToString(SF_item1["message"]);
        //                                }

        //                            }


        //                        }



        //                        //crmleadguid = item["Message"]["RelatedId"].ToString();
        //                        //logger.write("///////////////////International Admission LSQ for " + obj.name + "  Success : RelatedId" + crmleadguid + " ///");
        //                    }
        //                    else if (SF_item["Status"] == "Error")
        //                    {

        //                        //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //                        //{

        //                        //    //logger.write("///////////////////International Admission LSQ REQ  JSON  RE-Send Using EMAIL " + jsonEmail + "///////////////////");
        //                        //    //streamWriter.Write(jsonEmail);
        //                        //    streamWriter.Flush();
        //                        //    streamWriter.Close();
        //                        //}
        //                        //using (var streamReaderEmail = new StreamReader(httpResponse.GetResponseStream()))
        //                        //{
        //                        //    var textEmail = streamReaderEmail.ReadToEnd();
        //                        //    JavaScriptSerializer serializerEmail = new JavaScriptSerializer();
        //                        //    dynamic itemEmail = serializerEmail.Deserialize<object>(textEmail);
        //                        //    if (itemEmail["Status"] == "Success")
        //                        //    {
        //                        //        //crmleadguid = itemEmail["Message"]["RelatedId"].ToString();
        //                        //        //logger.write("///////////////////International Admission LSQ for " + obj.name + " RE - Send Using EMAIL Success  Response Received : " + textEmail + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");
        //                        //    }

        //                        //    else
        //                        //    {
        //                        //       // logger.write("///////////////////International Admission LSQ for " + obj.name + " RE - Send Using EMAIL Failed  Response Received : " + textEmail + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");
        //                        //    }
        //                        //}
        //                    }
        //                }
        //                //logger.write("///////////////////International Admission LSQ for " + obj.name + " Response Received : " + text + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");


        //            }
        //        }
        //        catch (WebException ex)
        //        {
        //            string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
        //            //logger.write("///////////////////International Admission LSQ for " + obj.name + " Response Response Received ERROR CATCH : " + message.ToString() + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");


        //        }


        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //}
        private static Dictionary<string, string> CourseDic = new Dictionary<string, string> {
            {"1","M.Tech"},
            {"2","B.Tech"},
            {"3","LLB"},
            {"4","MBA"},
            {"5","LLM"},
            {"6","BBA"},
            {"7","M.Des"},
            {"8","B.Des"},
            {"9","MSIE"},
            {"10","B.Plan"},
            {"11","M.Plan"},
            {"12","BA"},
            {"13","MA"},
            {"14","MAEE"},
            {"15","BFA"},
            {"16","B.Tech LLB"},
            {"17","BCA"},
            {"18","BAEE"},
            {"19","B.Com"},
            {"20","B.Sc"},
            {"21","B.Pharma"},
            {"22","D.Pharma"},
            {"23","M.Sc"},
            {"24","B.Optometry"},
            {"25","B.Physiotherapy"},
            {"26","PGD"}
            };
        //SF_json = SF_json + "\"CourseType\":\"" + (study == "UG" ? "Under Graduate" : "Post Graduate") + "\",";
        private static Dictionary<string, string> Coursetype = new Dictionary<string, string> {
            {"1","Post Graduate"},
            {"2","Under Graduate"},
            {"3","Under Graduate"},
            {"4","Post Graduate"},
            {"5","Post Graduate"},
            {"6","Under Graduate"},
            {"7","Post Graduate"},
            {"8","Under Graduate"},
            {"9","Post Graduate"},
            {"10","Under Graduate"},
            {"11","Post Graduate"},
            {"12","Under Graduate"},
            {"13","Post Graduate"},
            {"14","Post Graduate"},
            {"15","Under Graduate"},
            {"16","Under Graduate"},
            {"17","Under Graduate"},
            {"18","Under Graduate"},
            {"19","Under Graduate"},
            {"20","Under Graduate"},
            {"21","Under Graduate"},
            {"22","Post Graduate"},
            {"23","Post Graduate"},
            {"24","Under Graduate"},
            {"25","Under Graduate"},
            {"26","Post Graduate"}
            };

        public string GetCourseType(string courseid)
        {
            try
            {
                string CoursetypeValue = "";
                if (Coursetype.TryGetValue(courseid, out CoursetypeValue))
                {
                    return CoursetypeValue;
                }
                else
                {
                    return "Under Graduate";
                }
            }
            catch (Exception ex)
            {
                return "Under Graduate";
            }



        }
        public string GetCourseName(string courseid)
        {
            try
            {
                string CourseName = "";
                if (CourseDic.TryGetValue(courseid, out CourseName))
                {
                    return CourseName;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "";
            }



        }
    }
}