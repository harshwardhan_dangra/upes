﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using upes_website.Gutils;
using upes_website.GUtils;
using upes_website.Models;

namespace upes_website.Controller
{
    public class EnquiryCourseController : SurfaceController
    {
        // GET: EnquiryCourse
        DBUtils dbu = new DBUtils();
        Utils u = new Utils();
        //Logger logger = new Logger();
        string name = "", EmailId = "", mobile = "", stateId = "", Course = "", city = "", prospectId = "", SrcId = "";
        
        public ActionResult ModelCall(Enquiry_Course_Model model)
        {
            String sta = ModelState.IsValid.ToString();
            if (ModelState.IsValid)
            {

            }
            return Content(sta);
        }
        public ActionResult Index()
        {
            return PartialView("EnquiryCourseView");
        }
        public ActionResult CourseIndex()
        {
            return PartialView("CourseEnquiryView");
        }
        //[HttpPost]
        //[AllowAnonymous]
        public ActionResult HandleSubmit(string Name, string emailId, string mobile, string stateId, string course, string City, string prospectId, string srcId, string year)
        {
            string status = "", courseID = "", state = "", city = "", getLeadSrc = "",Year ="";
            bool modelState = ModelState.IsValid;
            string sta = modelState.ToString();
            var types = Umbraco.TypedContent(srcId);
            if (types.HasProperty("courseName") && types.HasValue("courseName"))
            {
                status = types.GetPropertyValue<string>("courseName");
            }
            if(course==null)
            {
                course = status;
            }
            if (Name != "" && emailId != "" && mobile != "" && stateId != "" && course != "" && City != "" && srcId != "")
            {
                name = Name;
                EmailId = emailId;
                state = stateId;
                Course = course;
                city = City;
                SrcId = srcId;
                Year = year;
                Session["Name"] = Name.TrimEnd();
                Session["State"] = state;
                Session["City"] = city;
                Session["emailId"] = emailId;
                Session["mobile"] = mobile;
                Session["ProspectId"] = prospectId;
                Session["course"] = course;
                Session["srcId"] = srcId;
                Session["year"] = year;
            }
            //if (types.HasProperty("courseName") &&types.HasValue("courseName"))
            //{
            //    status = types.GetPropertyValue<string>("courseName");
            //}
            //if(types.HasProperty("sourceName") &&types.HasValue("sourceName"))
            //{
            //    getLeadSrc= types.GetPropertyValue<string>("sourceName");
            //}
            //if (Name!=""&&emailId!=""&&mobile!=""&&stateId!=""&&course!=""&&City!="")
            //{
            var db = DatabaseContext.ConnectionString;
            Session["mobileNumber"] = mobile;
            string qry2 = "select STATENAME from STBL_STATE where STID=" + stateId + "";
            string qry1 = "select cityname from STBL_CITY where city_id=" + City + "";
            string query = "select C_NAME from STBL_COURSE where C_ID=" + course + "";
            //    string Datasource = "Online Leads", Lead_Subsource1 = "Education/Other Websites", Source = status+getLeadSrc;
            //    //var httpWebRequest = (HttpWebRequest)WebRequest.Create("");
            //    string LeadSquareurl = "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.CreateOrUpdate?postUpdatedLead=false&?accessKey=u$r8ae69b5fd7e1dc02c40f62a5bbf760bf&secretKey=713e7779425be94d70b7938881df93b3bb940670";
            ////}

            //var httpWebRequest = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Method = "POST";
            //try
            //{
            //    DataSet ds = dbu.executeQuery(query);
            //    if (ds.Tables[0].Rows[0] != null)
            //    {
            //        courseID = ds.Tables[0].Rows[0]["C_NAME"].ToString();
            //    }
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
            courseID = course;
            try
            {
                DataSet ds = dbu.executeQuery(qry2);
                if (ds.Tables[0].Rows[0] != null)
                {
                    state = ds.Tables[0].Rows[0]["STATENAME"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            try
            {
                DataSet ds = dbu.executeQuery(qry1);
                if (ds.Tables[0].Rows[0] != null)
                {
                    city = ds.Tables[0].Rows[0]["cityname"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            string maxiD = "";
            string qry4 = "select max(eid)+1 as id from quick_enquiry";
            try
            {
                DataSet ds = dbu.executeQuery(qry4);
                if (ds.Tables[0].Rows[0] != null)
                {
                    maxiD = ds.Tables[0].Rows[0]["id"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            string qry = "insert into quick_enquiry(eid,fname,mobile,email,city,program,trdate,query,status,uname,query_type,crminsertflag,iscrmexception,exceptiondetails) values(" + maxiD + ",'" + Name + "','" + mobile + "','" + emailId + "','" + city + "','" + courseID + "',getdate(),'NULL',1,'" + Name + "','NULL',0,0,'NULL')";
            try
            {
                DataSet redulu = dbu.executeQuery(qry);
                if (redulu != null)
                {
                    var status1 = true;
                }
            }
            catch (Exception e)
            {
                return Content("Data insertion Failure");
            }
            //string json = "[";
            //json = json + "{\"Attribute\":\"FirstName\",\"Value\":\"" + Name.TrimEnd() + "\"},";
            //json = json + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
            //json = json + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
            //json = json + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + emailId + "\"},";
            //json = json + "{\"Attribute\":\"Phone\",\"Value\":\"" + mobile + "\"},";
            //json = json + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
            //json = json + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
            //if (prospectId != "NA" && prospectId != "" && prospectId != null)
            //{
            //    json = json + "{\"Attribute\":\"ProspectID\",\"Value\":\"" + prospectId + "\"},";
            //}
            //json = json + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
            //json = json + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + course + "\"},";
            //json = json + "{\"Attribute\":\"SearchBy\",\"Value\":\"Phone\"}";
            //json = json + "]";
            //string json2 = "[";
            //json2 = json2 + "{\"Attribute\":\"FirstName\",\"Value\":\"" + Name.TrimEnd() + "\"},";
            //json2 = json2 + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
            //json2 = json2 + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
            //json2 = json2 + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + emailId + "\"},";
            //json2 = json2 + "{\"Attribute\":\"Phone\",\"Value\":\"" + mobile + "\"},";
            //json2 = json2 + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
            //json2 = json2 + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
            //json2 = json2 + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
            //json2 = json2 + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + course + "\"},";
            //json2 = json2 + "{\"Attribute\":\"SearchBy\",\"Value\":\"Phone\"}";
            //json2 = json2 + "]";
            //    try
            //    {
            //        using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //        {
            //            //logger.write("///////////////////Registration Stage JSON " + json + "///////////////////");
            //            streamWriter.Write(json);
            //            streamWriter.Flush();
            //            streamWriter.Close();
            //        }
            //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //        {
            //            var text = streamReader.ReadToEnd();
            //            JavaScriptSerializer serializer = new JavaScriptSerializer();
            //            dynamic item = serializer.Deserialize<object>(text);
            //            if (item["Status"] == "Success")
            //            {
            //                var crmleadguid = item["Message"]["RelatedId"].ToString();
            //                string qry5 = "insert into tbl_enquirynow_log(studId,response,relatedId) values(" + maxiD + ",'" + text + "','" + crmleadguid + "')";
            //                try
            //                {
            //                    DataSet redulu = dbu.executeQuery(qry5);
            //                    if (redulu != null)
            //                    {
            //                        status = "true";
            //                    }
            //                }
            //                catch (Exception e)
            //                {
            //                    return Content("Logger insertion Failure");
            //                }
            //            }

            //            //logger.write("///////////////////Response for Registration Stage Student Name:" + firstname + " Response Received : " + text + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");
            //        }
            //    }
            //    catch (WebException e)
            //    {
            //        string message = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
            //        try
            //        {
            //            using (StreamWriter streamWriter2 = new StreamWriter(httpWebRequest.GetRequestStream()))
            //            {
            //                //logger.write("///////////////////Registration Stage JSON " + json + "///////////////////");
            //                streamWriter2.Write(json2);
            //                streamWriter2.Flush();
            //                streamWriter2.Close();
            //            }
            //            var httpResponse1 = (HttpWebResponse)httpWebRequest.GetResponse();
            //            using (var streamReader2 = new StreamReader(httpResponse1.GetResponseStream()))
            //            {
            //                var text = streamReader2.ReadToEnd();
            //                JavaScriptSerializer serializer1 = new JavaScriptSerializer();
            //                dynamic item = serializer1.Deserialize<object>(text);
            //                if (item["Status"] == "Success")
            //                {
            //                    var crmleadguid = item["Message"]["RelatedId"].ToString();
            //                    string qry5 = "insert into tbl_enquirynow_log(studId,response,relatedId) values(" + maxiD + ",'" + text + "','" + crmleadguid + "')";
            //                    try
            //                    {
            //                        DataSet redulu = dbu.executeQuery(qry5);
            //                        if (redulu != null)
            //                        {
            //                            status = "true";
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        return Content("Logger insertion Failure");
            //                    }
            //                }

            //                //logger.write("///////////////////Response for Registration Stage Student Name:" + firstname + " Response Received : " + text + " Date: " + DateTime.Now.Date.ToString("dd/MM/yyyy") + "///////////////////");
            //            }
            //        }
            //        catch (Exception excep)
            //        {
            //            return Content(excep.ToString());
            //        }
            //        return Content(message);
            //    }
            //    catch (Exception exc)
            //    {
            //        return Content(exc.ToString());
            //    }

            //}
            //else
            //{

            //    status = "false";
            //    return CurrentUmbracoPage();
            //}
            ModelState.Clear();
            generateotp();
            return Content(sta);
        }
        //[HttpPost]
        public JsonResult generateotp()
        {
            string sendotp = "";
            string jsonresult = "", phone = Session["mobileNumber"].ToString();
            try
            {
                string result = "";



                Random r = new Random();
                string OTP = r.Next(1000, 9999).ToString();
                string Message =  "Thank you for showing interest in UPES Dehradun. Your one time password (OTP) to receive ongoing program related information from UPES Dehradun is " + OTP;
                sendotp = OTP;
                string url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=340783&username=9818212886&password=wwptp&To=" + phone + "&Text=" + Message + " & time=&senderid=UPESIN";

                try
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)
                    WebRequest.Create(url);
                    httpRequest.Method = WebRequestMethods.Http.Get;
                    HttpWebResponse objResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader sr =
                       new StreamReader(objResponse.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                        sr.Close();
                    }
                }
                catch (Exception ex)
                {
                    result = ex.ToString();

                }

                if (result.Contains("<ERROR>"))
                {
                    Session["userotp"] = "";

                    jsonresult = "otp not send" + "###" + "0";

                    //errorlog(jsonresult, "smsotp", "smsotp", Convert.ToInt32(Session["student_id"]));
                    string fromname = "UPES";
                    string fromaddress = "enrollments@upes.ac.in";
                    string subject = "error otp";
                    string cc = "maris@newgendigital.com";
                    string email = "prasanth@newgendigital.com";

                    //string mailcommonresult = mail_common(fromname, fromaddress, jsonresult, email, subject, cc);


                }
                else
                {
                    Session["userotp"] = OTP;

                    jsonresult = "Enter OTP sent to your mobile number" + "###" + OTP;
                }



                string insert = "insert into tb_1001_sms_otp(otp_message,otp,name,email,mobile,response_message,visible,created_on)values('" + Message + "','" + OTP + "','','','" + phone + "','" + u.SqlFriendly(result.Trim()) + "',1,getdate())";
                dbu.executeQuery(insert);

            }
            catch (Exception ex)
            {
                jsonresult = ex.ToString();


                //errorlog(ex.ToString(), "smsotp", "smsotp", Convert.ToInt32(Session["student_id"]));


                string fromname = "UPES";
                string fromaddress = "enrollments@upes.ac.in";
                string subject = "Error OTP";
                //string cc = "nandakumar@newgendigital.com";
                string email = "nandakumar@newgendigital.com";

                //string mailcommonresult = mail_common(fromname, fromaddress, jsonresult, email, subject, cc);

            }
            return Json(jsonresult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult verifyOTP(string otp)
        {
            string htmlString = "";
            string status = "", courseID = "", state = "", city = "", getLeadSrc = "", otpStatus = "",courseName="", SalesForceID="",courseType="", query = "";
            var types = Umbraco.TypedContent(Session["srcId"].ToString());
            string TokenURL = "https://login.salesforce.com/services/oauth2/token?password=P@55w0rd@21&client_secret=3E9FA44833FD5DF9B1B2672285FC09451B45BC776CFDDBF9AA21115FE3E1C893&client_id=3MVG9G9pzCUSkzZvwO4lhD68m6r7VS9I.BYScwhAim6E07s8Lh9tteGSfPyWMKlzRkpRGhHJZgBVYT2gZjRdC&username=system.admin@haloocom.com&grant_type=password";
            string SalesForceURL = "https://upes.my.salesforce.com/services/apexrest/CreateLead1/";
            string SalesForceEditURL = "https://upes.my.salesforce.com/services/data/v45.0/sobjects/Lead/";
            if (otp == Session["userotp"].ToString())
            {
                htmlString = "success";
                if (types.HasProperty("courseName") && types.HasValue("courseName"))
                {
                    status = types.GetPropertyValue<string>("courseName");
                }
                if (types.HasProperty("sourceName") && types.HasValue("sourceName"))
                {
                    getLeadSrc = types.GetPropertyValue<string>("sourceName");
                }
                var studId = Session["Name"].ToString();
                if (Session["Name"].ToString() != "" && Session["emailId"].ToString() != "" && Session["mobile"].ToString() != "" && Session["State"].ToString() != "" && Session["course"].ToString() != "" && Session["City"].ToString() != "")
                {
                    var db = DatabaseContext.ConnectionString;
                    Session["mobileNumber"] = mobile;
                    string qry2 = "select STATENAME from STBL_STATE where STID=" + Session["State"].ToString() + "";
                    string qry1 = "select cityname from STBL_CITY where city_id=" + Session["City"].ToString() + "";
                    Regex regex = new Regex(@"^\d$");
                    if(!regex.IsMatch(Session["course"].ToString()))
                    {
                        query = "select C_NAME,COURSETYPE from STBL_COURSE where C_NAME='"+ status + "'";
                    }
                    else
                    {
                        query = "select C_NAME,COURSETYPE from STBL_COURSE where C_ID=" + Session["course"].ToString() + "";

                    }
                    //string query = "select C_NAME,COURSETYPE from STBL_COURSE where C_ID=" + Session["course"].ToString() + "";
                    string Datasource = "Online Leads", Lead_Subsource1 = "Education/Other Websites", Source = status + getLeadSrc;
                    //var httpWebRequest = (HttpWebRequest)WebRequest.Create("");
                    string LeadSquareurl = "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Capture?accessKey=u$r8ae69b5fd7e1dc02c40f62a5bbf760bf&secretKey=713e7779425be94d70b7938881df93b3bb940670";
                    //}

                    //var httpWebRequest = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
                    //httpWebRequest.ContentType = "application/json";
                    //httpWebRequest.Method = "POST";
                    courseID = Course;
                    try
                    {
                        DataSet ds = dbu.executeQuery(qry2);
                        if (ds.Tables[0].Rows[0] != null)
                        {
                            state = ds.Tables[0].Rows[0]["STATENAME"].ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    try
                    {
                        DataSet ds = dbu.executeQuery(qry1);
                        if (ds.Tables[0].Rows[0] != null)
                        {
                            city = ds.Tables[0].Rows[0]["cityname"].ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    string maxiD = "";
                    string qry4 = "select max(eid)+1 as id from quick_enquiry";
                    try
                    {
                        DataSet ds = dbu.executeQuery(qry4);
                        if (ds.Tables[0].Rows[0] != null)
                        {
                            maxiD = ds.Tables[0].Rows[0]["id"].ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    try
                    {
                        DataSet ds = dbu.executeQuery(query);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            courseName = ds.Tables[0].Rows[0]["C_NAME"].ToString();
                            courseType = ds.Tables[0].Rows[0]["COURSETYPE"].ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception" + e);
                    }
                    if(courseType== "UG")
                    {
                        courseType = "Under Graduate";
                    }
                    else if(courseType=="PG")
                    {
                        courseType = "Post Graduate";
                    }
                    string qry = "insert into quick_enquiry(eid,fname,mobile,email,city,program,trdate,query,status,uname,query_type,crminsertflag,iscrmexception,exceptiondetails) values(" + maxiD + ",'" + Session["Name"].ToString() + "','" + Session["mobile"].ToString() + "','" + Session["emailId"].ToString() + "','" + Session["City"].ToString() + "','" + Session["course"].ToString() + "',getdate(),'NULL',1,'" + Session["Name"].ToString() + "','NULL',0,0,'NULL')";
                    string salesForceData = "{";
                    salesForceData += "\"FirstName\":\"\",";
                    salesForceData += "\"MiddleName\":\"\",";
                    salesForceData += "\"LastName\":\"" + Session["Name"].ToString().TrimEnd() + "\",";
                    salesForceData += "\"Email\":\"" + Session["emailId"].ToString().TrimEnd() + "\",";
                    salesForceData += "\"MobilePhone\":\"" + Session["mobile"].ToString().TrimEnd() + "\",";
                    salesForceData += "\"SelectCourse\":\"" + courseName + "\",";
                    salesForceData += "\"CourseType\":\"" + courseType + "\",";
                    salesForceData += "\"Country\":\"India\",";
                    salesForceData += "\"State\":\"" + state + "\",";
                    salesForceData += "\"IndiaCity\":\"" + city + "\",";
                    //salesForceData += "\"CourseType\":\"" + city + "\",";
                    salesForceData += "\"Status\":\"Open\",";
                    salesForceData += "\"DataSource\":\"" + Datasource + "\",";
                    salesForceData += "\"Source\":\"" + Source + "\",";
                    salesForceData += "\"SubSource\":\"" + Lead_Subsource1 + "\"";
                    salesForceData += "}";
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                    var webRequest = (HttpWebRequest)WebRequest.Create(TokenURL);
                    webRequest.ContentType = "application/json";
                    webRequest.Method = "POST";

                    try
                    {
                        //Json input to API
                        using (var SF_streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                        {
                            //logger.write("///////////////////International Admission LSQ REQ Json : " + json + "///////////////////");
                            //streamWriter.Write(json);
                            SF_streamWriter.Flush();
                            SF_streamWriter.Close();
                        }
                        var webResponse = (HttpWebResponse)webRequest.GetResponse();

                        using (var SF_streamReader = new StreamReader(webResponse.GetResponseStream()))
                        {
                            var SF_texttoken = SF_streamReader.ReadToEnd();
                            if (SF_texttoken != null && SF_texttoken != "")
                            {
                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                dynamic SF_item = serializer.Deserialize<object>(SF_texttoken);
                                if (!string.IsNullOrEmpty(SF_item["access_token"]))
                                {
                                    string SF_Access_token = "Bearer " + SF_item["access_token"];

                                    webRequest = (HttpWebRequest)WebRequest.Create(SalesForceURL);
                                    webRequest.Headers.Add("authorization", SF_Access_token);
                                    webRequest.ContentType = "application/json";
                                    webRequest.Method = "POST";
                                    webRequest.ContentLength = salesForceData.Length;
                                    webRequest.KeepAlive = false;

                                    using (StreamWriter SF_requestWriter2 = new StreamWriter(webRequest.GetRequestStream()))
                                    {
                                        SF_requestWriter2.Write(salesForceData);
                                    }

                                    var SF_httpResponse1 = (HttpWebResponse)webRequest.GetResponse();
                                    using (var SF_responseReader = new StreamReader(SF_httpResponse1.GetResponseStream()))
                                    {
                                        var SF_text_Data = SF_responseReader.ReadToEnd();
                                        if (SF_text_Data != null && SF_text_Data != "")
                                        {
                                            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                                            dynamic SF_item1 = json_serializer.Deserialize<object>(SF_text_Data);
                                            if(SF_item1!=null)
                                            {
                                                string pushSaleforceData = "insert into SalesForceLog(Stud_Id,Message,created_on) values('"+Session["Name"].ToString()+"','"+ SF_text_Data + "',GETDATE())";
                                                try
                                                {
                                                    DataSet redulu = dbu.executeQuery(pushSaleforceData);
                                                    if (redulu != null)
                                                    {
                                                        status = "true";
                                                    }
                                                }
                                                catch(Exception e)
                                                {

                                                }
                                            }
                                            if (SF_item1["id"] != null)
                                            {
                                                if (!string.IsNullOrEmpty(SF_item1["id"]))
                                                {
                                                    SalesForceID = Convert.ToString(SF_item1["id"]);
                                                }
                                            }
                                            else
                                            {
                                                string msg = Convert.ToString(SF_item1["message"]);
                                                SalesForceID = msg;
                                            }

                                        }


                                    }



                                    //crmleadguid = item["Message"]["RelatedId"].ToString();
                                    //logger.write("///////////////////International Admission LSQ for " + obj.name + "  Success : RelatedId" + crmleadguid + " ///");
                                }
                                else if (SF_item["Status"] == "Error")
                                {


                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();


                    }
                string json = "[";
                    json = json + "{\"Attribute\":\"FirstName\",\"Value\":\"" + Session["Name"].ToString().TrimEnd() + "\"},";
                    json = json + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
                    json = json + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
                    json = json + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + Session["emailId"].ToString() + "\"},";
                    json = json + "{\"Attribute\":\"Phone\",\"Value\":\"" + Session["mobile"].ToString() + "\"},";
                    json = json + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
                    json = json + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
                    if (Session["ProspectId"].ToString() != "NA" && Session["ProspectId"].ToString() != "" && Session["ProspectId"].ToString() != null)
                    {
                        json = json + "{\"Attribute\":\"ProspectID\",\"Value\":\"" + Session["ProspectId"].ToString() + "\"},";
                    }
                    json = json + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
                    json = json + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + Session["course"].ToString() + "\"},";
                    json = json + "{\"Attribute\":\"mx_Course\",\"Value\":\"" + status + "\"},";
                    json = json + "{\"Attribute\":\"ProspectStage\",\"Value\":\"ANP- Verified\"},";
                    json = json + "{\"Attribute\":\"SearchBy\",\"Value\":\"Phone\"}";
                    json = json + "]";
                    string json2 = "[";
                    json2 = json2 + "{\"Attribute\":\"FirstName\",\"Value\":\"" + Session["Name"].ToString().TrimEnd() + "\"},";
                    json2 = json2 + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
                    json2 = json2 + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
                    json2 = json2 + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + Session["emailId"].ToString() + "\"},";
                    //json2 = json2 + "{\"Attribute\":\"Phone\",\"Value\":\"" + Session["mobile"] .ToString()+ "\"},";
                    json2 = json2 + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
                    json2 = json2 + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
                    json2 = json2 + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
                    json2 = json2 + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + Session["course"].ToString() + "\"},";
                    json2 = json2 + "{\"Attribute\":\"mx_Course\",\"Value\":\"" + status + "\"},";
                    json2 = json2 + "{\"Attribute\":\"ProspectStage\",\"Value\":\"ANP- Verified\"}";
                    json2 = json2 + "]";
                    //try
                    //{
                    //    using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    //    {
                    //        streamWriter.Write(json);
                    //        streamWriter.Flush();
                    //        streamWriter.Close();
                    //    }
                    //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    //    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    //    {
                    //        var text = streamReader.ReadToEnd();
                    //        JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //        dynamic item = serializer.Deserialize<object>(text);
                    //        if (item["Status"] == "Success")
                    //        {
                    //            var crmleadguid = item["Message"]["RelatedId"].ToString();
                    //            string qry5 = "insert into tbl_enquirynow_log(studId,response,relatedId) values(" + maxiD + ",'" + text + "','" + crmleadguid + "')";
                    //            try
                    //            {
                    //                DataSet redulu = dbu.executeQuery(qry5);
                    //                if (redulu != null)
                    //                {
                    //                    status = "true";
                    //                }
                    //            }
                    //            catch (Exception exa)
                    //            {
                    //                return Content(exa.ToString());
                    //            }
                    //        }
                    //    }
                    //}
                    //catch (WebException e)
                    //{
                    //    string finalJSON1 = json;
                    //    string message = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                    //    finalJSON1 = json2;
                    //    string leadidbyemail = getLeadIdbyEmail(Session["emailId"].ToString());

                    //    if (leadidbyemail != null && leadidbyemail != "NA" && leadidbyemail != string.Empty)
                    //    {
                    //        LeadSquareurl = " https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Update?accessKey=u$r8ae69b5fd7e1dc02c40f62a5bbf760bf&secretKey=713e7779425be94d70b7938881df93b3bb940670&leadId=" + leadidbyemail;
                    //    }
                    //    if (e != null)
                    //    {
                    //        message = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                    //    }
                    //    JavaScriptSerializer serializer12 = new JavaScriptSerializer();
                    //    dynamic item12 = "";
                    //    if (message != null && message != "")
                    //    {
                    //        item12 = serializer12.Deserialize<object>(message);

                    //        string expMSG12 = "init";

                    //        if (item12["ExceptionMessage"] != null && item12["ExceptionMessage"] != string.Empty)
                    //        {
                    //            expMSG12 = Convert.ToString(item12["ExceptionMessage"]);
                    //        }
                    //        if (expMSG12 == "A Lead with same Email already exists.")
                    //        {

                    //        }
                    //    }
                    //    try
                    //    {
                    //        var httpWebRequest14 = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
                    //        httpWebRequest14.ContentType = "application/json";
                    //        httpWebRequest14.Method = "POST";
                    //        using (var streamWriter14 = new StreamWriter(httpWebRequest14.GetRequestStream()))
                    //        {
                    //            streamWriter14.Write(json2);
                    //            streamWriter14.Flush();
                    //            streamWriter14.Close();
                    //        }
                    //        var httpResponse1 = (HttpWebResponse)httpWebRequest14.GetResponse();
                    //        using (var streamReader2 = new StreamReader(httpResponse1.GetResponseStream()))
                    //        {
                    //            var text = streamReader2.ReadToEnd();
                    //            JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                    //            dynamic item = serializer1.Deserialize<object>(text);
                    //            if (item["Status"] == "Success")
                    //            {
                    //                htmlString = "success";
                    //            }
                    //        }
                    //    }
                    //    catch (Exception excep)
                    //    {
                    //        return Content(excep.ToString());
                    //    }
                    //    if (otp == Session["userotp"].ToString())
                    //    {
                    //        otpStatus = "Verified";
                    //    }
                    //    else
                    //        otpStatus = "NotVerified";
                    //    return Content(htmlString + "###" + otpStatus);
                    //}
                    //catch (Exception exc)
                    //{
                    //    if (otp == Session["userotp"].ToString())
                    //    {
                    //        otpStatus = "Verified";
                    //    }
                    //    else
                    //        otpStatus = "NotVerified";
                    //    return Content(exc.ToString());
                    //}

                }
            }
            else
            {
                htmlString = "failure";
            }
            if (otp == Session["userotp"].ToString())
            {
                otpStatus = "Verified";
            }
            else
                otpStatus = "NotVerified";
            return Content(htmlString + "###" + otpStatus);
        }
        public string getLeadIdbyEmail(string emailid)
        {

            string leadid = "NA";
            string LeadSquareurl = "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Leads.GetByEmailaddress?accessKey=u$r8ae69b5fd7e1dc02c40f62a5bbf760bf&secretKey=713e7779425be94d70b7938881df93b3bb940670&emailaddress=" + emailid;

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;




            try
            {
                var httpWebRequest11 = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
                httpWebRequest11.Method = "GET";

                var httpResponse11 = (HttpWebResponse)httpWebRequest11.GetResponse();
                using (var streamReader11 = new StreamReader(httpResponse11.GetResponseStream()))
                {
                    var text11 = streamReader11.ReadToEnd();
                    JavaScriptSerializer serializer11 = new JavaScriptSerializer();

                    dynamic item11 = "";
                    if (text11 != null && text11 != "")
                    {
                        item11 = serializer11.Deserialize<object>(text11);
                        string expMSGs = "init";
                        string vl = item11[0]["ProspectID"];
                        string v = "";
                        if ((item11[0]["ProspectID"] != null) && (item11[0]["ProspectID"] != ""))
                        {

                            leadid = Convert.ToString(item11[0]["ProspectID"]);
                        }


                    }
                }
            }

            catch (Exception ee)
            {

                Console.Write("get lead id by email catch for " + emailid + " : " + ee.StackTrace.ToString());

            }




            return leadid;
        }
        public ActionResult enQuiryCourses(string Name, string emailId, string mobile, string stateId, string course, string City, string prospectId, string srcId)
        {
            string htmlString = "";
            string status = "", courseID = "", state = "", city = "", getLeadSrc = "", SalesForceID = "", courseType = "";
            bool modelState = ModelState.IsValid;
            string sta = modelState.ToString();
            var types = Umbraco.TypedContent(srcId);
            string TokenURL = "https://login.salesforce.com/services/oauth2/token?password=Zoxima@2019&client_secret=3E9FA44833FD5DF9B1B2672285FC09451B45BC776CFDDBF9AA21115FE3E1C893&client_id=3MVG9G9pzCUSkzZvwO4lhD68m6r7VS9I.BYScwhAim6E07s8Lh9tteGSfPyWMKlzRkpRGhHJZgBVYT2gZjRdC&username=shubham.verma1@zoxima.com&grant_type=password";
            string SalesForceURL = "https://upes.my.salesforce.com/services/apexrest/CreateLead1/";
            string SalesForceEditURL = "https://upes.my.salesforce.com/services/data/v45.0/sobjects/Lead/";
            if (Name != "" && emailId != "" && mobile != "" && stateId != "" && course != "" && City != "" && srcId != "")
            {
                name = Name;
                EmailId = emailId;
                state = stateId;
                Course = course;
                city = City;
                SrcId = srcId;
                Session["Name"] = Name.TrimEnd();
                Session["State"] = state;
                Session["City"] = city;
                Session["emailId"] = emailId;
                Session["mobile"] = mobile;
                Session["ProspectId"] = prospectId;
                Session["course"] = course;
                Session["srcId"] = srcId;
            }
            var db = DatabaseContext.ConnectionString;
            Session["mobileNumber"] = mobile;
            string qry2 = "select STATENAME from STBL_STATE where STID=" + stateId + "";
            string qry1 = "select cityname from STBL_CITY where city_id=" + City + "";
            string query = "select C_NAME,COURSETYPE from STBL_COURSE where C_ID=" + course + "";
            string Datasource = "Online Leads", Lead_Subsource1 = "Education/Other Websites", Source = status + getLeadSrc;
            string LeadSquareurl = "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Capture?accessKey=u$r8ae69b5fd7e1dc02c40f62a5bbf760bf&secretKey=713e7779425be94d70b7938881df93b3bb940670";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            try
            {
                DataSet ds = dbu.executeQuery(query);
                if (ds.Tables[0].Rows[0] != null)
                {
                    courseID = ds.Tables[0].Rows[0]["C_NAME"].ToString();
                    courseType = ds.Tables[0].Rows[0]["COURSETYPE"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            courseID = course;
            try
            {
                DataSet ds = dbu.executeQuery(qry2);
                if (ds.Tables[0].Rows[0] != null)
                {
                    state = ds.Tables[0].Rows[0]["STATENAME"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            try
            {
                DataSet ds = dbu.executeQuery(qry1);
                if (ds.Tables[0].Rows[0] != null)
                {
                    city = ds.Tables[0].Rows[0]["cityname"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            string maxiD = "";
            string qry4 = "select max(eid)+1 as id from quick_enquiry";
            try
            {
                DataSet ds = dbu.executeQuery(qry4);
                if (ds.Tables[0].Rows[0] != null)
                {
                    maxiD = ds.Tables[0].Rows[0]["id"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            string qry = "insert into quick_enquiry(eid,fname,mobile,email,city,program,trdate,query,status,uname,query_type,crminsertflag,iscrmexception,exceptiondetails) values(" + maxiD + ",'" + Name + "','" + mobile + "','" + emailId + "','" + city + "','" + courseID + "',getdate(),'NULL',1,'" + Name + "','NULL',0,0,'NULL')";
            try
            {
                DataSet redulu = dbu.executeQuery(qry);
                if (redulu != null)
                {
                    var status1 = true;
                }
            }
            catch (Exception e)
            {
                return Content("Data insertion Failure");
            }
            string salesForceData = "{";
            salesForceData += "\"FirstName\":\"\",";
            salesForceData += "\"MiddleName\":\"\",";
            salesForceData += "\"LastName\":\"" + Name + "\",";
            salesForceData += "\"Email\":\"" + emailId + "\",";
            salesForceData += "\"MobilePhone\":\"" + mobile + "\",";
            salesForceData += "\"SelectCourse\":\"" + courseID + "\",";
            salesForceData += "\"CourseType\":\"" + courseType + "\",";
            salesForceData += "\"Country\":\"India\",";
            salesForceData += "\"State\":\"" + state + "\",";
            salesForceData += "\"IndiaCity\":\"" + city + "\",";
            //salesForceData += "\"CourseType\":\"" + city + "\",";
            salesForceData += "\"Status\":\"Open\",";
            salesForceData += "\"DataSource\":\"" + Datasource + "\",";
            salesForceData += "\"Source\":\"" + Source + "\",";
            salesForceData += "\"SubSource\":\"" + Lead_Subsource1 + "\"";
            salesForceData += "}";
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            var webRequest = (HttpWebRequest)WebRequest.Create(TokenURL);
            webRequest.ContentType = "application/json";
            webRequest.Method = "POST";

            try
            {
                //Json input to API
                using (var SF_streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    //logger.write("///////////////////International Admission LSQ REQ Json : " + json + "///////////////////");
                    //streamWriter.Write(json);
                    SF_streamWriter.Flush();
                    SF_streamWriter.Close();
                }
                var webResponse = (HttpWebResponse)webRequest.GetResponse();

                using (var SF_streamReader = new StreamReader(webResponse.GetResponseStream()))
                {
                    var SF_texttoken = SF_streamReader.ReadToEnd();
                    if (SF_texttoken != null && SF_texttoken != "")
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        dynamic SF_item = serializer.Deserialize<object>(SF_texttoken);
                        if (!string.IsNullOrEmpty(SF_item["access_token"]))
                        {
                            string SF_Access_token = "Bearer " + SF_item["access_token"];

                            webRequest = (HttpWebRequest)WebRequest.Create(SalesForceURL);
                            webRequest.Headers.Add("authorization", SF_Access_token);
                            webRequest.ContentType = "application/json";
                            webRequest.Method = "POST";
                            webRequest.ContentLength = salesForceData.Length;
                            webRequest.KeepAlive = false;

                            using (StreamWriter SF_requestWriter2 = new StreamWriter(webRequest.GetRequestStream()))
                            {
                                SF_requestWriter2.Write(salesForceData);
                            }

                            var SF_httpResponse1 = (HttpWebResponse)webRequest.GetResponse();
                            using (var SF_responseReader = new StreamReader(SF_httpResponse1.GetResponseStream()))
                            {
                                var SF_text_Data = SF_responseReader.ReadToEnd();
                                if (SF_text_Data != null && SF_text_Data != "")
                                {
                                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                                    dynamic SF_item1 = json_serializer.Deserialize<object>(SF_text_Data);
                                    if (SF_item1 != null)
                                    {
                                        string pushSaleforceData = "insert into SalesForceLog(Stud_Id,Message,created_on) values('" + Session["Name"].ToString() + "','" + SF_text_Data + "',GETDATE())";
                                        try
                                        {
                                            DataSet redulu = dbu.executeQuery(pushSaleforceData);
                                            if (redulu != null)
                                            {
                                                status = "true";
                                            }
                                        }
                                        catch (Exception e)
                                        {

                                        }
                                    }
                                    if (SF_item1["id"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(SF_item1["id"]))
                                        {
                                            SalesForceID = Convert.ToString(SF_item1["id"]);
                                        }
                                    }
                                    else
                                    {
                                        string msg = Convert.ToString(SF_item1["message"]);
                                        SalesForceID = msg;
                                    }
                                }
                            }
                            //crmleadguid = item["Message"]["RelatedId"].ToString();
                            //logger.write("///////////////////International Admission LSQ for " + obj.name + "  Success : RelatedId" + crmleadguid + " ///");
                        }
                        else if (SF_item["Status"] == "Error")
                        {


                        }
                    }
                }
            }
            catch (WebException ex)
            {
                string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();


            }
            string json = "[";
            json = json + "{\"Attribute\":\"FirstName\",\"Value\":\"" + Name.TrimEnd() + "\"},";
            json = json + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
            json = json + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
            json = json + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + emailId + "\"},";
            json = json + "{\"Attribute\":\"Phone\",\"Value\":\"" + mobile + "\"},";
            json = json + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
            json = json + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
            if (prospectId != "NA" && prospectId != "" && prospectId != null)
            {
                json = json + "{\"Attribute\":\"ProspectID\",\"Value\":\"" + prospectId + "\"},";
            }
            json = json + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
            json = json + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + course + "\"},";
            json = json + "{\"Attribute\":\"SearchBy\",\"Value\":\"Phone\"}";
            json = json + "]";
            string json2 = "[";
            json2 = json2 + "{\"Attribute\":\"FirstName\",\"Value\":\"" + Name.TrimEnd() + "\"},";
            json2 = json2 + "{\"Attribute\":\"mx_State\",\"Value\":\"" + state + "\"},";
            json2 = json2 + "{\"Attribute\":\"mx_City\",\"Value\":\"" + city + "\"},";
            json2 = json2 + "{\"Attribute\":\"EmailAddress\",\"Value\":\"" + emailId + "\"},";
            json2 = json2 + "{\"Attribute\":\"Phone\",\"Value\":\"" + mobile + "\"},";
            json2 = json2 + "{\"Attribute\":\"mx_Sub_Source\",\"Value\":\"" + Lead_Subsource1 + "\"},";
            json2 = json2 + "{\"Attribute\":\"Source\",\"Value\":\"" + Source + "\"},";
            json2 = json2 + "{\"Attribute\":\"mx_Data_Platform\",\"Value\":\"" + Datasource + "\"},";
            json2 = json2 + "{\"Attribute\":\"mx_Courseid\",\"Value\":\"" + course + "\"}";
            json2 = json2 + "]";
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //logger.write("///////////////////Registration Stage JSON " + json + "///////////////////");
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var text = streamReader.ReadToEnd();
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    dynamic item = serializer.Deserialize<object>(text);
                    if (item["Status"] == "Success")
                    {
                        var crmleadguid = item["Message"]["RelatedId"].ToString();
                        string qry5 = "insert into tbl_enquirynow_log(studId,response,relatedId) values(" + maxiD + ",'" + text + "','" + crmleadguid + "')";
                        try
                        {
                            DataSet redulu = dbu.executeQuery(qry5);
                            if (redulu != null)
                            {
                                status = "true";
                            }
                        }
                        catch (Exception e)
                        {
                            return Content(status);
                        }
                    }
                }
            }
            catch (WebException e)
            {
                string finalJSON1 = json;
                string message = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                finalJSON1 = json2;
                string leadidbyemail = getLeadIdbyEmail(Session["emailId"].ToString());

                if (leadidbyemail != null && leadidbyemail != "NA" && leadidbyemail != string.Empty)
                {
                    LeadSquareurl = " https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Update?accessKey=u$r8ae69b5fd7e1dc02c40f62a5bbf760bf&secretKey=713e7779425be94d70b7938881df93b3bb940670&leadId=" + leadidbyemail;
                }
                if (e != null)
                {
                    message = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                }
                JavaScriptSerializer serializer12 = new JavaScriptSerializer();
                dynamic item12 = "";
                if (message != null && message != "")
                {
                    item12 = serializer12.Deserialize<object>(message);

                    string expMSG12 = "init";

                    if (item12["ExceptionMessage"] != null && item12["ExceptionMessage"] != string.Empty)
                    {
                        expMSG12 = Convert.ToString(item12["ExceptionMessage"]);
                    }
                    if (expMSG12 == "A Lead with same Email already exists.")
                    {

                    }
                }
                try
                {
                    var httpWebRequest14 = (HttpWebRequest)WebRequest.Create(LeadSquareurl);
                    httpWebRequest14.ContentType = "application/json";
                    httpWebRequest14.Method = "POST";
                    using (var streamWriter14 = new StreamWriter(httpWebRequest14.GetRequestStream()))
                    {
                        streamWriter14.Write(json2);
                        streamWriter14.Flush();
                        streamWriter14.Close();
                    }
                    var httpResponse1 = (HttpWebResponse)httpWebRequest14.GetResponse();
                    using (var streamReader2 = new StreamReader(httpResponse1.GetResponseStream()))
                    {
                        var text = streamReader2.ReadToEnd();
                        JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                        dynamic item = serializer1.Deserialize<object>(text);
                        if (item["Status"] == "Success")
                        {
                            htmlString = "success";
                        }
                    }
                }
                catch (Exception excep)
                {
                    return Content(status);
                }
                return Content(status);
            }
            catch (Exception exc)
            {
                return Content(status);
            }
            ModelState.Clear();
            return Content(status);
        }
        public ActionResult Test()
        {
            return Content("TestCont");
        }
    }
}