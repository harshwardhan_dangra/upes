﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;

namespace upes_website.Controller
{
    public class FaqController : UmbracoApiController
    {
        StudentController objStu = new StudentController();
        public class ResponseModel
        {
            public object data { set; get; }
        }

        public class FaqList
        {
            public string question { get; set; }
            public string answer { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            String accessToken = "";
            JObject objResponse;
            int studentStatus = 0;
            try
            {
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];

                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    var cbStudContent = Umbraco.TypedContent(1712);
                    List<FaqList> objArticles = new List<FaqList>();
                    //ARTICLES
                    if (cbStudContent.HasProperty("topics") && cbStudContent.HasValue("topics"))
                    {
                        var arTopics = cbStudContent.GetPropertyValue<IEnumerable<IPublishedContent>>("topics");
                        foreach (var cbContent in arTopics)
                        {
                            FaqList objFaq = new FaqList();

                            if (cbContent.HasValue("accordionTitle"))
                            {
                                objFaq.question = cbContent.GetPropertyValue("accordionTitle").ToString();
                            }
                            else
                            {
                                objFaq.question = "";
                            }
                            if (cbContent.HasValue("accordionContent"))
                            {
                                objFaq.answer = cbContent.GetPropertyValue("accordionContent").ToString();
                            }
                            else
                            {
                                objFaq.answer = "";
                            }

                            objArticles.Add(objFaq);
                        }
                    }
                    ResponseModel responseModel = new ResponseModel();
                    responseModel.data = objArticles;
                    return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }
    }
}
