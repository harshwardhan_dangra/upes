﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using upes_website.Gutils;
using upes_website.GUtils;


namespace upes_website.Controller
{
    public class LoginController : SurfaceController
    {
        DBUtils db = new DBUtils();
        Utils ut = new Utils();
        Crypt cr = new Crypt();
        Mailer ma = new Mailer();
        // GET: Login

        public ActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public string CheckLogin(string email, string password)
        {
            string result = "noinit";
            try
            {
                email = ut.SqlFriendly(email);
                password = cr.Encrypt(ut.SqlFriendly(password));

                //SqlParameter[] Parms = new SqlParameter[3];
                //Parms[0] = new SqlParameter("@email", SqlDbType.VarChar);
                //Parms[0].Value = email;
                //Parms[1] = new SqlParameter("@password", SqlDbType.VarChar);
                //Parms[1].Value = password;
                //Parms[2] = new SqlParameter("@result", SqlDbType.Int);
                //Parms[2].Direction = ParameterDirection.Output;
                //Parms[2].Value = 0;
                //DataSet ds = objDBU.executeSP(true, "sp_login_check_faculty", Parms);

                string strQry = "select * from Addfacultymaster where email = '"+email+ "' and password = '" + password + "'";
                DataSet ds = db.executeQuery(strQry);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Session["facultyid"] = ds.Tables[0].Rows[0]["facultyid"].ToString();
                    Session["facultyname"] = ds.Tables[0].Rows[0]["fname"].ToString();
                    Session["isauth_faculty"] = true;
                    Session["ProfileURL"] = bindFacultyURL(ds.Tables[0].Rows[0]["facultyid"].ToString());
                    result = "yes";
                }
                else
                {
                    result = strQry;
                    
                  
                }

            }
            catch (Exception exe)
            {
                result = exe.ToString();
            }
           
            return result;
        }
        public string ChangePassword(string newpassword)
        {
            string result = "init";
            bool isAuthFaculty = Convert.ToBoolean(Session["isauth_faculty"]);
            string fid = Session["facultyid"].ToString();
            string strQry = "";

            newpassword = cr.Encrypt(ut.SqlFriendly(newpassword));

            DataSet ds = new DataSet();
            if (isAuthFaculty)
            {
                if (fid != "" || fid != null)
                {

                    strQry = "update Addfacultymaster set password = '" + newpassword + "' where facultyid = '" + fid + "'";
                    try
                    {
                        ds = db.executeQuery(strQry);
                        result = "yes";

                    }
                    catch (Exception ex)
                    {

                        result = "no:"+ex;
                    }
                }

                else
                {

                    result = "noid";
                }
            }

            else {


                result = "nologin";
            }


            Session.Abandon();
            return result;


        }

        public string ForgotPassword(string Email)
        {
            string result = "init";
          
            string strQry = "";

            Email = ut.SqlFriendly(Email);

            DataSet ds = new DataSet();
            if (string.IsNullOrEmpty(Email))
            {
                result = "noid" + Email;
            }
            else
            {
                strQry = "select top 1 password,email,facultyid,fname from Addfacultymaster where email = '" + Email + "'";
                try
                {
                    ds = db.executeQuery(strQry);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {

                        string password = cr.Decrypt(ds.Tables[0].Rows[0]["password"].ToString());
                        string name = ds.Tables[0].Rows[0]["fname"].ToString();
                        string message = "Dear " + name + ", <br/> your  password is " + password + "";
                        string tomail = ds.Tables[0].Rows[0]["email"].ToString();
                        try
                        {
                            if (ds.Tables[0].Rows[0]["email"].ToString() != "")
                            {
                                string mail_content = "";
                                mail_content = "<table style='width: 90%;border:1px solid #ddd;' cellspacing='0' cellpadding='0'>";
                                mail_content += "<tr><td style='padding:10px 0px 0px 10px;'><div style='width: 100%;border-bottom:1px solid #ddd;float:left;'><div style='float: left; width: 338px;'><img src='https://admission.upes.ac.in/img/application/upes%20logo.png' style='padding-top: 16px;' alt='UPES' /></div></div></td></tr>";
                                mail_content += "<br/><br/><br/><br/><tr><td style='padding:10px;'>" + message + "<br/></td></tr><br/><br/><br/><br/>";
                                mail_content += "</table>";
                                string fromaddress = "enrollments@upes.ac.in";
                                string subject = "UPES | Faculty | Forgot password";
                                mail_common("UPES | Forgot Password", fromaddress, mail_content, tomail, subject, "");
                                result = "yes";
                              
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "noemail:" + ex.ToString();
                        }
                    }

                    else
                    {
                        result = "norecordsfound" + strQry;
                    }

                }
                catch (Exception ex)
                {

                    result = "nocatch:" + ex.ToString();
                }

            }

        


            Session.Abandon();
            return result;


        }
        public ActionResult logout()
        {
            Session.Abandon();

            return RedirectToAction("Index", "Login", new { area = "user" });
        }

        public string mail_common(string fromname, string fromaddress, String body, string email, string subject, string cc)
        {
            string result = "";
            try
            {


                ma.is_html = true;
                ma.fromname = fromname;
                ma.fromaddress = fromaddress;
                ma.body = body;
                ma.toaddress = email;
                ma.subject = subject;
                if (cc != "")
                {
                    ma.cc = cc;
                }
                ma.bcc = "maris@newgendigital.com,parasanth@newgendigital.com";

                result = ma.send();

            }
            catch (Exception ex)
            {
                result = ex.ToString();

            }
            return result;
        }

        public string bindFacultyURL(string facid)
        {
            string url = "javascript:void(0);";

            string selQRY = "select url from faculty_url where facultyid = '" + facid + "' ";
            DataSet ds = db.executeQuery(selQRY);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                url = ds.Tables[0].Rows[0]["url"].ToString();

                url = "/" + url;

            }

                return url;
            }
    }
}