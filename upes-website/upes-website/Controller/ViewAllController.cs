﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace upes_website.Controller
{
    public class ViewAllController : SurfaceController
    {
        // GET: ViewAll
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ShowMore(int startAt, int contentID)
        {
            IPublishedContent conent = Umbraco.TypedContent(contentID);
            var thoughtleaders = conent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
            if (!thoughtleaders.Any())
                return Content("");
            var displaySet = thoughtleaders.Skip(startAt).Take(4);
            return PartialView("ViewAll", new Tuple<bool, int, IEnumerable<IPublishedContent>, int>(thoughtleaders.Count() > startAt + 3, startAt, displaySet, contentID));
        }
    }
}