﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using upes_website.Models;

namespace upes_website.Controller
{
    public class FinancialAssitanceContController : SurfaceController
    {
        // GET: FinancialAssitanceCont
        public ActionResult Index()
        {
            FinancialAssitance model = new FinancialAssitance();
            return PartialView("Forms/Financial",model);
        }
        public ActionResult HandleSubmit(FinancialAssitance model, string id)
        {
            if(ModelState.IsValid)
            {
                //ModelState.
            }
            return PartialView("Forms/Result", new Tuple<string,string> (id,model.domicile));
        }
    }
}