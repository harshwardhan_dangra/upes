﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.cms.helpers;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;

namespace upes_website.Controller
{
    public class PressReleaseController : SurfaceController
    {
        // GET: PressRelease
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult returnMonth()
        {
            string htmlString = "";
            Session["loadMore"] = false;
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            int count = 0;
            List<Months> allMonth = new List<Months>();
            foreach (var pressRelase in pressChild)
            {
                Months monthClass = new Months();
                if (pressRelase.HasValue("publishedDate"))
                {
                    var publishedDate = pressRelase.GetPropertyValue<DateTime>("publishedDate");
                    monthClass.MonthName = publishedDate;
                    allMonth.Add(monthClass);
                }
            }
            var groupedCustomerList = allMonth.GroupBy(u => u.MonthName.ToString("MMM yyyy")).Select(grp => grp.ToList()).ToList().OrderByDescending(x=>x[0].MonthName).ToList();
            for (int j = 0; j < groupedCustomerList.Count(); j++)
            {
                htmlString += "<li><a onclick='doLoadData(this,&apos;monthName&apos;)'>" + groupedCustomerList[j][0].MonthName.ToString("MMMM") + " " + groupedCustomerList[j][0].MonthName.ToString("yyyy") + "</a><a  class='monthPart'> (" + groupedCustomerList[j].Count + ")</a></li>";
            }

            return Content(htmlString);
        }
        public ActionResult returnTags()
        {
            string htmlString = "";
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            List<Tags> allTags = new List<Tags>();
            foreach (var pressRelease in pressChild)
            {

                if (pressRelease.HasValue("tag"))
                {
                    if (pressRelease.HasValue("publishedDate"))
                    {
                        var tags = pressRelease.GetPropertyValue<IEnumerable<string>>("tag");
                        foreach (var tag in tags)
                        {
                            Tags tagClass = new Tags();
                            tagClass.TagName = tag;
                            allTags.Add(tagClass);
                        }
                    }

                }
            }
            var groupedCustomerList = allTags.GroupBy(u => u.TagName).Select(grp => grp.ToList()).ToList();
            for (int i = 0; i < groupedCustomerList.Count(); i++)
            {
                htmlString += "<li><a onclick='doLoadData(this,&apos;tag&apos;)'>" + groupedCustomerList[i][0].TagName + "</a></li>";
            }
            return Content(htmlString);
        }
        public JsonResult returnJsonContent(string monthAndYear, string tagKeyWord)
        {
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            List<PressReleaseContent> allContent = new List<PressReleaseContent>();
            List<PressReleaseContent> finalContent = new List<PressReleaseContent>();
            foreach (var pressRelease in pressChild)
            {
                PressReleaseContent contentClass = new PressReleaseContent();
                if (pressRelease.HasValue("title"))
                {
                    contentClass.Name = pressRelease.GetPropertyValue<string>("title");
                }
                else
                {
                    contentClass.Name = "";
                }
                if (pressRelease.HasValue("shortDescription"))
                {
                    contentClass.ShortDescription = pressRelease.GetPropertyValue<string>("shortDescription");
                }
                else
                {
                    contentClass.ShortDescription = "";
                }
                if (pressRelease.HasValue("readMore"))
                {
                    contentClass.ReadMore = pressRelease.GetPropertyValue<RelatedLinks>("readMore");
                }
                else
                {
                    contentClass.ReadMore = null;
                }
                if (pressRelease.HasValue("publishedDate"))
                {
                    contentClass.PublishedDate = pressRelease.GetPropertyValue<DateTime>("publishedDate");
                }
                else
                {
                    contentClass.PublishedDate = DateTime.Now;
                }
                if (pressRelease.HasValue("tag"))
                {
                    contentClass.Tag = pressRelease.GetPropertyValue<IEnumerable<string>>("tag");
                }
                else
                {
                    contentClass.Tag = null;
                }
                contentClass.Id = pressRelease.Id;
                contentClass.PageTitle = pressRelease.GetPropertyValue<string>("title");
                contentClass.Url = pressRelease.GetPropertyValue<string>("title").ToLower().ToString().Replace(' ', '-');
                contentClass.ContentGridAlias = "";
                allContent.Add(contentClass);
            }
            if (monthAndYear != null)
            {
                for (int i = 0; i < allContent.Count; i++)
                {
                    if (monthAndYear == allContent[i].PublishedDate.ToString("MMMM yyyy"))
                    {
                        finalContent.Add(allContent[i]);
                    }
                }
            }
            else
            {
                if (tagKeyWord != null)
                {
                    for (int i = 0; i < allContent.Count; i++)
                    {
                        List<Tags> allTags = new List<Tags>();
                        var tags = allContent[i].Tag;
                        foreach (var tag in tags)
                        {
                            Tags tagClass = new Tags();
                            tagClass.TagName = tag;
                            allTags.Add(tagClass);
                        }
                        for (int j = 0; j < allTags.Count; j++)
                        {
                            if (allTags[j].TagName == tagKeyWord)
                            {
                                finalContent.Add(allContent[i]);
                            }
                        }
                    }
                }
            }
            return Json(finalContent, JsonRequestBehavior.AllowGet);
        }
        public ActionResult fullContent(int id)
        {
            string htmlString = "";
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            List<PressReleaseContent> allContent = new List<PressReleaseContent>();
            List<PressReleaseContent> finalContent = new List<PressReleaseContent>();
            //HtmlHelper html = new HtmlHelper();
            foreach (var pressRelease in pressChild)
            {
                PressReleaseContent contentClass = new PressReleaseContent();
                if (pressRelease.HasValue("title"))
                {
                    contentClass.Name = pressRelease.GetPropertyValue<string>("title");
                }
                else
                {
                    contentClass.Name = "";
                }
                if (pressRelease.HasValue("shortDescription"))
                {
                    contentClass.ShortDescription = pressRelease.GetPropertyValue<string>("shortDescription");
                }
                else
                {
                    contentClass.ShortDescription = "";
                }
                if (pressRelease.HasValue("readMore"))
                {
                    contentClass.ReadMore = pressRelease.GetPropertyValue<RelatedLinks>("readMore");
                }
                else
                {
                    contentClass.ReadMore = null;
                }
                if (pressRelease.HasValue("publishedDate"))
                {
                    contentClass.PublishDate = pressRelease.GetPropertyValue<DateTime>("publishedDate").ToString("MMMM dd, yyyy");
                    contentClass.PublishedDate = pressRelease.GetPropertyValue<DateTime>("publishedDate");
                }
                else
                {
                    contentClass.PublishedDate = DateTime.Now;
                }
                if (pressRelease.HasValue("tag"))
                {
                    contentClass.Tag = pressRelease.GetPropertyValue<IEnumerable<string>>("tag");
                }
                else
                {
                    contentClass.Tag = null;
                }
                contentClass.Id = pressRelease.Id;
                contentClass.PageTitle = pressRelease.GetPropertyValue<string>("title");
                contentClass.Url = pressRelease.GetPropertyValue<string>("title").ToLower().ToString().Replace(' ', '-');
                contentClass.ContentGridAlias = "";
                allContent.Add(contentClass);
            }
            if (id != 0)
            {
                for (int i = 0; i < allContent.Count; i++)
                {
                    if (allContent[i].Id == id)
                    {
                        htmlString = id.ToString();
                        //GetGridHtml(Umbraco.TypedContent(id), "");
                        var node = Umbraco.TypedContent(id);
                        MvcHtmlString html = node.GetGridHtml("pageContent", "bootstrap3");
                        htmlString = html.ToString();
                    }
                }
            }
            return Content(htmlString);
        }


        public ActionResult returnHtmlContent(string monthAndYear, string tagKeyWord,string All)
        {
            string htmlString = "";
            Session["tagName"] = "";
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            List<PressReleaseContent> allContent = new List<PressReleaseContent>();
            List<PressReleaseContent> finalContent = new List<PressReleaseContent>();
            foreach (var pressRelease in pressChild)
            {
                PressReleaseContent contentClass = new PressReleaseContent();
                if (pressRelease.HasValue("title"))
                {
                    contentClass.Name = pressRelease.GetPropertyValue<string>("title");
                }
                else
                {
                    contentClass.Name = "";
                }
                if (pressRelease.HasValue("shortDescription"))
                {
                    contentClass.ShortDescription = pressRelease.GetPropertyValue<string>("shortDescription");
                }
                else
                {
                    contentClass.ShortDescription = "";
                }
                if (pressRelease.HasValue("readMore"))
                {
                    contentClass.ReadMore = pressRelease.GetPropertyValue<RelatedLinks>("readMore");
                }
                else
                {
                    contentClass.ReadMore = null;
                }
                if (pressRelease.HasValue("publishedDate"))
                {
                    contentClass.PublishedDate = pressRelease.GetPropertyValue<DateTime>("publishedDate");
                }
                else
                {
                    contentClass.PublishedDate = DateTime.Now;
                }
                if (pressRelease.HasValue("tag"))
                {
                    contentClass.Tag = pressRelease.GetPropertyValue<IEnumerable<string>>("tag");
                }
                else
                {
                    contentClass.Tag = null;
                }
                if(pressRelease.HasValue("publisherName"))
                {
                    contentClass.PublisherName = pressRelease.GetPropertyValue<string>("publisherName");
                }
                else
                {
                    contentClass.PublisherName = "";
                }
                contentClass.ExternalLink = pressRelease.GetPropertyValue<bool>("externalLink");
                contentClass.Id = pressRelease.Id;
                contentClass.PageTitle = pressRelease.GetPropertyValue<string>("title");
                contentClass.Url = pressRelease.Url;
                contentClass.ContentGridAlias = "";
                allContent.Add(contentClass);
            }
            if (monthAndYear != null)
            {
                for (int i = 0; i < allContent.Count; i++)
                {
                    if (monthAndYear == allContent[i].PublishedDate.ToString("MMMM yyyy"))
                    {
                        finalContent.Add(allContent[i]);
                    }
                }
            }
            else if (tagKeyWord != null)
            {
                if (Session["tagName"].ToString() != tagKeyWord)
                {
                    for (int i = 0; i < allContent.Count; i++)
                    {
                        List<Tags> allTags = new List<Tags>();
                        var tags = allContent[i].Tag;
                        foreach (var tag in tags)
                        {
                            Tags tagClass = new Tags();
                            tagClass.TagName = tag;
                            allTags.Add(tagClass);
                        }
                        for (int j = 0; j < allTags.Count; j++)
                        {
                            if (allTags[j].TagName == tagKeyWord)
                            {
                                finalContent.Add(allContent[i]);
                            }
                        }
                    }
                }
                Session["tagName"] = tagKeyWord;
            }
            if (monthAndYear != null || tagKeyWord != null)
            {
                var orderLast = finalContent.OrderByDescending(x => x.PublishedDate).ToList();
                for (int i = 0; i < orderLast.Count; i++)
                {
                    if (orderLast[i].PublishedDate != null)
                    {
                        string loadClass = "";
                        if (i >= 5)
                        {
                            loadClass = "morePress";
                            Session["loadMore"] = true;
                        }
                        else
                        {
                            Session["loadMore"] = false;
                        }
                        htmlString += "<div class='newsBlock " + loadClass + "'> <h2 class='fontBold'> " + orderLast[i].Name + "<div class='addthis_inline_share_toolbox'></div></h2> <span> " + orderLast[i].PublishedDate.ToString("MMMM dd, yyyy") + " </span>";
                        if(orderLast[i].PublisherName!="")
                        {
                            htmlString += "<span class='leftIndicator'>" + orderLast[i].PublisherName + "</span>  <p> " + orderLast[i].ShortDescription + " </p>";
                        }
                        else
                        {
                            htmlString += "<p> " + orderLast[i].ShortDescription + " </p>";
                        }
                        if (orderLast[i].ReadMore != null)
                        {
                            if(orderLast[i].ExternalLink!=false)
                            {
                                htmlString += "<a href='" + orderLast[i].ReadMore.First().Link + "' class='readmore' onclick='doReloadContent(&apos;" + orderLast[i].Id + "&apos;)' target='_blank'>" + orderLast[i].ReadMore.First().Caption + "</a>";
                            }
                            else
                            {
                                htmlString += "<a href='" + orderLast[i].Url + "' class='readmore' onclick='doReloadContent(&apos;" + orderLast[i].Id + "&apos;)'>" + orderLast[i].ReadMore.First().Caption + "</a>";
                            }
                           
                        }
                        htmlString += " </div>";
                    }

                }
            }
            else
            {
                var orderLast = allContent.OrderByDescending(x => x.PublishedDate).ToList();
                for (int i = 0; i < orderLast.Count; i++)
                {
                    string loadClass = "";
                    if (allContent[i].PublishedDate != null)
                    {
                        if (i >= 5)
                        {
                            loadClass = "morePress";
                            Session["loadMore"] = true;
                        }
                        else
                        {
                            Session["loadMore"] = false;
                        }
                        htmlString += "<div class='newsBlock " + loadClass + "'> <h2 class='fontBold'> " + orderLast[i].Name + "<div class='addthis_inline_share_toolbox'></div></h2> <span> " + orderLast[i].PublishedDate.ToString("MMMM dd, yyyy") + " </span>";
                        if(orderLast[i].PublisherName!="")
                        {
                            htmlString += "<span class='leftIndicator'>" + orderLast[i].PublisherName + "</span>  <p> " + orderLast[i].ShortDescription + " </p>";
                        }
                        else
                        {
                            htmlString += "<p> " + orderLast[i].ShortDescription + " </p>";
                        }
                        if (orderLast[i].ReadMore != null)
                        {
                            if(orderLast[i].ExternalLink!=false)
                            {
                                htmlString += "<a href='" + orderLast[i].ReadMore.First().Link + "' class='readmore' onclick='doReloadContent(&apos;" + orderLast[i].Id + "&apos;)' target='_blank'>" + orderLast[i].ReadMore.First().Caption + "</a>";
                            }
                            else
                            {
                                htmlString += "<a href='" + orderLast[i].Url + "' class='readmore' onclick='doReloadContent(&apos;" + orderLast[i].Id + "&apos;)'>" + orderLast[i].ReadMore.First().Caption + "</a>";
                            }
                            
                        }
                        htmlString += " </div>";
                    }

                }
            }

            return Content(htmlString+"###"+Session["loadMore"].ToString());
        }
        public ActionResult bindMonthMobile()
        {
            string htmlString = "";
            Session["loadMore"] = false;
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            int count = 0;
            List<Months> allMonth = new List<Months>();
            foreach (var pressRelase in pressChild)
            {
                Months monthClass = new Months();
                if (pressRelase.HasValue("publishedDate"))
                {
                    var publishedDate = pressRelase.GetPropertyValue<DateTime>("publishedDate");
                    monthClass.MonthName = publishedDate;
                    allMonth.Add(monthClass);
                }
            }
            var groupedCustomerList = allMonth.GroupBy(u => u.MonthName.ToString("MMM yyyy")).Select(grp => grp.ToList()).ToList().OrderByDescending(x => x[0].MonthName).ToList();
            for (int j = 0; j < groupedCustomerList.Count(); j++)
            {
                htmlString += "<option value='" + groupedCustomerList[j][0].MonthName.ToString("MMMM") + " " + groupedCustomerList[j][0].MonthName.ToString("yyyy") + "'>" + groupedCustomerList[j][0].MonthName.ToString("MMMM") + " " + groupedCustomerList[j][0].MonthName.ToString("yyyy") + "("+ groupedCustomerList[j].Count +") </option>";
            }
            return Content(htmlString);
        }
        public ActionResult bindTagsMobile()
        {
            string htmlString = "";
            var press = Umbraco.TypedContent(6115);
            var pressChild = press.Children;
            List<Tags> allTags = new List<Tags>();
            foreach (var pressRelease in pressChild)
            {

                if (pressRelease.HasValue("tag"))
                {
                    if (pressRelease.HasValue("publishedDate"))
                    {
                        var tags = pressRelease.GetPropertyValue<IEnumerable<string>>("tag");
                        foreach (var tag in tags)
                        {
                            Tags tagClass = new Tags();
                            tagClass.TagName = tag;
                            allTags.Add(tagClass);
                        }
                    }

                }
            }
            var groupedCustomerList = allTags.GroupBy(u => u.TagName).Select(grp => grp.ToList()).ToList();
            for (int i = 0; i < groupedCustomerList.Count(); i++)
            {
                htmlString += "<option value='" + groupedCustomerList[i][0].TagName + "'>" + groupedCustomerList[i][0].TagName + "</option>";
            }
            return Content(htmlString);
        }
    }
}