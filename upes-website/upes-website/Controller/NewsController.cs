﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace upes_website.Controller
{
    public class NewsController : UmbracoApiController
    {
        public JObject GetAllNews()
        {

            string newsData = "";
            int newsCount = 0;
            try
            {
                var news = Umbraco.TypedContent(2920);
                var newsItems = news.Children.OrderByDescending(x => x.GetPropertyValue<DateTime>("publishedDate")); //OrderBy(x => x.GetPropertyValue<DateTime>("publishedDate"));

                foreach (var newsItem in newsItems)
                {
                    if (newsItem.HasValue("publishedDate"))
                    {
                        newsData += "{";
                        if (newsItem.HasValue("showcaseTitle"))
                        {
                            newsData += "\"title\":\"" + newsItem.GetPropertyValue<string>("showcaseTitle") + "\",";
                        }
                        else
                        {
                            newsData += "\"title\":\"\",";
                        }
                        if (newsItem.HasValue("shortDescription"))
                        {
                            newsData += "\"short_description\":\"" + newsItem.GetPropertyValue<string>("shortDescription") + "\",";
                        }
                        else
                        {
                            newsData += "\"short_description\":\"\",";
                        }
                        if (newsItem.HasValue("detailContent"))
                        {
                            newsData += "\"description\":\"" + newsItem.GetPropertyValue<string>("detailContent").Trim() + "\",";
                        }
                        else
                        {
                            newsData += "\"description\":\"\",";
                        }

                        if (newsItem.HasValue("publishedDate"))
                        {
                            newsData += "\"date\":\"" + newsItem.GetPropertyValue<DateTime>("publishedDate").ToString("dd/MM/yyyy HH:mm") + "\",";
                        }
                        else
                        {
                            newsData += "\"date\":\"\",";
                        }

                        if (newsItem.HasValue("thumbImage"))
                        {
                            newsData += "\"img\":\"https://www.upes.ac.in" + newsItem.GetPropertyValue<IPublishedContent>("thumbImage").Url + "\"";
                        }
                        else
                        {
                            newsData += "\"img\":\"\"";
                        }

                        newsData += "},";
                        newsCount++;
                    }
                }
                if (newsData != "")
                {
                    newsData = newsData.Substring(0, (newsData.Length - 1));
                }
                if (newsCount > 0)
                {
                    newsData = "{\"status\": \"OK\", \"message\": \"success\",\"list\": [" + newsData + "]}";
                }
                else
                {
                    newsData = "{\"status\": \"OK\", \"message\": \"no news data found\",\"list\": []}";
                }
            }
            catch (Exception exe)
            {
                newsData = "{\"status\": \"ERROR\", \"message\": \"" + exe.ToString().Replace("\\", "") + "\",\"list\": []}";
            }
            return JObject.Parse(newsData);
        }
    }
}