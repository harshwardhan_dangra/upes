﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using upes_website.Models;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;


namespace upes_website.Controller
{
    public class CampusFacilitiesController : UmbracoApiController
    {
        StudentController objStu = new StudentController();
        public class facilityModel
        {
            public string campusFacilityID { get; set; }
            public string title { get; set; }
            public string imageURL { get; set; }
            public string[] imageGallery { get; set; }
        }
        public class ResponseModel
        {
            public object data { set; get; }
        }

        [HttpGet]
        public HttpResponseMessage Get(string tag)
        {
            String accessToken = "", hostURL = "";
            int studentStatus = 0;
            Boolean tagStatus = false;
            try
            {
                hostURL = ConfigurationManager.AppSettings["api_host"].ToString();
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];

                if (tag == "")
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid tag");
                }

                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    var courseContent = Umbraco.TypedContent(1468);
                    List<facilityModel> objPrograms = new List<facilityModel>();
                    //UPCOMING EVENTS
                    if (courseContent.HasValue("contentPicker"))
                    {
                        var facilityItems = courseContent.Children();
                        foreach (var cbContent in facilityItems)
                        {
                            if(cbContent.HasProperty("tagging") &&cbContent.HasValue("tagging"))
                            {

                            }
                            if (cbContent.GetPropertyValue("tagging").ToString() == tag)
                            {
                                facilityModel objProgram = new facilityModel();
                                objProgram.campusFacilityID = cbContent.GetKey().ToString();

                                if (cbContent.HasValue("showcaseTitle"))
                                {
                                    objProgram.title = objStu.removeHTML(cbContent.GetPropertyValue("showcaseTitle").ToString());
                                }
                                else
                                {
                                    objProgram.title = "";
                                }
                                if (cbContent.HasValue("showcaseImage"))
                                {
                                    var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("showcaseImage");
                                    if (ImgPath != null)
                                    {
                                        objProgram.imageURL = hostURL + ImgPath.Url;
                                    }
                                    else
                                    {
                                        objProgram.imageURL = "";
                                    }
                                }
                                else
                                {
                                    objProgram.imageURL = "";
                                }

                                //List<string> arGalleryImage = new List<string>();
                                //if (cbContent.HasValue("galleryImages"))
                                //{
                                //    var imgPaths = cbContent.GetPropertyValue<IEnumerable<IPublishedContent>>("galleryImages");
                                //    foreach (var imgPath in imgPaths)
                                //    {
                                //        arGalleryImage.Add(hostURL + imgPath.Url);
                                //    }
                                //}
                                //objProgram.imageGallery = arGalleryImage.ToArray();
                                objPrograms.Add(objProgram);
                                tagStatus = true;
                                //break;
                            }
                        }
                    }
                    if (tagStatus)
                    {
                        ResponseModel responseModel = new ResponseModel();
                        responseModel.data = objPrograms;

                        return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No data found");
                    }
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }
    }
}
