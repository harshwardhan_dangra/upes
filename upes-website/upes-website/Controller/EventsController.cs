﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.IO;
using upes_website.Models;
using Umbraco.Core.Models;
using upes_website.Gutils;

namespace upes_website.Controller
{
    public class EventsController : UmbracoApiController
    {
        DBUtils objDbu = new DBUtils();
        StudentController objStu = new StudentController();
        public class eventsModel
        {
            public string eventID { get; set; }
            public string title { get; set; }
            public string imageURL { get; set; }
            public string description { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public Boolean isRegistered { get; set; }
        }
        public class ResultModel
        {
            public object upcomingEvents { get; set; }
            public object pastEvents { get; set; }
        }
        public class ResponseModel
        {
            public object data { set; get; }
        }

        public class RequestModel
        {
            public string eventID { set; get; }
        }
        [HttpGet]
        public HttpResponseMessage Get()
        {
            String accessToken = "", hostURL = "";
            int studentStatus = 0;
            try
            {
                hostURL = ConfigurationManager.AppSettings["api_host"].ToString();
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];
                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    var courseContent = Umbraco.TypedContent(1237);
                    List<eventsModel> objUpcomingEvents = new List<eventsModel>();
                    List<eventsModel> objPastEvents = new List<eventsModel>();
                    //UPCOMING EVENTS
                    if (courseContent.HasValue("contentPicker"))
                    {
                        var studentAchievementItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker");
                        foreach (var cbContent in studentAchievementItems)
                        {
                            eventsModel objEvent = new eventsModel();

                            objEvent.eventID = cbContent.GetKey().ToString();
                            if (cbContent.HasValue("showcaseImage"))
                            {
                                var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("showcaseImage");
                                if (ImgPath != null)
                                {
                                    objEvent.imageURL = hostURL + ImgPath.Url;
                                }
                                else
                                {
                                    objEvent.imageURL = "";
                                }
                            }
                            else
                            {
                                objEvent.imageURL = "";
                            }
                            if (cbContent.HasValue("shortDescription"))
                            {
                                objEvent.title = objStu.removeHTML(cbContent.GetPropertyValue("shortDescription").ToString());
                            }
                            else
                            {
                                objEvent.title = "";
                            }

                            if (cbContent.HasValue("pageContent"))
                            {
                                objEvent.description = objStu.removeHTML(cbContent.GetGridHtml("pageContent").ToString()).Replace("\"", "'").Trim();
                            }
                            else if(cbContent.HasProperty("shortDescription") &&cbContent.HasValue("shortDescription"))
                            {
                                objEvent.description = cbContent.GetPropertyValue<string>("shortDescription").ToString().Replace("\"", "'").Trim();
                            }
                            else
                            {
                                objEvent.description = "";
                            }
                            if (cbContent.HasValue("publishedDate"))
                            {
                                objEvent.startDate = cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
                                objEvent.endDate = cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
                            }
                            else
                            {
                                objEvent.startDate = "";
                                objEvent.endDate = "";
                            }

                            SqlParameter[] sqlCourseParams = new SqlParameter[3];
                            sqlCourseParams[0] = new SqlParameter("@accessToken", SqlDbType.VarChar, 128);
                            sqlCourseParams[0].Value = accessToken;
                            sqlCourseParams[1] = new SqlParameter("@eventID", SqlDbType.VarChar, 128);
                            sqlCourseParams[1].Value = cbContent.GetKey().ToString();
                            sqlCourseParams[2] = new SqlParameter("@result", SqlDbType.Int);
                            sqlCourseParams[2].Direction = ParameterDirection.Output;
                            sqlCourseParams[2].Value = 0;
                            DataSet objCourseds = objDbu.executeSP(true, "sp_get_event_registration_app", sqlCourseParams);
                            if (Convert.ToInt32(sqlCourseParams[2].Value) == 1)
                                objEvent.isRegistered = true;
                            else
                                objEvent.isRegistered = false;
                            objUpcomingEvents.Add(objEvent);
                        }
                    }

                    //PAST EVENTS
                    if (courseContent.HasValue("contentPicker2"))
                    {
                        var studentAchievementItems = courseContent.GetPropertyValue<IEnumerable<IPublishedContent>>("contentPicker2");
                        foreach (var cbContent in studentAchievementItems)
                        {
                            eventsModel objEvent = new eventsModel();

                            objEvent.eventID = cbContent.GetKey().ToString();
                            if (cbContent.HasValue("showcaseImage"))
                            {
                                var ImgPath = cbContent.GetPropertyValue<IPublishedContent>("showcaseImage");
                                if (ImgPath != null)
                                {
                                    objEvent.imageURL = hostURL + ImgPath.Url;
                                }
                                else
                                {
                                    objEvent.imageURL = "";
                                }
                            }
                            else
                            {
                                objEvent.imageURL = "";
                            }
                            if (cbContent.HasValue("shortDescription"))
                            {
                                objEvent.title = objStu.removeHTML(cbContent.GetPropertyValue("shortDescription").ToString());
                            }
                            else
                            {
                                objEvent.title = "";
                            }

                            if(cbContent.HasValue("pageContent"))
                            {
                                objEvent.description = objStu.removeHTML(cbContent.GetGridHtml("pageContent").ToString()).Replace("\"", "'").Trim();
                            }
                            else if (cbContent.HasProperty("shortDescription") && cbContent.HasValue("shortDescription"))
                            {
                                objEvent.description = cbContent.GetPropertyValue<string>("shortDescription").ToString().Replace("\"", "'").Trim();
                            }
                            else
                            {
                                objEvent.description = "";
                            }
                            if (cbContent.HasValue("publishedDate"))
                            {
                                objEvent.startDate = cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
                                objEvent.endDate = cbContent.GetPropertyValue<DateTime>("publishedDate").ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
                            }
                            else
                            {
                                objEvent.startDate = "";
                                objEvent.endDate = "";
                            }
                            SqlParameter[] sqlCourseParams = new SqlParameter[3];
                            sqlCourseParams[0] = new SqlParameter("@accessToken", SqlDbType.VarChar, 128);
                            sqlCourseParams[0].Value = accessToken;
                            sqlCourseParams[1] = new SqlParameter("@eventID", SqlDbType.VarChar, 128);
                            sqlCourseParams[1].Value = cbContent.GetKey().ToString();
                            sqlCourseParams[2] = new SqlParameter("@result", SqlDbType.Int);
                            sqlCourseParams[2].Direction = ParameterDirection.Output;
                            sqlCourseParams[2].Value = 0;
                            DataSet objCourseds = objDbu.executeSP(true, "sp_get_event_registration_app", sqlCourseParams);
                            if (Convert.ToInt32(sqlCourseParams[2].Value) == 1)
                                objEvent.isRegistered = true;
                            else
                                objEvent.isRegistered = false;
                            objPastEvents.Add(objEvent);
                        }
                    }

                    ResultModel objResponse = new ResultModel();
                    objResponse.upcomingEvents = objUpcomingEvents;
                    objResponse.pastEvents = objPastEvents;

                    ResponseModel responseModel = new ResponseModel();
                    responseModel.data = objResponse;

                    return Request.CreateResponse<ResponseModel>(HttpStatusCode.OK, responseModel);
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }

        [HttpPost]
        public HttpResponseMessage Register([FromBody] RequestModel objRequest)
        {
            String accessToken = "";
            int studentStatus = 0;
            JObject objJson;
            try
            {
                IEnumerable<string> inputStringAccessToken;
                if (!Request.Headers.TryGetValues("accessToken", out inputStringAccessToken))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid input parameter");
                }
                var customInputArrayAccessToken = inputStringAccessToken.ToArray();
                accessToken = customInputArrayAccessToken[0];
                objStu.validateAccessToken(accessToken, out studentStatus);
                if (studentStatus == 1)
                {
                    Boolean eventIdStatus = false;
                    var eventsContent = Umbraco.TypedContent(1237);
                    if (eventsContent.Children.Count() > 0)
                    {
                        foreach (var eventItem in eventsContent.Children)
                        {
                            if (eventItem.GetKey().ToString() == objRequest.eventID)
                            {
                                eventIdStatus = true;
                                break;
                            }
                        }

                        if (eventIdStatus)
                        {
                            SqlParameter[] sqlCourseParams = new SqlParameter[3];
                            sqlCourseParams[0] = new SqlParameter("@accessToken", SqlDbType.VarChar, 128);
                            sqlCourseParams[0].Value = accessToken;
                            sqlCourseParams[1] = new SqlParameter("@eventID", SqlDbType.VarChar, 128);
                            sqlCourseParams[1].Value = objRequest.eventID;
                            sqlCourseParams[2] = new SqlParameter("@result", SqlDbType.Int);
                            sqlCourseParams[2].Direction = ParameterDirection.Output;
                            sqlCourseParams[2].Value = 0;
                            DataSet objCourseds = objDbu.executeSP(true, "sp_insert_event_registration_app", sqlCourseParams);

                            if (Convert.ToInt32(sqlCourseParams[2].Value) > 0)
                            {
                                objJson = JObject.Parse("{\"message\" : \"Success\"}");
                                return Request.CreateResponse<JObject>(HttpStatusCode.OK, objJson);
                            }
                            else if (Convert.ToInt32(sqlCourseParams[2].Value) == -1)
                            {
                                objJson = JObject.Parse("{\"message\" : \"You have already registered for this event\"}");
                                return Request.CreateResponse<JObject>(HttpStatusCode.OK, objJson);
                            }
                            else
                            {
                                objJson = JObject.Parse("{\"message\" : \"Error while registering for the event\"}");
                                return Request.CreateResponse<JObject>(HttpStatusCode.InternalServerError, objJson);
                            }
                        }
                        else
                        {
                            objJson = JObject.Parse("{\"message\" : \"Invalid event id\"}");
                            return Request.CreateResponse<JObject>(HttpStatusCode.BadRequest, objJson);
                        }
                    }
                    else
                    {
                        objJson = JObject.Parse("{\"message\" : \"No events found\"}");
                        return Request.CreateResponse<JObject>(HttpStatusCode.BadRequest, objJson);
                    }
                }
                else if (studentStatus == -1)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access token");
                }
                else if (studentStatus == -2)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Access token is expired");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid request");
                }
            }
            catch (Exception exe)
            {
                string tempError = exe.ToString().Replace("\r\n", "").Replace("\\", "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, tempError);
            }
        }

    }
}
