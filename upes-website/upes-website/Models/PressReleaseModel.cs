﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Models;
namespace upes_website.Models
{
    public class PressReleaseModel
    {
        
    }
    public class Months
    {
        public DateTime MonthName { get; set; }
    }
    public class Tags
    {
        public string TagName { get; set; }
    }
    public class PressReleaseContent
    {
        public string Name { get; set; }
        public string PublisherName { get; set; }
        public string ShortDescription { get; set; }
        public string Month { get; set; }
        public IEnumerable<string> Tag { get; set; }
        public RelatedLinks ReadMore { get; set; }
        public DateTime PublishedDate { get; set; }
        public string PageContent { get; set; }
        public int Id { get; set; }
        public string Url { get; set; }
        public string ContentGridAlias { get; set; }
        public string PageTitle { get; set; }
        public string PublishDate { get; set; }
        public bool ExternalLink{get;set;}
    }
}