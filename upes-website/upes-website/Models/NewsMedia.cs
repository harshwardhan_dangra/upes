﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace upes_website.Models
{
    public class NewsMedia
    {
        public DateTime PublishedDate { get; set; }
        public String Title { get; set; }
        public string Description { get; set; }
        public IPublishedContent imgPath { get; set; }
        public RelatedLinks ExternalLink { get; set; }
        public IPublishedContent imgAltText { get; set; }
        public IPublishedContent MoreDetailSnippetImage { get; set; }
        public IPublishedContent MoreDetailSnippetPDF { get; set; }
    }
    public class Year
    {
        public int Years { get; set; }
    }
    public class Month
    {
        public DateTime Months { get; set; }
    }
}