﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace upes_website.Models
{
    public class Enquiry_Course_Model
    {
        [Required (ErrorMessage ="Please Enter Your Name", AllowEmptyStrings = false)]
        [Display(Name="Full Name")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please Enter Your Email Address", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage ="Please Enter Valid Email Address")]
        [Display(Name ="Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please Enter Mobile Number")]
        //[Phone(ErrorMessage ="Please Enter Valid Mobile Number")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        [Display(Name="Mobile Number")]
        public string MobileNo { get; set; }
        [Required(ErrorMessage ="Please Select Course")]
        [Display(Name ="Course")]
        public string course { get; set; }
        [Required(ErrorMessage ="Please Select State")]
        [Display(Name ="State")]
        public string State { get; set; }
        [Required(ErrorMessage ="Please Select City")]
        [Display(Name ="City")]
        public string City { get; set; }
        [Required(ErrorMessage ="Please Select Gender")]
        [Display(Name="Gender")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Please Select Year")]
        [Display(Name = "Year")]
        public string Year { get; set; }
        [Required(ErrorMessage ="Please Select Checkbox")]        
        public bool Checkbox { get; set; }
        public string ProspectId { get; set; }
    }
}