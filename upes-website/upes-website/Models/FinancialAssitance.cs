﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace upes_website.Models
{
    public class FinancialAssitance
    {
        [Required(ErrorMessage ="Please Choose Level")]
        public string Level { get; set; }
        public string Course { get; set; }
        public string domicile { get; set; }
        public string alias { get; set; }
    }
}