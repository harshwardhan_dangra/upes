﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace upes_website.Models
{
    public class CallBack
    {
        [Required(ErrorMessage ="Please Enter a Name")]
        [RegularExpression("([a-zA-Z;\\s.&amp;amp;&amp;#39;-]+)", ErrorMessage = "Enter valid name.")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please Enter a Email Address")]
        [EmailAddress(ErrorMessage ="Please Enter Valid Email")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please Enter a Phone no")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage ="Please select State")]
        public string State { get; set; }
        [Required(ErrorMessage ="Please select City")]
        public string City { get; set; }   
        [Required(ErrorMessage ="Please select Course",AllowEmptyStrings =false)]
        public string CourseName { get; set; }
        public string ProspectId { get; set; }
        [Required(ErrorMessage = "Please Select Year")]
        [Display(Name = "Year")]
        public string Year { get; set; }
    }
    public  class course
    {
        public string courseid { get; set; }
        public string coursename { get; set; }
    }
    public class courseNameList
    {
        public string courseName { get; set; }
        public string courseType { get; set; }
    }
}